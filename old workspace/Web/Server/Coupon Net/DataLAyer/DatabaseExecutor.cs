﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DataLayer
{
    public class DatabaseExecutor
    {
        private SqlConnection connection;
        private const string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\CouponDB.mdf;Integrated Security=True";


        public DatabaseExecutor()
        {
            connection = new SqlConnection();
        }

        public DataTable ExecuteSqlQuery(string query)
        {
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();
            }
            catch (Exception e)
            {
                throw e;
            }
            SqlCommand cmd = new SqlCommand(query, connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            try
            {
                dataAdapter.Fill(dataTable);
                return dataTable;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
