﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net.GUI.Options_GUI.Add_GUI
{
    public partial class BuisnessUser_AddCoupon : System.Web.UI.Page
    {
        List<Category> categoriesList;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();
                categoriesList = s.GetCategories();


                if (!Page.IsPostBack)
                {
                    CategoryList.Items.Add("Select Category");
                    foreach (Category category in (categoriesList))
                    {
                        CategoryList.Items.Add(category.name);
                    }
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();
            string name = CouponName.Text,
                  desc = CouponDescriptionTextBox.Text,
                  ini_price = InitPrizeTextBox.Text,
                  new_price = NewPriceTextBox.Text,
                  category = CategoryList.SelectedValue,
                  date = ExpirationTextBox.Text,
                  businessM = (string)Session["BuisnessName"];

            BusinessManager bm = s.GetBusinessManager(businessM);
            Coupon couponToAdd = new Coupon(name, bm.business.businessName, "NONE", float.Parse(ini_price), float.Parse(new_price), (string)Session["BuisnessName"]);
            couponToAdd.categories = new List<Category>();
            couponToAdd.categories.Add(new Category(CategoryList.SelectedValue.ToString(), ""));
            couponToAdd.expirationDate = DateTime.MinValue;
            couponToAdd.itemBrand = "";
            couponToAdd.isApproved = true;
            try
            {
                couponToAdd.expirationDate = DateTime.Parse(date);
            }
            catch (Exception)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Invaild Data.');</script>");
            }
            if (couponToAdd.expirationDate != DateTime.MinValue)
            {
                try
                {
                    s.PutAddCoupon((string)Session["BuisnessName"], couponToAdd);
                }
                catch (Exception ex)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");

                }
            }
        }
    }
}