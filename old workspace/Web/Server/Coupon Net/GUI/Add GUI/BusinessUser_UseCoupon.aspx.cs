﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Coupon_Net.GUI.Add_GUI
{
    public partial class BusinessUser_UseCoupon : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void checkCoupon_Click(object sender, EventArgs e)
        {
            ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();
            //should check if the serial is correct
            try
            {
                
                s.PutSetCouponAsUsed((string)Session["BussinessName"], serialTextBox.Text);
            }
            catch (Exception ex) {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert(ex.Message);</script>");
            }
        }
    }
}