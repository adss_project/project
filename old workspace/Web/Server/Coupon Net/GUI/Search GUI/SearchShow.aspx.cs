﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net.GUI.Search_GUI
{
    public partial class SearchShow : System.Web.UI.Page
    {
        private ServiceLayerClient.ServiceLayerClient serviceLayer;
        List<Coupon> couponList;
        List<Business> businessList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {


                serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                if (((string)Session["typeOfSearch"]).Equals("Coupon"))
                {
                    couponList = (List<Coupon>)Session["TableToShow"];


                    foreach (Coupon coupon in couponList)
                    {
                        ListItem newLine = new ListItem();
                        newLine.Text = coupon.name;  //text name goes i.e. here tab1
                        RadioButtonList1.Items.Add(newLine);
                    }
                }
                else
                {
                    businessList = (List<Business>)Session["TableToShow"];


                    foreach (Business business in businessList)
                    {
                        ListItem newLine = new ListItem();
                        newLine.Text = business.businessName;  //text name goes i.e. here tab1
                        RadioButtonList1.Items.Add(newLine);
                    }
                }
            }           
           
        }



        protected void redirectToCoupon_Click(object sender, EventArgs e)
        {
            if (((string)Session["typeOfSearch"]).Equals("Coupon"))
            {
                Coupon wantedCoupon = null;
                foreach (Coupon coupon in (List<Coupon>)Session["TableToShow"])
                {
                    if (coupon.name.CompareTo(RadioButtonList1.SelectedValue) == 0)
                        wantedCoupon = coupon;
                }
                Session["couponFromSearch"] = wantedCoupon;
                string redirect = "<script>window.open('../View GUI/ViewSingleCoupon.aspx');</script>";
                Response.Write(redirect);
            }
            else
            {
                Business wantedBusiness = null;
                foreach (Business business in (List<Business>)Session["TableToShow"])
                {
                    if (business.businessName.CompareTo(RadioButtonList1.SelectedValue) == 0)
                        wantedBusiness = business;
                }
                Session["BusinessName"] = wantedBusiness.businessName;
                string redirect = "<script>window.open('../View GUI/ViewSingleBuisness.aspx');</script>";
                Response.Write(redirect);
            }
        }
    }
}