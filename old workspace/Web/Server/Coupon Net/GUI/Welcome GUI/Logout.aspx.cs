﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net.GUI.Welcome_GUI
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // clean session
            Session["BusinessName"] = null;
            Session["userName"] = null;
            Session["Admin"] = null;
            Session["Normal"] = null;
            Response.Redirect("../Welcome GUI/Login.aspx");
        }
    }
}