﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Welcome GUI/Welcome.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Coupon_Net.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHolder" runat="server">
    <asp:Login ID="Login1" runat="server" BorderPadding="4" Height="216px" Width="370px" OnAuthenticate="Login1_Authenticate1">
    <TextBoxStyle Font-Bold="true" />
        <LabelStyle ForeColor="White" Font-Size="Large" />
        <ValidatorTextStyle ForeColor="Red" Font-Bold="true" Font-Size="Larger"/>
        <InstructionTextStyle Font-Italic="True" Font-Bold="true" ForeColor="White"/>
        <LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" Font-Size="0.8em" ForeColor="#284775" />
        <TextBoxStyle Font-Size="1em" />
        <TitleTextStyle Font-Bold="True" Font-Size="0.7cm" ForeColor="White" Font-Underline="true" />
        <CheckBoxStyle ForeColor="White" Font-Size="Large"/>
    </asp:Login>

    
</asp:Content>
