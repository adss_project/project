﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BuisnessEntry.aspx.cs" Inherits="Coupon_Net.GUI.Options_GUI.BuisnessEntry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../style/toolbar_style.css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   <div id="nav">
        <ul>
            <li style="float:left;"><a href="../Welcome GUI/Logout.aspx">Log Out</a></li>
            <li><a href="#"> Coupons</a>
                <ul>
                    <li><a href="../Add GUI/BuisnessUser_AddCoupon.aspx">Add New</a></li>
                    <li><a href="../Add GUI/BusinessUser_UseCoupon.aspx">Use</a></li>
                </ul>
            </li>
            <li><a href="#"> Search </a>
            <ul>
                <li><a href="../Search GUI/BuisnessUser_BuisnessSearch.aspx"> Buisness </a></li>
                <li><a href="../Search GUI/BuisnessUser_CouponSearch.aspx"> Coupon </a></li>
            </ul></li>
            <li><a href="#"> View  </a>
                 <ul>
                <li><a href="../View GUI/BuisnessUser_ProfileView.aspx"> Profile </a></li>
                <li><a href="../View GUI/BuisnessUser_CouponView.aspx">  Coupons </a></li>
            </ul></li>

        </ul>
    
    </div>
    </form>
</body>
</html>
