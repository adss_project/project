﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;

namespace Coupon_Net.GUI.View_GUI
{
    public partial class BuisnessUser_ProfileView : System.Web.UI.Page
    {
        Business business;
        BusinessManager businessM;
        string businessName;
        List<Category> categoriesList;
        ServiceLayerClient.ServiceLayerClient serviceLayer;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                businessName = (string)Session["BusinessName"];
                serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                businessM = serviceLayer.GetBusinessManager();
                business = businessM.business;
                //business = new Business("Kakaroto and Son", "Kamehameha", null, (List<Category>)Session["Category"]);

                EditButton.Visible = true;


                BuisnessNameText.Text = business.businessName;
                BuisnessInformationTextBox.Text = business.description;
                CityTextBox.Text = business.address.city.ToString();
                streetTextBox.Text = business.address.street.ToString();
                numTextBox.Text = business.address.number.ToString();
                categoriesList = serviceLayer.GetCategories();
                foreach (Category category in categoriesList)
                {
                    CategoryList.Items.Add(category.name);
                    //needs to be changed to sho the category that matches the business's category
                }


                CategoryList.Enabled = false;
                BuisnessNameText.Enabled = false;
                BuisnessInformationTextBox.Enabled = false;
                CityTextBox.Enabled = false;
                streetTextBox.Enabled = false;
                numTextBox.Enabled = false;
                CategoryList.Enabled = false;
                passTextBox.Enabled = false;
            }
            catch (Exception ex) {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            if (EditButton.Text.CompareTo("Edit") == 0)
            {
                BuisnessNameText.Enabled = true;
                BuisnessInformationTextBox.Enabled = true;
                CityTextBox.Enabled = true;
                streetTextBox.Enabled = true;
                numTextBox.Enabled = true;
                //passTextBox.Enabled = true;
                EditButton.Text = "Confirm";
            }
            else
            {
                BuisnessNameText.Enabled = false;
                BuisnessInformationTextBox.Enabled = false;
                CityTextBox.Enabled = false;
                streetTextBox.Enabled = false;
                numTextBox.Enabled = false;
                passTextBox.Enabled = false;
                EditButton.Text = "Edit";

                ServiceLayerClient.ServiceLayerClient serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                Address newAddress =  new Address(CityTextBox.Text,streetTextBox.Text,int.Parse(numTextBox.Text));
                Business newBusiness = new Business(BuisnessNameText.Text, BuisnessInformationTextBox.Text, newAddress, null);//need to change the nulls to category
                try
                {
                    serviceLayer.PutEditBusinessData((string)Session["userName"], newBusiness);
                    //sould be able to change password
                }
                catch (Exception ex) {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
                }
            }
        }


        protected void CouponList_Click(object sender, EventArgs e)
        {
            Session["BusinessName"] = businessName;
            Response.Redirect("BuisnessUser_CouponView.aspx");
        }

        protected void CityTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}