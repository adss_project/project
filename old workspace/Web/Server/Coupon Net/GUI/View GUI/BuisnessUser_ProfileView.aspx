﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BuisnessUser_ProfileView.aspx.cs" Inherits="Coupon_Net.GUI.View_GUI.BuisnessUser_ProfileView" %>

<!DOCTYPE html>



<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="stylesheet" type="text/css" href="../style/toolbar_style.css" />
    
    <title></title>
     <style type="text/css">

        .auto-style3 {
            width: 714px;
            height: 375px;
        }
        .auto-style7 {
            height: 26px;
            width: 199px;
            text-align: right;
        }
        .auto-style4 {
            height: 26px;
        }
         .auto-style8 {
             height: 26px;
             width: 277px;
         }
         .auto-style9 {
             width: 277px;
         }
        </style>
</head>
<body>
     <form id="form1" runat="server">
    <div id="nav">
        <ul>
            <li style="float:left;"><a href="../Welcome GUI/Logout.aspx">Log Out</a></li>
            <li><a href="#"> Order</a></li>
            <li><a href="#"> Search </a>
            <ul>
                <li><a href="../Search GUI/BuisnessUser_BuisnessSearch.aspx"> Buisness </a></li>
                <li><a href="../Search GUI/BuisnessUser_CouponSearch.aspx"> Coupon </a></li>
            </ul></li>
            <li><a href="#"> View  </a>
                 <ul>
                <li><a href="#"  style="background:#ff6a00"> Profile </a></li>
                <li><a href="../View GUI/BuisnessUser_CouponView.aspx"> My Coupon </a></li>
            </ul></li>

        </ul>
    
    </div>






        <div style="padding: 50px; float:right; margin-top: 42px;">
             <asp:Image ID="Image1" runat="server" ImageUrl="~/GUI/View/title.png" style=" margin-top: 20px; margin-bottom: 70px"  />

    <p style="font:italic; color:#FFFBFF; text-align:left; margin-left:100px; font-size:30px; text-decoration:underline;" > Buisness Profile</p>
    <table class="auto-style3">
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                &nbsp;</td>
            <td class="auto-style8">
                &nbsp;</td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label3" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness Name:"></asp:Label>

            </td>
            <td class="auto-style8">
                <asp:TextBox ID="BuisnessNameText" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label10" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness information:"></asp:Label>

            </td>
            <td class="auto-style8">
                <asp:TextBox ID="BuisnessInformationTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label4" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness Category:"></asp:Label>

            </td>
            <td class="auto-style8">

                <asp:DropDownList ID="CategoryList" runat="server" Width="205px">
                </asp:DropDownList>

            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                                <asp:Label ID="Label9" runat="server" Font-Size="Large" ForeColor="White" Text="City:"></asp:Label>

            </td>
            <td class="auto-style9">

                   <asp:TextBox ID="CityTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" OnTextChanged="CityTextBox_TextChanged" ></asp:TextBox>
            

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                                <asp:Label ID="Label5" runat="server" Font-Size="Large" ForeColor="White" Text="Street:"></asp:Label>

            </td>
            <td class="auto-style9">

                   <asp:TextBox ID="streetTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                                <asp:Label ID="Label6" runat="server" Font-Size="Large" ForeColor="White" Text="Number:"></asp:Label>

            </td>
            <td class="auto-style9">

                   <asp:TextBox ID="numTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            

            </td>
            <td>

            </td>
        </tr>

        <tr>
            <td class="auto-style7">
                                <asp:Label ID="Label1" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness Coupons:"></asp:Label>

            </td>
            <td class="auto-style9">

                <asp:Button ID="RedirectButton0" runat="server"  Text="Coupons List"  BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                            BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" ForeColor="#284775" Height="29px" Width="190px" OnClick="CouponList_Click" style="margin-left: 18px" />
            

            </td>
            <td>

            </td>
        </tr>
          <tr>
            <td class="auto-style7">
                                <asp:Label ID="Password" runat="server" Font-Size="Large" ForeColor="White" Text="password"></asp:Label>

            </td>
            <td class="auto-style9">

                   <asp:TextBox ID="passTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            

            </td>
            <td>

            </td>
        </tr>
        
        <tr>
            <td class="auto-style7">&nbsp;</td>
            <td class="auto-style9">
                <asp:Button ID="EditButton" runat="server"  Text="Edit"  BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                            BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" ForeColor="#284775" Height="29px" Width="69px" OnClick="Edit_Click" style="margin-left: 9px" />
                <br />
                
                &nbsp;&nbsp;
            </td>
            <td>
                <br />
            </td>
        </tr>
    </table>
  </div>





    </form>
</body>
</html>

