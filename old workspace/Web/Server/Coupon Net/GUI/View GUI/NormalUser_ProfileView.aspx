﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NormalUser_ProfileView.aspx.cs" Inherits="Coupon_Net.GUI.View_GUI.NormalUser_ViewProfile" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="stylesheet" type="text/css" href="../style/toolbar_style.css" />
    
    <title></title>
     <style type="text/css">

        .auto-style3 {
            width: 714px;
            height: 375px;
        }
        .auto-style7 {
            height: 26px;
            width: 199px;
            text-align: right;
        }
        .auto-style4 {
            height: 26px;
        }
         .auto-style8 {
             height: 26px;
             width: 277px;
         }
         .auto-style9 {
             width: 277px;
         }
        </style>
</head>
<body>
     <form id="form1" runat="server">
    <div id="nav">
        <ul>
            <li style="float:left;"><a href="../Welcome GUI/Logout.aspx">Log Out</a></li>
            <li><a href="#"> Order</a></li>
            <li><a href="#"> Search </a>
            <ul>
                <li><a href="../Search GUI/NormalUser_BuisnessSearch.aspx"> Buisness </a></li>
                <li><a href="../Search GUI/NormalUser_CouponSearch.aspx"" Coupon </a></li>
            </ul></li>
            <li><a href="#"> View  </a>
                 <ul>
                <li><a href="#"  style="background:#ff6a00"> Profile </a></li>
                <li><a href="../View GUI/NormalUser_CouponView.aspx"> My Coupon </a></li>
            </ul></li>

        </ul>
    
    </div>






        <div style="padding: 50px; float:right; margin-top: 42px;">
             <asp:Image ID="Image1" runat="server" ImageUrl="~/GUI/View/title.png" style=" margin-top: 20px; margin-bottom: 70px"  />

    <p style="font:italic; color:#FFFBFF; text-align:left; margin-left:100px; font-size:30px; text-decoration:underline;" > User Information</p>
    <table class="auto-style3">
        
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label8" runat="server" Font-Size="Large" ForeColor="White" Text="Username:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_UN" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox_UN" ErrorMessage="* Username is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label7" runat="server" Font-Size="Large" ForeColor="White" Text="E-mail:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_Email" runat="server" TextMode="Email" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox_Email" ErrorMessage="* Email is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox_Email" ErrorMessage="* You Must Enter  a Vaild Email address." ForeColor="Red"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label6" runat="server" Font-Size="Large" ForeColor="White" Text="First Name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_FN" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox_FN" ErrorMessage="* First name is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label5" runat="server" Font-Size="Large" ForeColor="White" Text="Last Name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_LN" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox_LN" ErrorMessage="* Last name is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
       >

        
          <tr>
            <td class="auto-style7">
                                <asp:Label ID="Password" runat="server" Font-Size="Large" ForeColor="White" Text="password"></asp:Label>

            </td>
            <td class="auto-style9">

                   <asp:TextBox ID="passTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                <asp:Label ID="Label3" runat="server" Font-Size="Large" ForeColor="White" Text="Phone Number:"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="TextBox_phone" runat="server" TextMode="Phone" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style4">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBox_phone" ErrorMessage="* Phone number is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
       
        
        <tr>
            <td class="auto-style7">&nbsp;</td>
            <td class="auto-style9">
                <asp:Button ID="EditButton" runat="server"  Text="Edit"  BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                            BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" ForeColor="#284775" Height="29px" Width="69px" OnClick="Edit_Click" style="margin-left: 9px" />
                <br />
                
                &nbsp;&nbsp;
            </td>
            <td>
                <br />
            </td>
        </tr>
    </table>
  </div>





    </form>
</body>
</html>

