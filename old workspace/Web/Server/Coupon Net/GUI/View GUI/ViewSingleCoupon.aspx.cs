﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;

namespace Coupon_Net.GUI.View_GUI
{
    public partial class ViewSingleCoupon : System.Web.UI.Page
    {
        Coupon _myCoupon;
        List<Category> categoriesList;
        ServiceLayerClient.ServiceLayerClient serviceLayer;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                serviceLayer = new ServiceLayerClient.ServiceLayerClient();

                _myCoupon = (Coupon)Session["couponFromSearch"];
                
                if (!Page.IsPostBack)
                {
                    if (!(_myCoupon is PurchasedCoupon))
                    {
                        Serial.Visible = false;
                        SerialTB.Visible = false;
                    }

                    if ((Session["Admin"]) != null)
                    {
                        EditButton.Visible = true;
                        DeleteButton.Visible = true;
                        OrderButton.Visible = false;
                    }
                    else if (Session["BuisnessName"] != null)
                    {
                        OrderButton.Visible = false;
                        EditButton.Visible = false;
                        DeleteButton.Visible = false;
                    }

                    else
                    {
                        OrderButton.Visible = true;
                        EditButton.Visible = false;
                        DeleteButton.Visible = false;
                        if ((_myCoupon is PurchasedCoupon))
                        {
                            SerialTB.Text = ((PurchasedCoupon)_myCoupon).serial;
                        }
                    }

                    CouponName.Text = _myCoupon.name;
                    CouponDescriptionTextBox.Text = _myCoupon.description;
                    ExpirationTextBox.Text = _myCoupon.expirationDate.ToString();
                    NewPriceTextBox.Text = _myCoupon.priceAfterDiscount.ToString();
                    InitPrizeTextBox.Text = _myCoupon.originalPrice.ToString();
                    RaitingTextBox.Text = _myCoupon.rating.ToString();
                    RedirectButton0.Text = _myCoupon.businessName;

                    CouponName.Enabled = false;
                    CouponDescriptionTextBox.Enabled = false;
                    ExpirationTextBox.Enabled = false;

                    categoriesList = serviceLayer.GetCategories();


                    if (!Page.IsPostBack)
                    {
                        CategoryList.Items.Add("Select Category");
                        foreach (Category category in (categoriesList))
                        {
                            CategoryList.Items.Add(category.name);
                        }
                    }
                    CategoryList.EnableViewState = false;

                    NewPriceTextBox.Enabled = false;
                    InitPrizeTextBox.Enabled = false;
                    RaitingTextBox.Enabled = false;
                    ///need to use some function to get the right coupon;
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            if (EditButton.Text.CompareTo("Edit")==0)
            {
                CouponDescriptionTextBox.Enabled = true;
                ExpirationTextBox.Enabled = true;
                CouponName.Enabled = true;
                NewPriceTextBox.Enabled = true;
                InitPrizeTextBox.Enabled = true;
                CategoryList.EnableViewState = true;
                EditButton.Text = "Confirm";

            }
            else
            {
                CouponDescriptionTextBox.Enabled = false;
                ExpirationTextBox.Enabled = false;
                CouponName.Enabled = false;
                NewPriceTextBox.Enabled = false;
                InitPrizeTextBox.Enabled = false;
                CategoryList.EnableViewState = false;
                EditButton.Text = "Edit";

                //creating a new and up to date coupon
                Coupon newCoupon = new Coupon(CouponName.Text, _myCoupon.businessName, _myCoupon.serial,float.Parse(InitPrizeTextBox.Text),float.Parse(NewPriceTextBox.Text), (string)(Session["userName"]) );

                //editing the coupon
                try
                {
                    serviceLayer.PutEditCoupon((string)Session["userName"], _myCoupon.name, _myCoupon.businessName, newCoupon);
                }
                catch (Exception ex)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
                }

                
            }

        }

        protected void RedirectToBuisness_Click(object sender, EventArgs e)
        {
            Session["BusinessName"] = _myCoupon.businessName;
            string redirect = "<script>window.open('ViewSingleBuisness.aspx');</script>";
            Response.Write(redirect);
        }

        protected void Order_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceLayerClient.ServiceLayerClient serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                string serial = serviceLayer.orderCoupon(_myCoupon, (string)Session["userName"]);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Your Serial: " + serial + "');</script>");
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceLayerClient.ServiceLayerClient serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                serviceLayer.deleteCoupon(_myCoupon.name, (string)Session["userName"]);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
            //should close the page
        }
    }
}