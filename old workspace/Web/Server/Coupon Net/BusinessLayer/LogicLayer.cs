﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Spatial;
using System.Data.SqlClient;
using DataLAyer;
namespace BusinessLayer
{
    public class LogicLayer : IBusinessLayer
    {
        private IDataManager dataManager;
        public LogicLayer()
        {
            this.dataManager = new DataManager();
        }
        private List<Coupon> insertTableIntoCouponsList(DataTable dt)
        {
            List<Coupon> lst = new List<Coupon>();
            if (dt == null)
                return lst;
            foreach (DataRow row in dt.Rows)
            {
                Coupon c = new Coupon();
                c.name = row["couponName"].ToString();
                c.businessName = row["businessName"].ToString();
                c.description = row["description"].ToString();
                c.originalPrice = (float)row["originalPrice"];
                c.priceAfterDiscount = (float)row["priceAfterDiscount"];
                c.rating = (float)row["rating"];
                c.expirationDate = (DateTime)row["expireDate"];
                c.isApproved = (bool)row["isApproved"];
                c.itemBrand = row["brand"].ToString();
                c.adderUsername = row["addedByUser"].ToString();
                c.approverUsername = row["approvedByUser"].ToString();
                c.addedDate = (DateTime)row["addingDate"];
                lst.Add(c);
            }
            return lst;
        }
        private List<Buyer> insertTableIntoBuyersList(DataTable dt)
        {
            List<Buyer> lst = new List<Buyer>();
            if (dt == null)
                return lst;
            foreach (DataRow row in dt.Rows)
            {
                Buyer b = new Buyer();
                b.userName = row["username"].ToString();
                b.password = row["password"].ToString();
                b.email = row["email"].ToString();
                b.phoneNum = row["phoneNum"].ToString();
                lst.Add(b);
            }
            return lst;
        }
        private List<PurchasedCoupon> insertTableIntoPurchasedCouponsList(DataTable dt)
        {
            List<PurchasedCoupon> lst = new List<PurchasedCoupon>();
            if (dt == null)
                return lst;
            foreach (DataRow row in dt.Rows)
            {
                PurchasedCoupon pc = new PurchasedCoupon();
                pc.name = row["couponName"].ToString();
                pc.businessName = row["businessName"].ToString();
                pc.buyerUserName = row["buyerUsername"].ToString();
                pc.purchaseDate = (DateTime)row["pruchaseDate"];
                pc.isUsed = (bool)row["isUsed"];
                pc.serial = row["serial"].ToString();
                lst.Add(pc);
            }
            return lst;
        }

        /// <summary>
        /// login user
        /// </summary>
        /// <param name="buyerUsername">his username</param>
        /// <param name="buyerPassword">his password</param>
        /// <returns>Token that keep his login and will be used to find him, token must be unique and contain info on the usertype</returns>
        public string GetLogin(string username, string password)
        {
            DataTable dt = dataManager.GetLogin(username, password);
            return dt.Rows[0][0].ToString();
        }

        /// <summary>
        /// register buyer to the system
        /// </summary>
        /// <param name="user"> the buyer to register</param>
        /// <returns> true if success</returns>
        public bool RegisterBuyer(Buyer user)
        {
            bool check = dataManager.RegisterBuyer(user);
            if (!check)
                throw new Exception("Could not register the wanted Buyer");
            return true;
        }

        /// <summary>
        /// register businessmanager, the businessmanager must contain business else exception, to the system
        /// </summary>
        /// <param name="token">can only be done by system manager, this is the token he got when he loged in</param>
        /// <param name="user">the user to add</param>
        public bool RegisterBusinessManager(string token, BusinessManager user)
        {
            bool check = dataManager.RegisterBusinessManager(token, user);
            if (!check)
                throw new Exception("Could not register the wanted BusinessManager");
            return true;
        }

        /// <summary>
        /// register system manager to the system
        /// </summary>
        /// <param name="token">can only be done by system manager, this is the token he got when he loged in</param>
        /// <param name="user">the user to add</param>
        public bool RegisterSystemManager(string token, SystemManager user)
        {
            bool check = dataManager.RegisterSystemManager(token, user);
            if (!check)
                throw new Exception("Could not register the wanted SystemManager");
            return true;
        }

        /// <summary>
        /// Logout user from the system
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>true if sucsess or false otherwise</returns>
        public bool Logout(string token)
        {
            bool check = dataManager.Logout(token);
            if (!check)
                throw new Exception("Could not Logout");
            return true;
        }

        /// <summary>
        /// return all the coupons in radius range from location
        /// </summary>
        /// <param name="range">the radius for the search</param>
        /// <param name="location">the center</param>
        /// <returns>List of those coupons</returns>
        public List<Coupon> GetCouponWithinRange(float range, System.Data.Spatial.DbGeography location)
        {
            DataTable table = dataManager.GetCouponWithinRange(range, location);
            return insertTableIntoCouponsList(table);
        }

        /// <summary>
        /// get all coupons that belongs to given categories.
        /// </summary>
        /// <param name="category">list of categories</param>
        /// <returns>get all coupons that belongs to given categories.</returns>
        public List<Coupon> GetCouponInCategory(List<string> category)
        {
            DataTable table = dataManager.GetCouponInCategory(category);
            return insertTableIntoCouponsList(table);
        }

        /// <summary>
        /// get the friend list of user with token.
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>friend list of user with token</returns>
        public List<Buyer> GetFriendList(string token)
        {
            DataTable table = dataManager.GetFriendList(token);
            return insertTableIntoBuyersList(table);
        }

        /// <summary>
        ///  get all the friend requests of user with token.
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>all the friend requests of user with token</returns>
        public List<Buyer> GetFriendRequests(string token)
        {
            DataTable table = dataManager.GetFriendList(token);
            return insertTableIntoBuyersList(table);
        }

        /// <summary>
        /// Accept all friend request of user with token that their sender is in senders
        /// </summary>
        /// <param name="token"></param>
        /// <param name="senders"></param>
        /// <returns></returns>
        public bool AcceptFreindsRequest(string token, List<string> senders)
        {
            bool check = dataManager.AcceptFreindsRequest(token,senders);
            if (!check)
                throw new Exception("Could not Accept Friends' Requests");
            return true;
        }

        /// <summary>
        /// delete all of user with token, friend that in friends
        /// </summary>
        /// <param name="token"></param>
        /// <param name="friends">list of friend that we want to delete</param>
        /// <returns></returns>
        public bool DeleteFriends(string token, List<string> friends)
        {
            bool check = dataManager.DeleteFriends(token, friends);
            if (!check)
                throw new Exception("Could not Delete Friends");
            return true;
        }

        /// <summary>
        /// deny all friends requests of user with token, that their sender in senders
        /// </summary>
        /// <param name="token"></param>
        /// <param name="senders">list of all the sender we want to deny</param>
        public bool DenyFriendRequests(string token, List<string> senders)
        {
            bool check = dataManager.DenyFriendRequests(token,senders);
            if (!check)
                throw new Exception("Could not deny Friends' Requests");
            return true;
        }

        /// <summary>
        /// user with token buy coupon
        /// </summary>
        /// <param name="token">the token of the buying user</param>
        /// <param name="coupon">the coupon he bought</param>
        /// <returns>if sucess return true else false</returns>
        public bool BuyCoupon(string token, Coupon coupon)
        {
            bool check = dataManager.BuyCoupon(token,coupon);
            if (!check)
                throw new Exception("Could not buy Coupon");
            return true;
        }

        /// <summary>
        /// Get all coupon that was bougth by user with token and have one week to be expires
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public List<PurchasedCoupon> GetPressingCoupons(string token)
        {
            DataTable table = dataManager.GetPressingCoupons(token);
            return insertTableIntoPurchasedCouponsList(table);
        }

        /// <summary>
        /// get all the coupons that user with token has bougth
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public List<PurchasedCoupon> GetPruchasedHistory(string token)
        {
            DataTable table = dataManager.GetPruchasedHistory(token);
            return insertTableIntoPurchasedCouponsList(table);
        }

        /// <summary>
        /// Edit buyer data
        /// </summary>
        /// <param name="token">his token</param>
        /// <param name="newData">the new data that will override the old one</param>
        public bool EditBuyerData(string token, Buyer newData)
        {
            bool check = dataManager.EditBuyerData(token, newData);
            if (!check)
                throw new Exception("Could not edit Buyer's Data");
            return true;
        }

        /// <summary>
        /// Get list of couon that friend has bougth
        /// </summary>
        /// <param name="token"></param>
        /// <param name="friend">the friend</param>
        /// <returns></returns>
        public List<PurchasedCoupon> GetFriendPruchasedCoupon(string token, string friend)
        {
            DataTable table = dataManager.GetFriendPruchasedCoupon(token,friend);
            return insertTableIntoPurchasedCouponsList(table);
        }

        /// <summary>
        /// send friend request from user with token to toUser
        /// </summary>
        /// <param name="token">token of sender user</param>
        /// <param name="toUsers">the reciver of the request</param>
        /// <returns></returns>
        public bool SendFreindsRequests(string token, List<string> toUsers)
        {
            bool check = dataManager.SendFreindsRequests(token, toUsers);
            if (!check)
                throw new Exception("Could not send friends' Requests");
            return true;
        }

        /// <summary>
        /// Add coupon
        /// </summary>
        /// <param name="token">token of user that added the coupon</param>
        /// <param name="coupon">the coupon to be added</param>
        public bool AddCoupon(string token, Coupon coupon)
        {
            bool check = dataManager.AddCoupon(token, coupon);
            if (!check)
                throw new Exception("Could not add Coupon");
            return true;
        }

        /// <summary>
        /// set coupon as used
        /// </summary>
        /// <param name="token">need to be the business Manager whitch his business accept the coupon</param>
        /// <param name="serial">the serial of the deal</param>
        public bool SetCouponAsUsed(string token, string serial)
        {
            bool check = dataManager.SetCouponAsUsed(token, serial);
            if (!check)
                throw new Exception("Could not set Coupon as used");
            return true;
        }

        /// <summary>
        /// Edit business
        /// </summary>
        /// <param name="token">token of the business manager</param>
        /// <param name="business">the new data that will override the old</param>
        public bool EditBusinessData(string token, string oldBusinessName, Business business)
        {
            bool check = dataManager.EditBusinessData(token,oldBusinessName,business);
            if (!check)
                throw new Exception("Could not edit business data");
            return true;
        }

        /// <summary>
        /// Edit business Manager
        /// </summary>
        /// <param name="token">of the business manager</param>
        /// <param name="manager">the new data that will override the old one</param>
        public bool EditBusinessManagerData(string token, string oldBusinessManagerUserName, BusinessManager manager)
        {
            bool check = dataManager.EditBusinessManagerData(token, oldBusinessManagerUserName, manager);
            if (!check)
                throw new Exception("Could not edit business dataManager");
            return true;
        }

        /// <summary>
        /// Edit coupon, WARNING token must belong to system manager
        /// </summary>
        /// <param name="token">of system manager</param>
        /// <param name="couponName">whitch coupon</param>
        /// <param name="businessName">The business of this coupon</param>
        /// <param name="newData">the new data that will override the old one</param>
        public bool EditCoupon(string token, string couponName,string businessName, Coupon newData)
        {
            bool check = dataManager.EditCoupon(token, couponName, businessName, newData);
            if (!check)
                throw new Exception("Could not edit Coupon data");
            return true;
        }

       /* /// <summary>
        /// Add coupon o business businessName.
        /// </summary>
        /// <param name="token">the businessmanager of this business</param>
        /// <param name="businessName">the business</param>
        /// <param name="coupon">the coupon to be added</param>
        public bool AddCoupon(string token, string businessName, Coupon coupon)
        {
            bool check = dataManager.AddCoupon(token, businessName, coupon);
            if (!check)
                throw new Exception("Could not set edit Coupon data");
            return true;
        }*/

        /// <summary>
        /// set coupon as Approved, WARNING token must be belong to system manger
        /// </summary>
        /// <param name="token">of system manager that approves it</param>
        /// <param name="businessName">the business of the coupon</param>
        /// <param name="couponName">  the name of the coupon</param>
        public bool SetCouponAsApproved(string token, string businessName, string couponName)
        {
            bool check = dataManager.SetCouponAsApproved(token, businessName, couponName);
            if (!check)
                throw new Exception("Could not set Coupon as approved");
            return true;
        }

        /// <summary>
        /// Get list of all the unapproved coupon, Token must belong to system manager
        /// </summary>
        /// <param name="token">of system manager</param>
        /// <returns>list of all the unapproved coupons</returns>
        public List<Coupon> GetUnapprovedCoupons(string token)
        {
            DataTable table = dataManager.GetUnapprovedCoupons(token);
            return insertTableIntoCouponsList(table);
        }

        /// <summary>
        /// Add category to the system, must be done by system manager ==> token belong to system manager
        /// </summary>
        /// <param name="token">of system manger</param>
        /// <param name="category">the category to be added</param>
        public bool AddCategory(string token, Category category)
        {
            bool check = dataManager.AddCategory(token, category);
            if (!check)
                throw new Exception("Could not add the desired category");
            return true;
        }

        /// <summary>
        /// edit data of system manger
        /// </summary>
        /// <param name="token">the token of the system manager that we will edit</param>
        /// <param name="manager">the new data that will overrdie the old one</param>
        public bool EditSystemManagerData(string token, SystemManager manager)
        {
            bool check = dataManager.EditSystemManagerData(token, manager);
            if (!check)
                throw new Exception("Could not edit the systemManager data");
            return true;
        }

        /// <summary>
        /// Get all the pruchased coupon that was bougth in bussiness belong to user businessmanager with token.
        /// MUST NOT return the serial of those deals!!!
        /// </summary>
        /// <param name="token">of businessmanager that manage the business</param>
        /// <returns></returns>
        public List<PurchasedCoupon> GetPruchasedCoupon(string token)
        {
            DataTable table = dataManager.GetPruchasedCoupon(token);
            return insertTableIntoPurchasedCouponsList(table);
        }

    }
}
