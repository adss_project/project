﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceLayerClient
{
    public class OperationFailedException : Exception
    {
        public OperationFailedException(string msg) : base(msg) { }
    }
}
