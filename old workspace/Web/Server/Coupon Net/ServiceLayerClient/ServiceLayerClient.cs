﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayerClient
{
    public class ServiceLayerClient
    {
        private static readonly string baseAddress = "http://localhost:9020/";
       
        private static string _token = null;

        // Create HttpCient and make a request to api/values 
        private HttpClient client = new HttpClient();


        public string GetLogin(string buyerUsername, string buyerPassword)
        {
            var response = client.GetAsync(baseAddress + "api/login/GetLogin/"+buyerUsername+"/"+buyerPassword).Result;
            if (response.IsSuccessStatusCode)
            {
                List<string> returnList = JsonConvert.DeserializeObject<List<string>>(response.Content.ReadAsStringAsync().Result);
                if (returnList == null || returnList.Count < 2)
                {
                    throw new OperationFailedException("Username or password was invalid.");
                }
                _token = returnList[0];
                return returnList[1];
            }
            else
            {
                throw new OperationFailedException("Error has occurd during registration.");
            }
        }

        public void Logout(string token)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/login/PutLogout/",_token).Result;
            if (response.IsSuccessStatusCode)
            {
                _token = null;
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public bool BuyerRegistretion(Buyer user)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/login/PutBuyerRegistretion/", user).Result;
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                throw new OperationFailedException("Error has occurd during registration.");
            }
        }
   

        public List<Coupon> GetCouponWithinRange(float range, DbGeography location)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetCouponWithinRange?range=" + range + "&lat=" + location.Latitude + "&longtidue=" + location.Longitude).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
          
        }

        public List<Coupon> GetCouponWithinRange(float range, float latitude, float longitude)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetCouponWithinRange?range=" + range + "&lat=" + latitude + "&longtidue=" + longitude).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        /*****************************************************************************************************************/
        public List<Coupon> GetCouponInCategory(List<Category> Category)
        {
            string list = "?";
            foreach(Category c in Category){
                list += ("Category=" + c.name +"&");
            }
            list.Remove(list.Length - 1);
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetCouponInCategory" + list).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<Buyer> GetFriendList(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetFriendList?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Buyer> returnList = JsonConvert.DeserializeObject<List<Buyer>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<Buyer> GetFriendRequests(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetFriendRequests?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Buyer> returnList = JsonConvert.DeserializeObject<List<Buyer>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutSendFreindsRequests(string token, List<string> toUsers)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutSendFreindsRequests?token=" + token, toUsers).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutAcceptFreindsRequest(string token, List<string> senders)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutAcceptFreindsRequest?token=" + token, senders).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutDeleteFriends(string token, List<string> friends)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutDeleteFriends?token=" + token, friends).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutDenyFriendRequests(string token, List<string> senders)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutDenyFriendRequests?token=" + token, senders).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public string PutBuyCoupon(string token, Coupon coupon)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutBuyCoupon?token=" + token, coupon).Result;
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result);
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<PurchasedCoupon> GetPressingCoupon(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetPressingCoupons?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<PurchasedCoupon> returnList = JsonConvert.DeserializeObject<List<PurchasedCoupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<PurchasedCoupon> GetPruchasedHistory(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetPruchasedHistory?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<PurchasedCoupon> returnList = JsonConvert.DeserializeObject<List<PurchasedCoupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEdit(string token, Buyer newData)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutEdit?token=" + token, newData).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<PurchasedCoupon> GetFriendPurchasedCoupon(string token, string friend)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetFriendPruchasedCoupon?token=" + token + "&friend=" + friend).Result;
            if (response.IsSuccessStatusCode)
            {
                List<PurchasedCoupon> returnList = JsonConvert.DeserializeObject<List<PurchasedCoupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutAddCoupon(string token, Coupon coupon)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbusiness/PutAddCoupon?token=" + token, coupon).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutSetCouponAsUsed(string token, string serial)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbusiness/PutSetCouponAsUsed?token=" + token + "&serial="+ serial,0).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEditBusinessData(string token, Business business){
            var response = client.PutAsJsonAsync(baseAddress + "api/userbusiness/PutEditBusinessData?token=" + token ,business).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEditBusinessManagerData(string token, BusinessManager manager){
            var response = client.PutAsJsonAsync(baseAddress + "api/userbusiness/PutEditBusinessManagerData?token=" + token ,manager).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<PurchasedCoupon> GetPurchasedCoupon(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbusiness/GetPruchasedCoupon?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<PurchasedCoupon> returnList = JsonConvert.DeserializeObject<List<PurchasedCoupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }
        public void PutAddCoupon(string token, string businessName, Coupon coupon)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutAddCoupon?token=" + token, coupon).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutSetCouponAsApproved(string token, string businessName, string couponName)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutSetCouponAsApproved?token=" + token +
                                                                "&businessName=" + businessName + "&couponName=" + couponName, 0).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<Coupon> GetUnapprovedCoupon(string token)
        {
            var response = client.GetAsync(baseAddress + "api/usersystemmanager/GetUnapprovedCoupons?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutBusinessManagerRegistretion(string token, BusinessManager user)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutBusinessManagerRegistretion?token=" + token, user).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutSystemManagerRegistretion(string token, SystemManager user){
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutSystemManagerRegistretion?token=" + token, user).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }
        public void PutAddCategory(string token, Category category)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutAddCategory?token=" + token, category).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEditData(string token, SystemManager manager)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutEditData?token=" + token, manager).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEditCoupon(string token, string couponName, string businessName, Coupon newData)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutEditCoupon?token=" + token +
                                                                    "&couponName=" + couponName + "&businessName=" + businessName, newData).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<Category> GetCategories()
        {
            var response = client.GetAsync(baseAddress + "api/login/GetCategories").Result;
            if (response.IsSuccessStatusCode)
            {
                List<Category> returnList = JsonConvert.DeserializeObject<List<Category>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }
       /// <summary>
       /// ///////////////////////////////////
       /// </summary>
       /// <param name="coupon"></param>
       /// <param name="userName"></param>
        public string orderCoupon(Coupon coupon, string userName)
        {
            return PutBuyCoupon(userName, coupon);
        }

        public void deleteCoupon(string couponName, string userName)
        {

        }
        public void AddBusiness(string userName, Business businesstoAdd)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/AddBusiness?token=" + userName, businesstoAdd).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }
        public List<Business> GetBusiness(string bussinesName, string category, string t)
        {
            var response = client.GetAsync(baseAddress + "api/login/GetBussiness?bussinesName=" + bussinesName +
                "&category=" + category ).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Business> returnList = JsonConvert.DeserializeObject<List<Business>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Buissness Not Found.");
                }
            }
            else
            {
                throw new OperationFailedException("Server Error.");
            }
        }


        public List<Coupon> GetCoupon(string bussinesName, string category, string t) //need to add reference to spacial but we dont know from where !!!
        {
            var response = client.GetAsync(baseAddress + "api/login/GetCoupon?bussinesName=" + bussinesName +
               "&category=" + category).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Business Not Found.");
                }
            }
            else
            {
                throw new OperationFailedException("Server Error.");
            }
        }

        public string Login(string password, string userName) {
            return GetLogin(userName, password); 
        }

        public void deleteBusiness(string businessName, string userName)
        {
            //TODO
        }

        public Buyer getBuyer(string buyerName)
        {
            var response = client.GetAsync(baseAddress + "api/login/GetBuyer?buyerName=" + buyerName).Result;
            if (response.IsSuccessStatusCode)
            {
                Buyer returnList = JsonConvert.DeserializeObject<Buyer>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Buyer Not Found.");
                }
            }
            else
            {
                throw new OperationFailedException("Server Error.");
            }
        }

        public BusinessManager GetBusinessManager(string businessName)
        {
            var response = client.GetAsync(baseAddress + "api/login/GetBusinessManager?businessName=" + businessName).Result;
            if (response.IsSuccessStatusCode)
            {
                BusinessManager returnList = JsonConvert.DeserializeObject<BusinessManager>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("BusinessManager Not Found.");
                }
            }
            else
            {
                throw new OperationFailedException("Server Error.");
            }
        }
        public BusinessManager GetBusinessManager()
        {
            return GetBusinessManager(_token);
        }
        
    }
}

/*
 * צריך לשנות לביזנס במקום מספר קטגוריות לקטכוריה אחת
 * 
*/