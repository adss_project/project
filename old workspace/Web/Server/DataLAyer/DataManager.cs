﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Entities;
using System.Data.Spatial;
using System.IO;
using System.Reflection;
namespace DataLAyer
{
    public class DataManager : IDataManager
    {
        private const string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\CouponDB.mdf;Integrated Security=True";
        private SqlConnection connection;
        public DataManager()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            this.connection = new SqlConnection(connectionString);
        }
        /**
         * 
         * 
         * it should contain calls of queries from the DB and 
         * And returns collection of CONCRETE OBJECTS (from the Entities Layer)
         * This static class will be used in the business layer
         * 
         * VERY IMPORTENT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         * NOTE: all of the functions here should be based on stored procedured from the DB
         * That means that if you want to use a queary and there is no stored procedured for it in the DB, then you should add it to there....
         * SEE the DB file: in stored procedured folder, there are some completed queries there.....
         * Note: every function you write in here, should have a signature in its interface...
         * so add it in the interface as well
         * 
         * */
        private bool IsT(string token, string T)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Login Where username='{0}' AND isLogin='{1}' AND type='{2}'", token, true, T);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter1 = new SqlDataAdapter(sqlCmd);
            if (dataAdapter1.Fill(table) <= 0)
                return false;
            return true;
        }
        private bool IsDataManager(string token)
        {
            return IsT(token, "S");
        }
        private bool IsBusinessManager(string token)
        {
            return IsT(token, "M");
        }
        private bool IsBuyer(string token)
        {
            return IsT(token, "B");
        }
        private DataTable CategoriesCollectionToDataTable(List<Category> collection)
        {
            DataTable table = new DataTable();
            table.Columns.Add("category", typeof(string));
            foreach (Category category in collection)
            {
                table.Rows.Add(category.ToString());
            }
            return table;
        }

        public DataTable GetLogin(string userName, string password)
        {
            DataTable result = new DataTable();
            DataTable table = new DataTable();
            SqlCommand sqlCmd;
            SqlDataAdapter dataAdapter;
            StringBuilder cmdStr = new StringBuilder();
            /*cmdStr.AppendFormat("SELECT * From [dbo].[Login] L  WHERE L.username = '" + userName + "' AND L.[password] = '" + password + "' AND L.isLogin = 0");
            sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            */
            connection.Open();
            cmdStr = new StringBuilder();
            cmdStr.AppendFormat("UPDATE [dbo].[Login] SET isLogin = 1,LoginToken = '" + userName + "' WHERE username = '" + userName + "' AND [Login].password = '" + password + "'");
            sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            sqlCmd.ExecuteNonQuery();


            cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT Login.LoginToken, Login.Type From Login WHERE username = '" + userName + "' AND [Login].password = '" + password + "'");
            sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(result) <= 0)
                return null;


            return result;
        }

        public bool RegisterBuyer(Buyer user)
        {
            using (var cmd = new SqlCommand("RegisterBuyer", connection))
            {
                try
                {
                    connection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = user.userName;
                    cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = user.password;
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = user.email;
                    cmd.Parameters.Add("@phonenum", SqlDbType.VarChar).Value = user.phoneNum;
                    cmd.Parameters.AddWithValue("@hobbies", CategoriesCollectionToDataTable(user.preferences));
                    bool returnValue = (cmd.ExecuteNonQuery() > 0);
                    return returnValue;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        public bool RegisterBusinessManager(string token, BusinessManager user)
        {
            if (!IsDataManager(token))
                return false;
            Business business = user.business;
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("Select * From [dbo].[Categories] C WHERE C.category = '" + user.business.categories[0] + "'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
            {
                return false;
            }
            
            using (var cmd = new SqlCommand("RegisterBusiness", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@businessManagerUsername", user.userName);
                cmd.Parameters.AddWithValue("@businessManagerPassword", user.password);
                cmd.Parameters.AddWithValue("@businessManagerEmail", user.email);
                cmd.Parameters.AddWithValue("@businessManagerPhone", user.phoneNum);
                cmd.Parameters.AddWithValue("@businessName", business.businessName);
                cmd.Parameters.AddWithValue("@businessDescription", business.description);
                cmd.Parameters.AddWithValue("@businessCity", business.address.city);
                cmd.Parameters.AddWithValue("@businessAddress", business.address.ToString());
                cmd.Parameters.AddWithValue("@businessLatitude", business.latitude);
                cmd.Parameters.AddWithValue("@businessLongitude", business.longitude);

                if (cmd.ExecuteNonQuery() <= 0)
                {
                    return false;
                }
            }
            table = new DataTable();
            cmdStr = new StringBuilder();
            cmdStr.AppendFormat("INSERT INTO BusinessCategories VALUES ('" + business.businessName + "','" + user.business.categories[0].name + "')");
            sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
            {
                return false;
            }
            return true;
        }
        public bool RegisterSystemManager(string token, SystemManager user)
        {
            if (!IsDataManager(token))
                return false;
            using (var cmd = new SqlCommand("RegisterSystemManager", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = user.userName;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = user.password;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = user.email;
                cmd.Parameters.Add("@phonenum", SqlDbType.VarChar).Value = user.phoneNum;
                return (cmd.ExecuteNonQuery() > 0);
            }
        }
        public bool Logout(string token)
        {
            using (var cmd = new SqlCommand("Logout", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = token;
                return (cmd.ExecuteNonQuery() > 0);
            }
        }
        public DataTable GetCouponWithinRange(float range, DbGeography location)
        {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("	SELECT Coupons.couponName,Coupons.businessName,Coupons.description,Coupons.originalPrice, 			Coupons.priceAfterDiscount,Coupons.rating,Coupons.brand,Coupons.expireDate,Business.location 	FROM Coupons , Business 	Where Coupons.businessName = Business.name AND Coupons.isApproved = 'True'  		  AND '{0}'.STDistance(Business.location) < '{1}' AND Coupons.expireDate >= GETDATE()", location,range);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            dataAdapter.Fill(table);

            return table;
        }
        public DataTable GetCouponInCategory(List<string> categories)
        {
            DataTable table = new DataTable();
            foreach (string category in categories)
            {
                DataTable tmp = new DataTable();

                StringBuilder cmdStr = new StringBuilder();
                cmdStr.AppendFormat("SELECT Coupons.couponName,Coupons.businessName,Coupons.description,Coupons.originalPrice, 			Coupons.priceAfterDiscount,Coupons.rating,Coupons.brand,Coupons.expireDate,Business.location 	FROM Coupons,Business,CouponCategories 	WHERE Coupons.businessName = Business.name AND 		  Coupons.couponName = CouponCategories.couponName AND 		  Coupons.businessName = CouponCategories.businessName AND 		  Coupons.isApproved = 'True' AND '{0}' = CouponCategories.category AND Coupons.expireDate >= GETDATE()", category);
                SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
                SqlDataAdapter dataAdapter1 = new SqlDataAdapter(sqlCmd);
                dataAdapter1.Fill(tmp);
                table.Merge(tmp);
            }
            return table;
        }
        public DataTable GetFriendList(string token)
        {

            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT friendUsername 	FROM BuyersFriend 	WHERE buyerUsername = '{0}'", token);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;

        }
        public DataTable GetFriendRequests(string token)
        {

            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * 	FROM FriendRequest 	WHERE FriendRequest.buyerUsername = '{0}'", token);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            dataAdapter.Fill(table);
            return table;

        }
        public bool AcceptFreindsRequest(string token, List<string> senders)
        {
            bool check = true;
            using (var cmd = new SqlCommand("AcceptFreindRequest", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string sender in senders)
                {
                    cmd.Parameters.AddWithValue("@buyerUsername", token);
                    cmd.Parameters.AddWithValue("@senderUsername", sender);
                    check = check & (cmd.ExecuteNonQuery() > 0);
                }
            }
            return check;
        }
        public bool DeleteFriends(string token, List<string> friends)
        {
            bool check = true;
            using (var cmd = new SqlCommand("DeleteFriend", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string sender in friends)
                {
                    cmd.Parameters.AddWithValue("@buyerUsername", token);
                    cmd.Parameters.AddWithValue("@friendUsername", sender);
                    check = check & (cmd.ExecuteNonQuery() > 0);
                }
            }
            return check;
        }
        public bool DenyFriendRequests(string token, List<string> senders)
        {
            bool check = true;
            using (var cmd = new SqlCommand("DenyFreindRequest", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string sender in senders)
                {
                    cmd.Parameters.AddWithValue("@buyerUsername", token);
                    cmd.Parameters.AddWithValue("@senderUsername", sender);
                    check = check & (cmd.ExecuteNonQuery() > 0);
                }
            }
            return check;
        }
        public string BuyCoupon(string token, Coupon coupon)
        {
            bool check = true;
            using (var cmd = new SqlCommand("BuyCoupon", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@couponName", coupon.name);
                cmd.Parameters.AddWithValue("@businessName", coupon.businessName);
                cmd.Parameters.AddWithValue("@buyerUsername", token);
                if (coupon.serial == null || coupon.serial == "" || coupon.serial == "" || coupon.serial == " " || coupon.serial == "0")
                {
                    coupon.serial = (Guid.NewGuid()).ToString();
                }
                cmd.Parameters.AddWithValue("@serial", coupon.serial);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return coupon.serial;
        }

        public DataTable GetPressingCoupons(string token)
        {

            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * 	FROM Coupons C, PruchasedCoupon PC 	WHERE C.businessName = PC.businessName  AND 		  C.couponName	 = PC.couponName    AND 		  PC.buyerUsername = '{0}' AND 		  GETDATE() <= C.expireDate", token);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            dataAdapter.Fill(table);
            return table;

        }
        public DataTable GetPruchasedHistory(string token)
        {

            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * 	FROM PruchasedCoupon 	WHERE PruchasedCoupon.buyerUsername = '{0}'", token);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            dataAdapter.Fill(table);
            return table;


        }
        public bool EditBuyerData(string token, Buyer newData)
        {
            bool check = true;
            using (var cmd = new SqlCommand("EditBuyer", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", token);
                cmd.Parameters.AddWithValue("@username", newData.userName);
                cmd.Parameters.AddWithValue("@password", newData.password);
                cmd.Parameters.AddWithValue("@email", newData.email);
                cmd.Parameters.AddWithValue("@phonenum", newData.phoneNum);
                cmd.Parameters.AddWithValue("@hobbies", CategoriesCollectionToDataTable(newData.preferences));
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        public DataTable GetFriendPruchasedCoupon(string token, string friend)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Login Where username='{0}' AND isLogin='{1}' AND type='{2}'", token, true, "B");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return GetPruchasedHistory(friend);
        }
        public bool SendFreindsRequests(string token, List<string> toUsers)
        {
            bool check = true;
            using (var cmd = new SqlCommand("AddToFrinedRequests", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string toUser in toUsers)
                {
                    cmd.Parameters.AddWithValue("@buyerUsername", toUser);
                    cmd.Parameters.AddWithValue("@senderUsername", token);
                    check = check & (cmd.ExecuteNonQuery() > 0);
                }
            }
            return check;
        }
        public bool AddCoupon(string token, Coupon coupon)
        {
            if ((!IsBusinessManager(token)) && (!IsDataManager(token)))
            {
                return false;
            }
            if (coupon.categories != null && coupon.categories.Count > 0)
            {
                bool check = true;
                DataTable table = new DataTable();
                StringBuilder cmdStr = new StringBuilder();
                cmdStr.AppendFormat("Select * From [dbo].[Categories] C WHERE C.category = '" + coupon.categories[0] + "'", token, true, "B");
                SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
                if (dataAdapter.Fill(table) <= 0)
                {
                    return false;
                }
                table = new DataTable();
                cmdStr = new StringBuilder();
                cmdStr.AppendFormat("INSERT INTO Coupons VALUES('" + coupon.name + "','" + coupon.businessName + "','" + coupon.description +
                    "','" + coupon.originalPrice + "','" + coupon.priceAfterDiscount + "','" + coupon.rating + "','" + coupon.expirationDate +
                        "','" + coupon.isApproved + "','" + coupon.itemBrand + "','" + coupon.adderUsername + "',NULL,GETDATE())");
                sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
                dataAdapter = new SqlDataAdapter(sqlCmd);
                if (dataAdapter.Fill(table) <= 0)
                {
                    return false;
                }

                table = new DataTable();
                cmdStr = new StringBuilder();
                cmdStr.AppendFormat("INSERT INTO CouponCategories VALUES ('"+coupon.name+"','"+coupon.businessName+"','"+coupon.categories[0]+"')");
                sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
                dataAdapter = new SqlDataAdapter(sqlCmd);
                if (dataAdapter.Fill(table) <= 0)
                {
                    return false;
                }
                return true;
            }
            return false;
        }
        public bool SetCouponAsUsed(string token, string serial)
        {
            if (!IsBusinessManager(token))
                return false;
            bool check = true;
            using (var cmd = new SqlCommand("SetCouponAsUsed2", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@serial", serial);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        public bool EditBusinessData(string token, string oldBusinessName, Business business)
        {
            if (!IsBusinessManager(token))
                return false;
            bool check = true;
            using (var cmd = new SqlCommand("EditBusinessData", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", oldBusinessName);
                cmd.Parameters.AddWithValue("@name", business.businessName);
                cmd.Parameters.AddWithValue("@managerUsername", business.manager);
                cmd.Parameters.AddWithValue("@description", business.description);
                cmd.Parameters.AddWithValue("@city", business.address.city);
                cmd.Parameters.AddWithValue("@address", business.address.ToString());
                cmd.Parameters.AddWithValue("@location", null);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        public bool EditBusinessManagerData(string token, string oldBusinessManagerUserName, BusinessManager manager)
        {
            if (!IsDataManager(token))
                return false;
            bool check = true;
            using (var cmd = new SqlCommand("EditBusinessManagerData", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", oldBusinessManagerUserName);
                cmd.Parameters.AddWithValue("@username", manager.userName);
                cmd.Parameters.AddWithValue("@password", manager.password);
                cmd.Parameters.AddWithValue("@email", manager.email);
                cmd.Parameters.AddWithValue("@phoneNumber", manager.phoneNum);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        public bool EditSystemManagerData(string token, SystemManager manager)
        {
            if (!IsDataManager(token))
                return false;
            bool check = true;
            using (var cmd = new SqlCommand("EditBusinessManagerData", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", token);
                cmd.Parameters.AddWithValue("@username", manager.userName);
                cmd.Parameters.AddWithValue("@password", manager.password);
                cmd.Parameters.AddWithValue("@email", manager.email);
                cmd.Parameters.AddWithValue("@phoneNumber", manager.phoneNum);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }

        public bool EditCoupon(string token, string couponName, string businessName, Coupon newData)
        {
            if (!IsDataManager(token))
                return false;

            bool check = true;
            using (var cmd = new SqlCommand("EditCouponData", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@oldCouponName", couponName);
                cmd.Parameters.AddWithValue("@businessName", businessName);
                cmd.Parameters.AddWithValue("@couponName", newData.name);
                cmd.Parameters.AddWithValue("@description", newData.description);
                cmd.Parameters.AddWithValue("@originalPrice", newData.originalPrice);
                cmd.Parameters.AddWithValue("@priceAfterDiscount", newData.priceAfterDiscount);
                cmd.Parameters.AddWithValue("@rating", newData.rating);
                cmd.Parameters.AddWithValue("@expireDate", newData.expirationDate);
                cmd.Parameters.AddWithValue("@isApproved", newData.isApproved);
                cmd.Parameters.AddWithValue("@brand", newData.itemBrand);
                cmd.Parameters.AddWithValue("@addedByUser", newData.adderUsername);
                cmd.Parameters.AddWithValue("@approvedByUser", newData.approverUsername);
                cmd.Parameters.AddWithValue("@addingDate", newData.addedDate);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        public bool SetCouponAsApproved(string token, string businessName, string couponName)
        {
            if (!IsDataManager(token))
                return false;

            bool check = true;
            using (var cmd = new SqlCommand("ApproveCoupon", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@couponName", couponName);
                cmd.Parameters.AddWithValue("@businessName", businessName);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        public DataTable GetUnapprovedCoupons(string token)
        {
            if (!IsDataManager(token))
                return null;

            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Coupons 	WHERE Coupons.isApproved = 0");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            dataAdapter.Fill(table);
            return table;

        }
        public bool AddCategory(string token, Category category)
        {
            if (!IsDataManager(token))
                return false;

            bool check = true;
            using (var cmd = new SqlCommand("AddCategory", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@category", category.name);
                cmd.Parameters.AddWithValue("@description", category.description);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        public DataTable GetPruchasedCoupon(string token)
        {

            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT PC.buyerUsername,PC.couponName,PC.pruchaseDate, PC.isUsed 	FROM PruchasedCoupon PC, Business b 	WHERE b.managerUsername='{0}' AND PC.businessName = b.name", token);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            dataAdapter.Fill(table);
            return table;
        }

        public DataTable GetCategories()
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Categories");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable GetBusiness()
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Business");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable GetBusinessByCategory(string category)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Business B, BusinessCategories BC WHERE B.name = BC.businessName AND BC.category ="+category);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable GetBusinessByName(string bussinesName)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Business B WHERE B.name ='"+bussinesName + "'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable GetBusiness(string bussinesName, string category)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Business B, BusinessCategories BC WHERE B.name = BC.businessName AND B.name = '" +bussinesName+"' AND BC.category ='" + category+"'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable GetCoupons()
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Coupons");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable GetCouponInCategory(string category)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Coupons C,CouponCategories CC WHERE C.couponName = CC.couponName" +   
                                    " AND C.businessName = CC.businessName AND CC.category = '"+category+"'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable GetCoupons(string bussinesName)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Coupons C WHERE C.businessName ='" + bussinesName + "'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public  DataTable GetCoupons(string bussinesName, string category)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Coupons C,CouponCategories CC WHERE C.couponName = CC.couponName" +
                                    " AND C.businessName = CC.businessName AND CC.category = '" + category + "' AND C.businessName ='" + bussinesName + "'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable GetBuyer(string name)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Buyers Where Buyers.username ='"+name+"'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }

        public DataTable BusinessManager(string name)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM BusinessManager Where BusinessManager.username ='" + name + "'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }


        public DataTable GetBusinessByManagerName(string name)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Business Where Business.managerUsername ='" + name + "'");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return table;
        }
    }
}



