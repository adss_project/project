﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Class1 : IBusinessLayer
    {
        /// <summary>
        /// login user
        /// </summary>
        /// <param name="buyerUsername">his username</param>
        /// <param name="buyerPassword">his password</param>
        /// <returns>Token that keep his login and will be used to find him, token must be unique and contain info on the usertype</returns>
        public List<string> GetLogin(string Username, string Password)
        {
            return new List<string>();
        }

        /// <summary>
        /// register buyer to the system
        /// </summary>
        /// <param name="user"> the buyer to register</param>
        /// <returns> true if success</returns>
        public bool RegisterBuyer(Buyer user)
        {
            if (user == null)
                throw new Exception();
            return false;
        }

        /// <summary>
        /// register businessmanager, the businessmanager must contain business else exception, to the system
        /// </summary>
        /// <param name="token">can only be done by system manager, this is the token he got when he loged in</param>
        /// <param name="user">the user to add</param>
        public bool RegisterBusinessManager(string token, BusinessManager user)
        {
            if (user == null)
                throw new Exception();
            if (token != "TOKEN")
                throw new Exception();
            return true;


        }

        /// <summary>
        /// register system manager to the system
        /// </summary>
        /// <param name="token">can only be done by system manager, this is the token he got when he loged in</param>
        /// <param name="user">the user to add</param>
        public bool RegisterSystemManager(string token, SystemManager user)
        {
            if (user == null)
                throw new Exception();
            if (token != "TOKEN")
                throw new Exception();
            return true;

        }

        /// <summary>
        /// Logout user from the system
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>true if sucsess or false otherwise</returns>
        public bool Logout(string token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// return all the Coupon in radius range from location
        /// </summary>
        /// <param name="range">the radius for the search</param>
        /// <param name="location">the center</param>
        /// <returns>List of those Coupon</returns>
        public List<Coupon> GetCouponWithinRange(float range, System.Data.Spatial.DbGeography location)
        {
            return getCouponList();
        }


        private List<Coupon> getCouponList()
        {
            List<Coupon> c = new List<Coupon>();
            c.Add(new Coupon("A1","B1","AAAA"));
            c.Add(new Coupon( "A2","B2","A"));
            c.Add(new Coupon( "A3","B3" ,"5"));
            c.Add(new Coupon( "A4", "B4" ,"yyy"));
            c.Add(new Coupon("A5", "B5" ,"6"));
            c.Add(new Coupon("A6", "B6","85"));
            return c;
        }

        public List<PurchasedCoupon> GetPruchasedCoupon(string token)
        {
            if (token == "TOKEN")
            {
                List<PurchasedCoupon> c = new List<PurchasedCoupon>();
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                return c;
            }
            else throw new Exception();
        }

        private List<PurchasedCoupon> getDealList(string token)
        {
            if (token == "TOKEN")
            {
                List<PurchasedCoupon> c = new List<PurchasedCoupon>();
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                c.Add(new PurchasedCoupon());
                return c;
            }
            else throw new Exception();
        }

        private List<Buyer> getBuyerList(string token)
        {
            if (token == "TOKEN")
            {
                List<Buyer> c = new List<Buyer>();
                c.Add(new Buyer());
                c.Add(new Buyer());
                c.Add(new Buyer());
                c.Add(new Buyer());
                c.Add(new Buyer());
                c.Add(new Buyer());
                return c;
            }
            else throw new Exception();
        }

        /// <summary>
        /// get all Coupon that belongs to given categories.
        /// </summary>
        /// <param name="category">list of categories</param>
        /// <returns>get all Coupon that belongs to given categories.</returns>
        public List<Coupon> GetCouponInCategory(List<string> category)
        {
            if (category == null)
                throw new Exception();
            return getCouponList();
        }

        /// <summary>
        /// get the friend list of user with token.
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>friend list of user with token</returns>
        public List<Buyer> GetFriendList(string token)
        {
            return getBuyerList(token);
        }

        /// <summary>
        ///  get all the friend requests of user with token.
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>all the friend requests of user with token</returns>
        public List<Buyer> GetFriendRequests(string token)
        {
            return getBuyerList(token);
        }

        /// <summary>
        /// Accept all friend request of user with token that their sender is in senders
        /// </summary>
        /// <param name="token"></param>
        /// <param name="senders"></param>
        /// <returns></returns>
        public bool AcceptFreindsRequest(string token, List<string> senders)
        {
            if (senders == null)
                throw new Exception();
            return true;
        }

        /// <summary>
        /// delete all of user with token, friend that in friends
        /// </summary>
        /// <param name="token"></param>
        /// <param name="friends">list of friend that we want to delete</param>
        /// <returns></returns>
        public bool DeleteFriends(string token, List<string> friends)
        {
            if (friends == null)
                throw new Exception();
            return true;
        }

        /// <summary>
        /// deny all friends requests of user with token, that their sender in senders
        /// </summary>
        /// <param name="token"></param>
        /// <param name="senders">list of all the sender we want to deny</param>
        public bool DenyFriendRequests(string token, List<string> senders)
        {
            if (senders == null)
                throw new Exception();
            return true;
        }

        /// <summary>
        /// user with token buy coupon
        /// </summary>
        /// <param name="token">the token of the buying user</param>
        /// <param name="coupon">the coupon he bought</param>
        /// <returns>if sucess return true else false</returns>
        public string BuyCoupon(string token, Coupon coupon)
        {
            if (coupon == null)
                throw new Exception();
            return "";
        }

        /// <summary>
        /// Get all coupon that was bougth by user with token and have one week to be expires
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public List<PurchasedCoupon> GetPressingCoupons(string token)
        {
            return GetPruchasedCoupon(token);
        }

        /// <summary>
        /// get all the Coupon that user with token has bougth
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public List<PurchasedCoupon> GetPruchasedHistory(string token)
        {
            return GetPruchasedCoupon(token);
        }

        /// <summary>
        /// Edit buyer data
        /// </summary>
        /// <param name="token">his token</param>
        /// <param name="newData">the new data that will override the old one</param>
        public bool EditBuyerData(string token, Buyer newData)
        {
            if (newData == null)
                throw new Exception();
            return true;

        }

        /// <summary>
        /// Get list of couon that friend has bougth
        /// </summary>
        /// <param name="token"></param>
        /// <param name="friend">the friend</param>
        /// <returns></returns>
        public List<PurchasedCoupon> GetFriendPruchasedCoupon(string token, string friend)
        {
            return GetPruchasedCoupon(token);
        }

        /// <summary>
        /// send friend request from user with token to toUser
        /// </summary>
        /// <param name="token">token of sender user</param>
        /// <param name="toUsers">the reciver of the request</param>
        /// <returns></returns>
        public bool SendFreindsRequests(string token, List<string> toUsers)
        {
            if (toUsers == null)
                throw new Exception();
            return true;
        }

        /// <summary>
        /// Add coupon
        /// </summary>
        /// <param name="token">token of user that added the coupon</param>
        /// <param name="coupon">the coupon to be added</param>
        public bool AddCoupon(string token, Coupon coupon)
        {
            if (coupon == null)
                throw new Exception();
            return true;

        }

        /// <summary>
        /// set coupon as used
        /// </summary>
        /// <param name="token">need to be the business Manager whitch his business accept the coupon</param>
        /// <param name="serial">the serial of the deal</param>
        public bool SetCouponAsUsed(string token, string serial)
        {
            return true;
        }

        /// <summary>
        /// Edit business
        /// </summary>
        /// <param name="token">token of the business manager</param>
        /// <param name="business">the new data that will override the old</param>
        public bool EditBusinessData(string token,string a, Business business)
        {
            if (business == null)
                throw new Exception();
            Console.WriteLine(business);
            return true;

        }

        /// <summary>
        /// Edit business Manager
        /// </summary>
        /// <param name="token">of the business manager</param>
        /// <param name="manager">the new data that will override the old one</param>
        public bool EditBusinessManagerData(string token, string a,BusinessManager manager)
        {
            if (manager == null)
                throw new Exception();
            Console.WriteLine(manager);
            return true;

        }

        /// <summary>
        /// Edit coupon, WARNING token must belong to system manager
        /// </summary>
        /// <param name="token">of system manager</param>
        /// <param name="couponName">whitch coupon</param>
        /// <param name="businessName">The business of this coupon</param>
        /// <param name="newData">the new data that will override the old one</param>
        public bool EditCoupon(string token, string couponName, string businessName, Coupon newData)
        {
            if (newData == null)
                throw new Exception();
            Console.WriteLine(newData);
            return true;

        }

        /// <summary>
        /// Add coupon o business businessName.
        /// </summary>
        /// <param name="token">the businessmanager of this business</param>
        /// <param name="businessName">the business</param>
        /// <param name="coupon">the coupon to be added</param>
        public bool AddCoupon(string token, string businessName, Coupon coupon)
        {
            if (coupon == null)
                throw new Exception();
            Console.WriteLine(coupon);
            return true;
        }

        /// <summary>
        /// set coupon as Approved, WARNING token must be belong to system manger
        /// </summary>
        /// <param name="token">of system manager that approves it</param>
        /// <param name="businessName">the business of the coupon</param>
        /// <param name="couponName">  the name of the coupon</param>
        public bool SetCouponAsApproved(string token, string businessName, string couponName)
        {
            Console.WriteLine(businessName + "," + couponName);
            return true;

        }

        /// <summary>
        /// Get list of all the unapproved coupon, Token must belong to system manager
        /// </summary>
        /// <param name="token">of system manager</param>
        /// <returns>list of all the unapproved Coupon</returns>
        public List<Coupon> GetUnapprovedCoupons(string token)
        {
            return getCouponList();
        }

        /// <summary>
        /// Add category to the system, must be done by system manager ==> token belong to system manager
        /// </summary>
        /// <param name="token">of system manger</param>
        /// <param name="category">the category to be added</param>
        public bool AddCategory(string token, Category category)
        {
            if (token != "TOKEN" || category == null)
                throw new Exception();
            return true;

        }

        /// <summary>
        /// edit data of system manger
        /// </summary>
        /// <param name="token">the token of the system manager that we will edit</param>
        /// <param name="manager">the new data that will overrdie the old one</param>
        public bool EditSystemManagerData(string token, SystemManager manager)
        {
            if (token != "TOKEN" || manager == null)
                throw new Exception();
            return true;

        }

        /// <summary>
        /// Get all the pruchased coupon that was bougth in bussiness belong to user businessmanager with token.
        /// MUST NOT return the serial of those deals!!!
        /// </summary>
        /// <param name="token">of businessmanager that manage the business</param>
        /// <returns></returns>
        public List<PurchasedCoupon> GetPurchasedCoupon(string token)
        {
            return getDealList(token);
        }
        public List<Category> getCategories()
        {
            return new List<Category>();
        }

        public List<Business> GetBussiness(string a,string v){
            return null;
        }
        public List<Coupon> GetCoupons(string a, string v)
        {
            return null;
        }

        public Buyer GetBuyer(string name)
        {
            return null;
        }

        public BusinessManager GetBusinessManager(string businessName)
        {
            return null;
        }
    }
}