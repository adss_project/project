﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Coupon
    {
        private string _name;
        private string _businessName;
        private string _serial;
        private string _description;
        private double _originalPrice;
        private double _priceAfterDiscount;
        private DateTime _expirationDate;
        private double _rating;
        private string _itemBrand;
        private bool _isApproved;
        private string _adderUsername;
        private string _approverUsername;
        private List<Category> _categories;
        private DateTime _addedDate;
        public Coupon()
        {

        }
        public Coupon(string name, string businessName, string serial, string description, float originalPrice, float priceAfterDiscount, DateTime expirationDate, float rating, string itemBrand, bool isApproved, string adderUsername, string approverUsername, List<Category> categories, DateTime addedDate)
        {
            this._name = name;
            this._businessName = businessName;
            this._serial = serial;
            this._description = description;
            this._originalPrice = originalPrice;
            this._priceAfterDiscount = priceAfterDiscount;
            this._expirationDate = expirationDate;
            this._rating = rating;
            this._itemBrand = itemBrand;
            this._isApproved = isApproved;
            this._adderUsername = adderUsername;
            this._approverUsername = approverUsername;
            this._categories = categories;
            this._addedDate = addedDate;
        }
        public Coupon(Coupon coupon)
        {
            this._name = coupon.name;
            this._businessName = coupon.businessName;
            this._serial = coupon.serial;
            this._description = coupon.description;
            this._originalPrice = coupon.originalPrice;
            this._priceAfterDiscount = coupon.priceAfterDiscount;
            this._expirationDate = coupon.expirationDate;
            this._rating = coupon.rating;
            this._itemBrand = coupon.itemBrand;
            this._isApproved = coupon.isApproved;
            this.adderUsername = coupon.adderUsername;
            this._approverUsername = coupon.approverUsername;
            this._categories = coupon.categories;
            this._addedDate = coupon.addedDate;
        }
        public Coupon(string name, string businessName, string serial, string description, float originalPrice, float priceAfterDiscount, DateTime expirationDate, float rating, string itemBrand, string adderUsername, string approverUsername, List<Category> categories)
        {
            this._name = name;
            this._businessName = businessName;
            this._serial = serial;
            this._description = description;
            this._originalPrice = originalPrice;
            this._priceAfterDiscount = priceAfterDiscount;
            this._expirationDate = expirationDate;
            this._rating = rating;
            this._itemBrand = itemBrand;
            this._adderUsername = adderUsername;
            this._approverUsername = approverUsername;
            this._categories = categories;
        }
        public Coupon(string name, string businessName, string serial, float originalPrice, float priceAfterDiscount, string adderUsername)
        {
            this._name = name;
            this._businessName = businessName;
            this._serial = serial;
            this._originalPrice = originalPrice;
            this._priceAfterDiscount = priceAfterDiscount;
            this._adderUsername = adderUsername;
        }
        public Coupon(string name, string businessName, string serial)
        {
            this._name = name;
            this._businessName = businessName;
            this._serial = serial;
        }
        
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string businessName
        {
            get { return _businessName; }
            set { _businessName = value; }
        }
        public string serial
        {
            get { return _serial; }
            set { _serial = value; }
        }
        public string description
        {
            get { return _description; }
            set { _description = value; }
        }
        public double originalPrice
        {
            get { return _originalPrice; }
            set { _originalPrice = value; }
        }
        public double priceAfterDiscount
        {
            get { return _priceAfterDiscount; }
            set { _priceAfterDiscount = value; }
        }
        public DateTime expirationDate
        {
            get { return _expirationDate; }
            set { _expirationDate = value; }
        }
        public double rating
        {
            get { return _rating; }
            set { _rating = value; }
        }
        public string itemBrand
        {
            get { return _itemBrand; }
            set { _itemBrand = value; }
        }
        public bool isApproved
        {
            get { return _isApproved; }
            set { _isApproved = value; }
        }
        public string adderUsername
        {
            get { return _adderUsername; }
            set { _adderUsername = value; }
        }
        public string approverUsername
        {
            get { return _approverUsername; }
            set { _approverUsername = value; }
        }
        public List<Category> categories
        {
            get { return _categories; }
            set { _categories = value; }
        }
        public DateTime addedDate
        {
            get { return _addedDate; }
            set { _addedDate = value; }
        }
    }
}
