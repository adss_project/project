﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServiceLayer
{
    public class LoginController : ServiceBaseController
    {
        public List<string> GetLogin(string buyerUsername, string buyerPassword)
        {
            try
            {
                List<string> token = businessLayer.GetLogin(buyerUsername, buyerPassword);
                return token;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public HttpResponseMessage PutLogout([FromBody] string token)
        {
            try
            {
                businessLayer.Logout(token);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutBuyerRegistretion([FromBody] Buyer user)
        {
            try
            {
                businessLayer.RegisterBuyer(user);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public List<Category> GetCategories()
        {
            return businessLayer.getCategories();
        }

        public List<Business> GetBussiness(string bussinesName, string category)
        {
            return businessLayer.GetBussiness(bussinesName, category);
        }
        public List<Coupon> GetCoupon(string bussinesName, string category) //need to add reference to spacial but we dont know from where !!!
        {
            return businessLayer.GetCoupons(bussinesName, category);
        }

        public Buyer GetBuyer(string buyerName)
        {
            return businessLayer.GetBuyer(buyerName);
        }

        public BusinessManager GetBusinessManager(string businessName)
        {
            return businessLayer.GetBusinessManager(businessName);
        }
    }
}
