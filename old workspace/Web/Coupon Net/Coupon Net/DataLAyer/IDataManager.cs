﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Entities;
using System.Data.Spatial;
namespace DataLAyer
{
    public interface IDataManager
    {
        DataTable GetLogin(string username, string password);

        bool RegisterBuyer(Buyer user);

        bool RegisterBusinessManager(string token, BusinessManager user);

        bool RegisterSystemManager(string token, SystemManager user);

        bool Logout(string token);

        DataTable GetCouponWithinRange(float range, DbGeography location);

        DataTable GetCouponInCategory(List<string> categories);

        DataTable GetFriendList(string token);

        DataTable GetFriendRequests(string token);

        bool AcceptFreindsRequest(string token, List<string> senders);

        bool DeleteFriends(string token, List<string> friends);

        bool DenyFriendRequests(string token, List<string> senders);

        bool BuyCoupon(string token, Coupon coupon);

        DataTable GetPressingCoupons(string token);

        DataTable GetPruchasedHistory(string token);

        bool EditBuyerData(string token, Buyer newData);

        DataTable GetFriendPruchasedCoupon(string token, string friend);

        bool SendFreindsRequests(string token, List<string> toUsers);

        bool AddCoupon(string token, Coupon coupon);

        bool SetCouponAsUsed(string token, string serial);

        bool EditBusinessData(string token,string oldBusinessName, Business business);

        bool EditBusinessManagerData(string token, string oldBusinessManagerUserName, BusinessManager manager);

        bool EditCoupon(string token, string couponName, string businessName, Coupon newData);

        bool SetCouponAsApproved(string token, string businessName, string couponName);

        DataTable GetUnapprovedCoupons(string token);

        bool AddCategory(string token, Category category);

        bool EditSystemManagerData(string token, SystemManager manager);

        DataTable GetPruchasedCoupon(string token);
    }
}
