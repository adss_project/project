﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
namespace BusinessLayer
{
    public interface IBusinessLayer
    {
        // Return login token, the string need to contain info about the type
        string GetLogin(string buyerUsername, string buyerPassword);

        bool RegisterBuyer(Buyer user);

        bool RegisterBusinessManager(string token, BusinessManager user);

        bool RegisterSystemManager(string token, SystemManager user);

        bool Logout(string token);

        List<Coupon> GetCouponWithinRange(float range, DbGeography location);

        List<Coupon> GetCouponInCategory(List<string> category);

        List<Buyer> GetFriendList(string token);

        List<Buyer> GetFriendRequests(string token);

        bool AcceptFreindsRequest(string token, List<string> senders);

        bool DeleteFriends(string token, List<string> friends);

        bool DenyFriendRequests(string token, List<string> senders);

        bool BuyCoupon(string token, Coupon coupon);

        List<PurchasedCoupon> GetPressingCoupons(string token);

        List<PurchasedCoupon> GetPruchasedHistory(string token);

        bool EditBuyerData(string token, Buyer newData);

        List<PurchasedCoupon> GetFriendPruchasedCoupon(string token, string friend);

        bool SendFreindsRequests(string token, List<string> toUsers);

        bool AddCoupon(string token, Coupon coupon);

        bool SetCouponAsUsed(string token, string serial);

        bool EditBusinessData(string token, string oldBusinessName, Business business);

        bool EditBusinessManagerData(string token, string oldBusinessManagerUserName, BusinessManager manager);

        bool EditCoupon(string token, string couponName, string businessName, Coupon newData);

        /*bool AddCoupon(string token, string businessName, Coupon coupon);*/

        bool SetCouponAsApproved(string token, string businessName, string couponName);

        List<Coupon> GetUnapprovedCoupons(string token);

        bool AddCategory(string token, Category category);

        bool EditSystemManagerData(string token, SystemManager manager);

        List<PurchasedCoupon> GetPruchasedCoupon(string token);
    }
}
