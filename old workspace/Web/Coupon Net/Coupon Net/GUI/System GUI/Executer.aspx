﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Executer.aspx.cs" Inherits="Coupon_Net.Executer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Coupon Net Sql Executer</title>
    <script type = "text/javascript">
        function sqlError(message) {
            window.returnValue = message;
            window.close();
        }
        function scrollMap(position) {
            // Scrolls the map so that it is centered at (position.coords.latitude, position.coords.longitude).
            var coordinates = position.coords;
            document.getElementById("location").value = coordinates.latitude;
            document.getElementById("location").value += (";" + coordinates.longitude);
            
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            document.getElementById('latitude').value = latitude;
            document.getElementById('longitude').value = longitude;
        }

        // Request repeated updates.
        var watchId = navigator.geolocation.watchPosition(scrollMap);

        function buttonClickHandler() {
            // Cancel the updates when the user clicks a button.
            navigator.geolocation.clearWatch(watchId);
        }
    </script> 
</head>
<body bgcolor="#ccffff">
    <form id="form1" runat="server">
    <asp:HiddenField ID="latitude" runat="server" />
    <asp:HiddenField ID="longitude" runat="server" />
    <div>
        <h1>Coupon Net Sql Executer</h1>
        <asp:TextBox id="query" TextMode="multiline" Font-Size="Large" style="resize:none" Text="Query" runat="server" Height="108px" Width="1032px" ></asp:TextBox>
     </div>
     <div>

    </div>
        <p>
            <asp:TextBox ID="location" Font-Size="Large" style="resize:none" Text="lOCATION" runat="server"  ></asp:TextBox>
        <asp:Button id="execute" Text="Execute" runat="server" OnClick="execute_Click" style="direction: ltr" />

        </p>
            <asp:DataGrid ID="dataTable" HeaderStyle-BackColor="SkyBlue" BackColor="WhiteSmoke" runat="server" PageSize="5" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None" Width="1031px"/>
    </form>
</body>
</html>
