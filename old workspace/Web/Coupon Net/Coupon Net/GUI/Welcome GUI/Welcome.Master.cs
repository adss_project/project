﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net
{
    public partial class Welcome : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["first"] == null) {
                Session["first"] = new object();
                List<Category> lca = new List<Category>();
                lca.Add(new Category("Sport", "Blabla"));
                lca.Add(new Category("Gaming", "ASSASSIN'S CREED!!"));
                lca.Add(new Category("Tv", "KAKAROTO!!!"));
                lca.Add(new Category("Amusement", "Manga!"));
                lca.Add(new Category("Health Care", "You know what i mean"));
                Session["Category"] = lca;



                List<Coupon> lco = new List<Coupon>();
                lco.Add(new Coupon("SuperCup", "Nike", "E45c2r32fF"));
                lco.Add(new Coupon("SongAboutJane", "TowerRecord", "5Gd4fGff4"));
                lco.Add(new Coupon("PayOffManga", "MangaShop", "gf5gdDG774"));
                lco.Add(new Coupon("fiftyPXFreeBall", "Nike", "Ertb4532gfF"));
                lco.Add(new Coupon("SuperTampon", "Always", "Lol69Trolololo"));
                Session["Coupon"] = lco;



                Address[] aad = {   new Address("AirSportCity", "StinkyShoes Blv.", 007), 
                                    new Address("MusicWave", "deaf st.", 7007), 
                                    new Address("MangaLand", "ShippuGaiden Av.", 2034), 
                                    new Address("Sometime", "BloodyWay", 6969) };
                Session["Adress"] = aad;


                List<Business> lbu = new List<Business>();
                lbu.Add(new Business("Nike","Desc",aad[0],lca));
                lbu.Add(new Business("TowerRecord", "Desc", aad[1], lca));
                lbu.Add(new Business("MangaShop", "Desc", aad[2], lca));
                lbu.Add(new Business("Always", "Desc", aad[3], lca));
                Session["Business"] = lbu;
            }
        }
    }
}