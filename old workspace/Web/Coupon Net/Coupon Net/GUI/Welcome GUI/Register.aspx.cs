﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net.GUI.Welcome_GUI
{
    public partial class Register : System.Web.UI.Page
    {
        ServiceLayerClient.ServiceLayerClient serviceLayer;
        protected void Page_Load(object sender, EventArgs e)
        {
            serviceLayer = new ServiceLayerClient.ServiceLayerClient();
        }

        protected void SubmitRegistration(object sender, EventArgs e)
        {
            try
            {
                Buyer buyer = new Buyer(TextBox_UN.Text, TextBox_Password.Text, null, TextBox_Email.Text, TextBox_phone.Text);
                serviceLayer.BuyerRegistretion(buyer);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void TextBox_Email_TextChanged(object sender, EventArgs e)
        {

        }

    }
}