﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GUI/Welcome GUI/Welcome.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Coupon_Net.GUI.Welcome_GUI.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style3 {
            width: 714px;
            height: 375px;
        }
        .auto-style4 {
            height: 26px;
        }
        .auto-style5 {
            width: 199px;
        }
        .auto-style7 {
            height: 26px;
            width: 199px;
            text-align: right;
        }
        .auto-style8 {
            width: 199px;
            text-align: right;
        }
        .auto-style9 {
            height: 17px;
            width: 199px;
            text-align: right;
        }
        .auto-style10 {
            height: 17px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHolder" runat="server">
  
    <table class="auto-style3">
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label8" runat="server" Font-Size="Large" ForeColor="White" Text="Username:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_UN" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox_UN" ErrorMessage="* Username is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label7" runat="server" Font-Size="Large" ForeColor="White" Text="E-mail:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_Email" runat="server" TextMode="Email" Width="200px" OnTextChanged="TextBox_Email_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox_Email" ErrorMessage="* Email is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox_Email" ErrorMessage="* You Must Enter  a Vaild Email address." ForeColor="Red"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label6" runat="server" Font-Size="Large" ForeColor="White" Text="First Name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_FN" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox_FN" ErrorMessage="* First name is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label5" runat="server" Font-Size="Large" ForeColor="White" Text="Last Name:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_LN" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox_LN" ErrorMessage="* Last name is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style8">
                <asp:Label ID="Label4" runat="server" Font-Size="Large" ForeColor="White" Text="Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBox_Password" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox_Password" ErrorMessage="* Password is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style9">
                <asp:Label ID="Label3" runat="server" Font-Size="Large" ForeColor="White" Text="Confirm Password:"></asp:Label>
            </td>
            <td class="auto-style10">
                <asp:TextBox ID="TextBox_CP" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style10">
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBox_Password" ControlToValidate="TextBox_CP" ErrorMessage="* Password doesn&amp;#39;t match." ForeColor="Red"></asp:CompareValidator>
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBox_CP" ErrorMessage="* Confirm password is requeired." ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                <asp:Label ID="Label2" runat="server" Font-Size="Large" ForeColor="White" Text="Phone Number:"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="TextBox_phone" runat="server" TextMode="Phone" Width="200px"></asp:TextBox>
            </td>
            <td class="auto-style4">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBox_phone" ErrorMessage="* Phone number is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Size="Large" ForeColor="White" Text="Preferance:"></asp:Label>
            </td>
            <td class="auto-style4">
                <asp:CheckBoxList ID="Preferance" runat="server" DataSourceID="SqlDataSource1" DataTextField="category" DataValueField="category" Font-Size="Large" ForeColor="White" >
                </asp:CheckBoxList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [category] FROM [Categories]"></asp:SqlDataSource>
            </td>
            <td class="auto-style4">
                </td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td>
                <br />
                &nbsp;
                <input id="Reset1" type="reset" value="Reset"
                     style="border-bottom: 1px solid #CCCCCC; font-family: Verdana; background-color: #FFFBFF; background-color: #FFFBFF; color: #284775; height: 29px; width: 69px; border-left-color: #CCCCCC; border-left-width: 1px; border-right-color: #CCCCCC; font-weight: bold; border-right-width: 1px; border-top-color: #CCCCCC; border-top-width: 1px;"/></td>
            <td>
                <br />
                <asp:Button ID="Button1" runat="server" OnClick="SubmitRegistration" Text="Submit"  BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" ForeColor="#284775" Height="29px" Width="81px" />
            </td>
        </tr>
    </table>
  
</asp:Content>
