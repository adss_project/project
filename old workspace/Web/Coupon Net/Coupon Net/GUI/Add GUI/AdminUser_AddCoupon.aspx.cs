﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;

namespace Coupon_Net.GUI.Add_GUI
{
    public partial class AdminUser_AddCoupon : System.Web.UI.Page
    {
        List<Category> categoriesList;
        List<Business> busiList;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();

                categoriesList = s.GetCategories();
                busiList = s.GetBusiness(null, null, null);

                if (!Page.IsPostBack)
            {
                CategoryList.Items.Add("Select Category");
                foreach (Category category in categoriesList)
                {
                    CategoryList.Items.Add(category.name);
                }
                CategoryList0.Items.Add("Select Business");
                foreach (Business business in busiList)
                {
                    CategoryList0.Items.Add(business.businessName);
                }
            }
            }
            catch (Exception ex) {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            string name = CouponName.Text,
                   desc = CouponDescriptionTextBox.Text,
                   ini_price = InitPrizeTextBox.Text,
                   new_price = NewPriceTextBox.Text,
                   category = CategoryList.SelectedValue,
                   date = ExpirationTextBox.Text,
                   business = CategoryList0.SelectedValue;
            Coupon couponToAdd = new Coupon(name, business, "NONE", float.Parse(ini_price), float.Parse(new_price), (string)Session["Admin"]);
            ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();
            try
            {
                s.PutAddCoupon((string)Session["Admin"], couponToAdd);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

    }
}