﻿46<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewSingleBuisness.aspx.cs" Inherits="Coupon_Net.GUI.View_GUI.ViewSingleBuisness" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="stylesheet" type="text/css" href="../style/toolbar_style.css" />
    
    <title></title>
     <style type="text/css">

        .auto-style3 {
            width: 714px;
            height: 375px;
        }
        .auto-style7 {
            height: 26px;
            width: 199px;
            text-align: right;
        }
        .auto-style4 {
            height: 26px;
        }
         .auto-style8 {
             height: 26px;
             width: 277px;
         }
         .auto-style9 {
             width: 277px;
         }
        </style>
</head>
<body>
     <form id="form1" runat="server">
   






        <div style="padding: 50px; float:right; margin-top: 42px;">
             <asp:Image ID="Image1" runat="server" ImageUrl="~/GUI/View/title.png" style=" margin-top: 20px; margin-bottom: 70px"  />

    <p style="font:italic; color:#FFFBFF; text-align:left; margin-left:100px; font-size:30px; text-decoration:underline;" > Buisness view</p>
    <table class="auto-style3">
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label2" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness Id:"></asp:Label>

            </td>
            <td class="auto-style8">
                <asp:TextBox ID="BuisnessIdText" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label3" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness Name:"></asp:Label>

            </td>
            <td class="auto-style8">
                <asp:TextBox ID="BuisnessNameText" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label10" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness information:"></asp:Label>

            </td>
            <td class="auto-style8">
                <asp:TextBox ID="BuisnessInformationTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                                <asp:Label ID="Label9" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness Location:"></asp:Label>

            </td>
            <td class="auto-style9">

                   <asp:TextBox ID="BuisnessLocationTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label7" runat="server" Font-Size="Large" ForeColor="White" Text="Business Category:"></asp:Label>


            </td>
            <td class="auto-style8">

                <asp:DropDownList ID="CategoryList" runat="server" Width="205px">
                </asp:DropDownList>

            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>

       
        
        <tr>
            <td class="auto-style7">&nbsp;</td>
            <td class="auto-style9">
                <asp:Button ID="EditButton" runat="server"  Text="Edit"  BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                            BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" ForeColor="#284775" Height="29px" Width="69px" OnClick="Edit_Click" style="margin-left: 9px" />
                <asp:Button ID="DeleteButton" runat="server"  Text="Delete"  BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                            BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" ForeColor="#284775" Height="29px" Width="65px" OnClick="Delete_Click" style="margin-left: 9px" />
                <br />
                
                &nbsp;&nbsp;
            </td>
            <td>
                <br />
            </td>
        </tr>
    </table>
  </div>





    </form>
</body>
</html>
