﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Entities;
using ServiceLayerClient;

namespace Coupon_Net.GUI.View_GUI
{
    public partial class NormalUserCouponView : System.Web.UI.Page
    {
        private ServiceLayerClient.ServiceLayerClient serviceLayer;
        List<PurchasedCoupon> couponList;
        User normalUser;
        string normalUserName;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { 
                normalUserName = (string)Session["Normal"];
                serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                couponList = serviceLayer.GetPruchasedHistory(normalUserName);
                if (!Page.IsPostBack)
                {
                   
                    //business = serviceLayer.getBusiness(businessName);
                    //normalUser = new User("goku", "hello", "1700707070", "gargamel@iHateDardaSaba.com");

                    foreach (Coupon coupon in couponList)
                    {
                        ListItem newLine = new ListItem();
                        newLine.Text = coupon.name;  //text name goes i.e. here tab1
                        RadioButtonList1.Items.Add(newLine);
                    }
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }


        protected void selectCoupon_Click(object sender, EventArgs e)
        {
            Coupon wantedCoupon = null;
            foreach (Coupon coupon in couponList)
            {
                if (coupon.name.CompareTo(RadioButtonList1.SelectedValue) == 0)
                    wantedCoupon = coupon;
            }
            Session["couponFromSearch"] = wantedCoupon;
            string redirect = "<script>window.open('../View GUI/ViewSingleCoupon.aspx');</script>";
            Response.Write(redirect);
        }
    }
}