﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Entities;
    using ServiceLayerClient;

    namespace Coupon_Net.GUI.View_GUI
    {
        public partial class ViewSingleBuisness : System.Web.UI.Page
        {
            List<Category> categoriesList;
            Business business;
            string businessName;
            ServiceLayerClient.ServiceLayerClient serviceLayer;

            protected void Page_Load(object sender, EventArgs e)
            {
                try
                {
                    serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                    businessName = (string)Session["BusinessName"];
                    business = serviceLayer.GetBusiness(businessName, null, null).ToArray()[0];
                    // business = new Business("Kakaroto and Son", "Kamehameha", null, (List<Category>)Session["Category"] );

                    if (Session["Admin"] != null)
                    {
                        EditButton.Visible = true;
                        DeleteButton.Visible = true;
                    }
                    else
                    {
                        EditButton.Visible = false;
                        DeleteButton.Visible = false;

                    }

                    BuisnessNameText.Text = business.businessName;
                    BuisnessInformationTextBox.Text = business.description;
                    //should be somthing about the location

                    categoriesList = serviceLayer.GetCategories();


                    if (!Page.IsPostBack)
                    {
                        CategoryList.Items.Add("Select Category");
                        foreach (Category category in (categoriesList))
                        {
                            CategoryList.Items.Add(category.name);
                        }
                    }


                    CategoryList.EnableViewState = false;
                    BuisnessIdText.Enabled = false;
                    BuisnessNameText.Enabled = false;
                    BuisnessInformationTextBox.Enabled = false;
                    BuisnessLocationTextBox.Enabled = false;
                    CategoryList.EnableViewState = false;

                }
                catch (Exception ex)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
                }
            }

            protected void Delete_Click(object sender, EventArgs e)
            {
                try
                {
                    serviceLayer.deleteBusiness(businessName, (string)Session["userName"]);
                }
                catch (Exception ex)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
                }
                //should close window
            }

            protected void Edit_Click(object sender, EventArgs e)
            {
                try
                {
                    if (EditButton.Text.CompareTo("Edit") == 0)
                    {
                        BuisnessNameText.Enabled = true;
                        BuisnessInformationTextBox.Enabled = true;
                        BuisnessLocationTextBox.Enabled = true;
                        EditButton.Text = "Confirm";

                    }
                    else
                    {
                        BuisnessNameText.Enabled = false;
                        BuisnessInformationTextBox.Enabled = false;
                        BuisnessLocationTextBox.Enabled = false;
                        EditButton.Text = "Edit";

                

                        Business newBusiness = new Business(BuisnessNameText.Text, BuisnessInformationTextBox.Text, null, null);//need to change the nulls
                        serviceLayer.PutEditBusinessData((string)Session["userName"], newBusiness);
                    }
            

                }
                catch (Exception ex)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
                }
            }


        }
    }