﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NormalUser_BuisnessSearch.aspx.cs" Inherits="Coupon_Net.GUI.Search_GUI.NormalBuisnessSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="stylesheet" type="text/css" href="../style/toolbar_style.css" />
    
    <title></title>
     <style type="text/css">

        .auto-style3 {
            width: 714px;
            height: 375px;
        }
        .auto-style7 {
            height: 26px;
            width: 199px;
            text-align: right;
        }
        .auto-style4 {
            height: 26px;
        }
        </style>
</head>
<body>
     <form id="form1" runat="server">
    <div id="nav">
        <ul>
            <li style="float:left;"><a href="../Welcome GUI/Logout.aspx">Log Out</a></li>
            <li><a href="#"> Order</a></li>
            <li><a href="#"> Search </a>
            <ul>
                <li><a href="#" style="background:#ff6a00"> Business </a></li>
                <li><a href="../Search GUI/NormalUser_CouponSearch.aspx"> Coupon </a></li>
            </ul></li>
            <li><a href="#"> View  </a>
                 <ul>
                <li><a href="../View GUI/NormalUser_ProfileView.aspx"> Profile </a></li>
                <li><a href="../View GUI/NormalUser_CouponView.aspx"> My Coupon </a></li>
            </ul></li>

        </ul>
    
    </div>






        <div style="padding: 50px; float:right; margin-top: 42px;">
             <asp:Image ID="Image1" runat="server" ImageUrl="~/GUI/View/title.png" style=" margin-top: 20px; margin-bottom: 70px"  />

    <p style="font:italic; color:#FFFBFF; text-align:left; margin-left:100px; font-size:30px; text-decoration:underline;" > Search Buisness</p>
    <table class="auto-style3">

        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label10" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness Name:"></asp:Label>

            </td>
            <td class="auto-style4">
                <asp:TextBox ID="TextBox_Buisness" runat="server" TextMode="Phone" Width="200px" style="height: 25px"></asp:TextBox>
            </td>
            <td class="auto-style4">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBox_Buisness" ErrorMessage="* buisness name id required." ForeColor="Red"></asp:RequiredFieldValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                                <asp:Label ID="Label9" runat="server" Font-Size="Large" ForeColor="White" Text="Buisness Category:"></asp:Label>

            </td>
            <td>

                <asp:DropDownList ID="CategoryList" runat="server" Width="205px">
                </asp:DropDownList>

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                  <asp:Label ID="Label3" runat="server" Font-Size="Large" ForeColor="White" Text="By Location:"></asp:Label>

            </td>
            <td>

                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>

            </td>
            <td>

            </td>

        </tr>

        <tr>
            <td class="auto-style7">&nbsp;</td>
            <td>
                <input id="Reset1" type="reset" value="Reset"
                     style="border-bottom: 1px solid #CCCCCC; font-family: Verdana; 
                     background-color: #FFFBFF; background-color: #FFFBFF; color: #284775; 
                     height: 29px; width: 74px; border-left-color: #CCCCCC; border-left-width: 1px; 
                     border-right-color: #CCCCCC; font-weight: bold; border-right-width: 1px; border-top-color: #CCCCCC; border-top-width: 1px;"/>
                <asp:Button ID="Search" runat="server"  Text="Search"  BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                            BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" ForeColor="#284775" Height="29px" Width="84px" style="margin-left: 61px" OnClick="Search_Click" />
                <br />
                &nbsp;&nbsp;
            </td>
            <td>
                <br />
            </td>
        </tr>
    </table>
  </div>





    </form>
</body>
</html>