﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayerClient
{
    public class ServiceLayerClient
    {
        private static readonly string baseAddress = "http://localhost:9020/";
       
        private string _token = null;

        // Create HttpCient and make a request to api/values 
        private HttpClient client = new HttpClient();


        public async void GetLogin(string buyerUsername, string buyerPassword)
        {
            var response = client.GetAsync(baseAddress + "api/login/GetLogin/To/155").Result;
            _token = await response.Content.ReadAsStringAsync();
        }

        public void Logout(string token)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/login/PutLogout/",_token).Result;
            if (response.IsSuccessStatusCode)
            {
                _token = null;
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void BuyerRegistretion(Buyer user)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/login/PutBuyerRegistretion/", user).Result;
            if (response.IsSuccessStatusCode)
            {
                return;
            }
            else
            {
                throw new OperationFailedException("Error has occurd during registration.");
            }
        }
   

        public List<Coupon> GetCouponWithinRange(float range, DbGeography location)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetCouponWithinRange?range=" + range + "&lat=" + location.Latitude + "&longtidue=" + location.Longitude).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
          
        }

        public List<Coupon> GetCouponWithinRange(float range, float latitude, float longitude)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetCouponWithinRange?range=" + range + "&lat=" + latitude + "&longtidue=" + longitude).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        /*****************************************************************************************************************/
        public List<Coupon> GetCouponInCategory(List<Category> Category)
        {
            string list = "?";
            foreach(Category c in Category){
                list += ("Category=" + c.name +"&");
            }
            list.Remove(list.Length - 1);
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetCouponInCategory" + list).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<Buyer> GetFriendList(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetFriendList?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Buyer> returnList = JsonConvert.DeserializeObject<List<Buyer>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<Buyer> GetFriendRequests(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetFriendRequests?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Buyer> returnList = JsonConvert.DeserializeObject<List<Buyer>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutSendFreindsRequests(string token, List<string> toUsers)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutSendFreindsRequests?token=" + token, toUsers).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutAcceptFreindsRequest(string token, List<string> senders)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutAcceptFreindsRequest?token=" + token, senders).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutDeleteFriends(string token, List<string> friends)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutDeleteFriends?token=" + token, friends).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutDenyFriendRequests(string token, List<string> senders)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutDenyFriendRequests?token=" + token, senders).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutBuyCoupon(string token, Coupon coupon)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutBuyCoupon?token=" + token, coupon).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<PurchasedCoupon> GetPressingCoupon(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetPressingCoupons?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<PurchasedCoupon> returnList = JsonConvert.DeserializeObject<List<PurchasedCoupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<PurchasedCoupon> GetPruchasedHistory(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetPruchasedHistory?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<PurchasedCoupon> returnList = JsonConvert.DeserializeObject<List<PurchasedCoupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEdit(string token, Buyer newData)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbuyer/PutEdit?token=" + token, newData).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<PurchasedCoupon> GetFriendPurchasedCoupon(string token, string friend)
        {
            var response = client.GetAsync(baseAddress + "api/userbuyer/GetFriendPruchasedCoupon?token=" + token + "&friend=" + friend).Result;
            if (response.IsSuccessStatusCode)
            {
                List<PurchasedCoupon> returnList = JsonConvert.DeserializeObject<List<PurchasedCoupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutAddCoupon(string token, Coupon coupon)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbusiness/PutAddCoupon?token=" + token, coupon).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutSetCouponAsUsed(string token, string serial)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/userbusiness/PutSetCouponAsUsed?token=" + token + "&serial="+ serial,0).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEditBusinessData(string token, Business business){
            var response = client.PutAsJsonAsync(baseAddress + "api/userbusiness/PutEditBusinessData?token=" + token ,business).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEditBusinessManagerData(string token, BusinessManager manager){
            var response = client.PutAsJsonAsync(baseAddress + "api/userbusiness/PutEditBusinessManagerData?token=" + token ,manager).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<PurchasedCoupon> GetPurchasedCoupon(string token)
        {
            var response = client.GetAsync(baseAddress + "api/userbusiness/GetPruchasedCoupon?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<PurchasedCoupon> returnList = JsonConvert.DeserializeObject<List<PurchasedCoupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }
        public void PutAddCoupon(string token, string businessName, Coupon coupon)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutAddCoupon?token=" + token, coupon).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutSetCouponAsApproved(string token, string businessName, string couponName)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutSetCouponAsApproved?token=" + token +
                                                                "&businessName=" + businessName + "&couponName=" + couponName, 0).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<Coupon> GetUnapprovedCoupon(string token)
        {
            var response = client.GetAsync(baseAddress + "api/usersystemmanager/GetUnapprovedCoupons?token=" + token).Result;
            if (response.IsSuccessStatusCode)
            {
                List<Coupon> returnList = JsonConvert.DeserializeObject<List<Coupon>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutBusinessManagerRegistretion(string token, BusinessManager user)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutBusinessManagerRegistretion?token=" + token, user).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutSystemManagerRegistretion(string token, SystemManager user){
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutSystemManagerRegistretion?token=" + token, user).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }
        public void PutAddCategory(string token, Category category)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutAddCategory?token=" + token, category).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEditData(string token, SystemManager manager)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutEditData?token=" + token, manager).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public void PutEditCoupon(string token, string couponName, string businessName, Coupon newData)
        {
            var response = client.PutAsJsonAsync(baseAddress + "api/usersystemmanager/PutEditCoupon?token=" + token +
                                                                    "&couponName=" + couponName + "&businessName=" + businessName, newData).Result;
            if (response.IsSuccessStatusCode)
            {

            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }

        public List<Category> GetCategories()
        {
            var response = client.GetAsync(baseAddress + "api/login/GetCategories").Result;
            if (response.IsSuccessStatusCode)
            {
                List<Category> returnList = JsonConvert.DeserializeObject<List<Category>>(response.Content.ReadAsStringAsync().Result);
                if (returnList != null)
                {
                    return returnList;
                }
                else
                {
                    throw new OperationFailedException("Coudn't log out.");
                }
            }
            else
            {
                throw new OperationFailedException("Coudn't log out.");
            }
        }
       /// <summary>
       /// ///////////////////////////////////
       /// </summary>
       /// <param name="coupon"></param>
       /// <param name="userName"></param>
        public void orderCoupon(Coupon coupon, string userName)
        {
            PutBuyCoupon(userName, coupon);
        }

        public void deleteCoupon(string couponName, string userName)
        {

        }
        public void AddBusiness(string userName, Business businesstoAdd)
        {
            return;
        }
        public List<Business> GetBusiness(string bussinesName, string category, string t)//DbGeography location) //need to add reference to spacial but we dont know from where !!!
        {
            return new List<Business>();
        }


        public List<Coupon> GetCoupon(string bussinesName, string category, string t)//DbGeography location) //need to add reference to spacial but we dont know from where !!!
        {
            return new List<Coupon>();
        }

        public string Login(string password, string userName) { 
            string typeOfUser ="";
            return userName;// typeOfUser;
        }

        public void deleteBusiness(string businessName, string userName)
        {

        }
        public Buyer getBuyer(string buyerName)
        {
            return null;
        }
        
    }
}

/*
 * צריך לשנות לביזנס במקום מספר קטגוריות לקטכוריה אחת
 * 
*/