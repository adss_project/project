﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataLayer;
using System.Data;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        private DatabaseExecutor executor = new DatabaseExecutor();
        private static bool AreTablesEqual(DataTable t1, DataTable t2)
        {
            if (t1.Rows.Count != t2.Rows.Count)
                return false;

            for (int i = 0; i < t1.Rows.Count; i++)
            {
                foreach (DataColumn col in t1.Columns)
                {
                    if (!Equals(t1.Rows[i][col.ColumnName], t2.Rows[i][col.ColumnName]))
                        return false;
                }
            }
            return true;
        }
        /// <summary>
        /// insert to table by table name and values
        /// </summary>
        /// <param name="table">talbe name</param>
        /// <param name="values">values in this format: (val1,val2,...)</param>
        private DataTable InsertToTableWithValues(string table, string values)
        {
            string quary = string.Format("INSERT INTO {0} values {1}", table, values);
            return executor.ExecuteSqlQuery(quary);
        }
        /// <summary>
        /// delete from table by table name and where condition
        /// </summary>
        /// <param name="table">table name</param>
        /// <param name="whereCondition">condition such as: where username='aaaaa'</param>
        private DataTable DeleteFromTableWhereCondition(string table, string whereCondition)
        {
            string quary = string.Format("DELETE FROM {0} WHERE {1}", table, whereCondition);
            return executor.ExecuteSqlQuery(quary);
        }
        /// <summary>
        /// update table by table name, set commands and a where condition
        /// </summary>
        /// <param name="table">table name</param>
        /// <param name="setVals">set values, in this format: name='bbb',....</param>
        /// <param name="whereCondition">condition such as: where username='aaaaa'</param>
        private DataTable UpdateTable(string table, string setVals, string whereCondition)
        {
            string quary = string.Format("UPDATE {0} SET {1} WHERE {2}", table, setVals, whereCondition);
            return executor.ExecuteSqlQuery(quary);
        }
        /// <summary>
        /// select values from table where condition
        /// </summary>
        /// <param name="table">table name</param>
        /// <param name="values">the columns, for example *</param>
        /// <param name="whereCondition">condition such as: where username='aaaaa', if it is equal to "" then it will get all</param>
        /// <returns></returns>
        private DataTable SelectFromTable(string table, string values, string whereCondition)
        {
            string quary = "";
            if (whereCondition == "")
                quary = string.Format("SELECT {0} FROM {1}", values, table);
            else
                quary = string.Format("SELECT {0} FROM {1} WHERE {2}", values, table, whereCondition);
            return executor.ExecuteSqlQuery(quary);
        }



        /*************************************************************************************************************************/
        /*                                                    TESTS!!!!!!!!!                                                     */
        /*************************************************************************************************************************/


        [TestMethod]
        public void TestInsertionToBuyers()
        {
            //checking the pre-condition
            if (SelectFromTable("Buyers", "*", "username='Lidor11111111'").Rows.Count != 0)
            {
                DeleteFromTableWhereCondition("Buyers", "username='Lidor11111111'");
            }
            //from here it is the initialization:
            bool check = true;
            DataTable dt = SelectFromTable("Buyers", "*", "1=0");//NOTE: this is just in order to get the form of the table
            DataRow newBuyer = dt.NewRow();
            newBuyer["username"] = "Lidor11111111";
            newBuyer["password"] = "123456";
            newBuyer["email"] = "tomer@aaa.com";
            newBuyer["phoneNum"] = "1";
            dt.Rows.Add(newBuyer);
            //from here it's the test:
            InsertToTableWithValues("Buyers", "('Lidor11111111', '123456', 'tomer@aaa.com','1')");
            //checks that the user also exist in the login now....
            check = check && (SelectFromTable("Login", "*", "username='Lidor11111111'").Rows.Count != 0);
            DataTable dt2 = SelectFromTable("Buyers", "*", "username='Lidor11111111'");
            check = check && AreTablesEqual(dt, dt2);
            DeleteFromTableWhereCondition("Buyers", "username='Lidor11111111'");
            Assert.IsTrue(check);
        }
        [TestMethod]
        public void TestDeletionFromBuyers()
        {
            bool check = true;
            //checking the pre-condition
            if (SelectFromTable("Buyers", "*", "username='Tomer11111111'").Rows.Count != 0)
            {
                DeleteFromTableWhereCondition("Buyers", "username='Tomer11111111'");
            }
            DataTable dt = SelectFromTable("Buyers", "*", "");
            InsertToTableWithValues("Buyers", "('Tomer11111111', '123456', 'tomer@aaa.com','1')");//add in order to delete
            DeleteFromTableWhereCondition("Buyers", "username='Tomer11111111'");//we want to check this quary
            //checks that the user also does not exist in the login now....
            check = check && (SelectFromTable("Login", "*", "username='Tomer11111111'").Rows.Count == 0);
            DataTable dt2 = SelectFromTable("Buyers", "*", "");
            check = check && AreTablesEqual(dt, dt2);
            Assert.IsTrue(check);
        }
        [TestMethod]
        public void TestUpdateBuyers()
        {
            //checking the pre-condition
            if (SelectFromTable("Buyers", "*", "username='Lidor11111111'").Rows.Count != 0)
            {
                DeleteFromTableWhereCondition("Buyers", "username='Lidor11111111'");
            }
            DataTable dt = SelectFromTable("Buyers", "*", "1=0");//NOTE: just in order to get the table form
            DataRow newBuyer = dt.NewRow();
            newBuyer["username"] = "Lidor11111111";
            newBuyer["password"] = "1234567";
            newBuyer["email"] = "tomer@aaaa.com";
            newBuyer["phoneNum"] = "1";
            dt.Rows.Add(newBuyer);
            //from here it's the test:
            InsertToTableWithValues("Buyers", "('Lidor11111111', '123456', 'tomer@aaa.com','1')");
            UpdateTable("Buyers", "password='1234567',email='tomer@aaaa.com'", "username='Lidor11111111'");
            DataTable dt2 = SelectFromTable("Buyers", "*", "username='Lidor11111111'");
            bool check = AreTablesEqual(dt, dt2);
            bool check2 = (((SelectFromTable("Login", "*", "username='Lidor11111111'").Rows[0])["password"]).ToString() == "1234567");
            DeleteFromTableWhereCondition("Buyers", "username='Lidor11111111'");
            Assert.IsTrue(check & check2);

        }
        [TestMethod]
        public void Test_Update_Buyer_affect_on_friends_and_categories()
        {
            InsertToTableWithValues("Buyers", "('Tomer11111111', '123456', 'tomer@aaa.com','1')");
            InsertToTableWithValues("Buyers", "('Lidor11111111', '123456', 'lidor@aaa.com','1')");
            InsertToTableWithValues("BuyersFriend", "('Lidor11111111', 'Tomer11111111')");
            InsertToTableWithValues("Categories", "('CHECKKKK','NONE')");
            InsertToTableWithValues("BuyerPreference", "('Tomer11111111', 'CHECKKKK')");

            UpdateTable("Buyers", "username='Tomerr11111111'", "username='Tomer11111111'");
            
            bool check3 = (((SelectFromTable("BuyerPreference", "*", "buyerUsername='Tomerr11111111'").Rows[0])["category"]).ToString() == "CHECKKKK");

            //checks the insertion of buyer's friend 
            bool check5 = ((SelectFromTable("BuyersFriend", "*", "buyerUsername='Lidor11111111'").Rows[0])["friendUsername"].ToString() == "Tomerr11111111");
            //delete the username Tomerr11111111
            DeleteFromTableWhereCondition("Buyers", "username='Tomerr11111111'");
            //check that after deleting Tomerr11111111 from buyers, it is no longer in the BuyersFriend table
            bool check7 = (SelectFromTable("BuyersFriend", "*", "buyerUsername='Lidor11111111'").Rows.Count == 0);
            //after deleting a buyer he should not be in buyer categories....
            bool check4 = (SelectFromTable("BuyerPreference", "*", "buyerUsername='Tomerr11111111'").Rows.Count == 0);
            //delete the username Lidor11111111
            DeleteFromTableWhereCondition("Buyers", "username='Lidor11111111'");
            //check that there Lidor11111111 is not in the BuyersFriend table anymore
            bool check6 = (SelectFromTable("BuyersFriend", "*", "buyerUsername='Lidor11111111'").Rows.Count == 0);
            //deleting the category CHECKKKK
            DeleteFromTableWhereCondition("Categories", "category='CHECKKKK'");
            //Assert.IsTrue(check3 & check4 & check5 & check6 & check7);
            Assert.IsTrue(check3);
            Assert.IsTrue(check4);
            Assert.IsTrue(check5);
            Assert.IsTrue(check6);
            Assert.IsTrue(check7);

        }

        [TestMethod]
        public void TestInsertionAndDeletionToSystemManager()
        {
            string tableName = "SystemManager";
            //checking the pre-condition
            if (SelectFromTable(tableName, "*", "username='Lidor2'").Rows.Count != 0)
            {
                DeleteFromTableWhereCondition(tableName, "username='Lidor2'");
            }
            //from here it is the initialization:

            DataTable dt = SelectFromTable(tableName, "*", "1=0");//NOTE: this is just in order to get the form of the table
            DataRow newSystemManager = dt.NewRow();
            newSystemManager["username"] = "Lidor2";
            newSystemManager["password"] = "123456";
            newSystemManager["email"] = "tomer@aaa.com";
            dt.Rows.Add(newSystemManager);

            DataTable originalDT = SelectFromTable(tableName, "*", "");

            //from here it's the test:
            InsertToTableWithValues(tableName, "('Lidor2', '123456', 'tomer@aaa.com')");

            //checks that the user also exist in the login now....
            //make sure that he is under type S
            Assert.IsTrue(SelectFromTable("Login", "*", "username='Lidor2' AND type='S'").Rows.Count != 0);

            DataTable dt2 = SelectFromTable(tableName, "*", "username='Lidor2'");
            Assert.IsTrue(AreTablesEqual(dt, dt2));

            //post condition
            DeleteFromTableWhereCondition(tableName, "username='Lidor2'");
            Assert.IsTrue(SelectFromTable("Login", "*", "username='Tomer2'").Rows.Count == 0);

            DataTable dt3 = SelectFromTable(tableName, "*", "");
            Assert.IsTrue(AreTablesEqual(originalDT, dt3));
        }


        [TestMethod]
        public void TestUpdateSystemManager()
        {
            string tableName = "SystemManager";
            //checking the pre-condition
            if (SelectFromTable(tableName, "*", "username='Lidor3'").Rows.Count != 0)
            {
                DeleteFromTableWhereCondition(tableName, "username='Lidor3'");
            }
            DataTable dt = SelectFromTable(tableName, "*", "1=0");//NOTE: just in order to get the table form
            DataRow newBuyer = dt.NewRow();
            newBuyer["username"] = "Lidor3";
            newBuyer["password"] = "1234567";
            newBuyer["email"] = "tomer@aaaa.com";
            dt.Rows.Add(newBuyer);
            //from here it's the test:
            InsertToTableWithValues(tableName, "('Lidor3', '123456', 'tomer@aaa.com')");
            UpdateTable(tableName, "password='1234567',email='tomer@aaaa.com'", "username='Lidor3'");
            DataTable dt2 = SelectFromTable(tableName, "*", "username='Lidor3'");
            Assert.IsTrue(AreTablesEqual(dt, dt2));

            //Test that change appear in login
            Assert.IsTrue(((SelectFromTable("Login", "*", "username='Lidor3'").Rows[0])["password"]).ToString() == "1234567");
            DeleteFromTableWhereCondition(tableName, "username='Lidor3'");

        }


        [TestMethod]
        public void TestBusinessManagerBasicCommand()
        {
            string tableName = "BusinessManager";
            string itemTableName = "Business";
            string itemCategoriesTableName = "BusinessCategories";
            string userName = "Lidor5";
            string secondaryUserName = "Lidor6";
            DataTable originalData = SelectFromTable(tableName, "*", "");
            
            // Prepare Table to Test 
            DeleteFromTableWhereCondition(itemTableName, "name='BGU_ADSS'");


            //from here it is the initialization:
            DataTable dataTable = SelectFromTable(tableName, "*", "1=0");//NOTE: this is just in order to get the form of the table
            DataRow newBusinessManager = dataTable.NewRow();
            newBusinessManager["username"] = userName;
            newBusinessManager["password"] = "123456";
            newBusinessManager["email"] = "tomer@aaa.com";
            newBusinessManager["phoneNumber"] = "0576519998";
            dataTable.Rows.Add(newBusinessManager);

            //from here it's the test:
            InsertToTableWithValues(tableName, "('" + userName + "', '123456', 'tomer@aaa.com','0576519998')");

            //checks that the user also exist in the login now....
            //make sure that he is under type M
            Assert.IsTrue(SelectFromTable("Login", "*", "username='" + userName + "' AND type='M'").Rows.Count != 0);

            DataTable dt2 = SelectFromTable(tableName, "*", "username='" + userName + "'");
            Assert.IsTrue(AreTablesEqual(dataTable, dt2));

            // Try insert the same user name from diffrent table
            try
            {
                InsertToTableWithValues("SystemManager", "('" + userName + "', '369852', 'tyu@aaa.com')");
                Assert.IsTrue(false);
            }
            catch(Exception){
                Assert.IsTrue(true);
            }
            try
            {
                InsertToTableWithValues("Buyers", "('" + userName + "', '147852', 'mki@aaa.com')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }

            // Add diffrent username
            newBusinessManager = dataTable.NewRow();
            newBusinessManager["username"] = secondaryUserName;
            newBusinessManager["password"] = "123456";
            newBusinessManager["email"] = "tomer@aaa.com";
            newBusinessManager["phoneNumber"] = "0576519998";
            dataTable.Rows.Add(newBusinessManager);

            //from here it's the test:
            InsertToTableWithValues(tableName, "('" + secondaryUserName + "', '123456', 'tomer@aaa.com','0576519998')");

            //checks that the user also exist in the login now....
            //make sure that he is under type M
            Assert.IsTrue(SelectFromTable("Login", "*", "username='" + secondaryUserName + "' AND type='M'").Rows.Count != 0);

            Assert.IsTrue(SelectFromTable(tableName, "*", "username='" + secondaryUserName + "' OR username='" + userName + "'").Rows.Count == 2);

            // Try insert the same user name from diffrent table
            try
            {
                InsertToTableWithValues("SystemManager", "('" + secondaryUserName + "', '369852', 'tyu@aaa.com')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            try
            {
                InsertToTableWithValues("Buyers", "('" + secondaryUserName + "', '147852', 'mki@aaa.com')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }


            // Adding Business
            DataTable businessTableExample = SelectFromTable(itemTableName, "*", "1=0");//NOTE: this is just in order to get the form of the table
            DataRow newBusiness = businessTableExample.NewRow();
            
            
            newBusiness["name"] = "BGU_ADSS";
            newBusiness["managerUsername"] = userName;
            newBusiness["description"] = "very_fun";
            newBusiness["city"] = "beer-sheva";
            newBusiness["address"] = "blackhole";
            businessTableExample.Rows.Add(newBusiness);
            InsertToTableWithValues(itemTableName, "('" + "BGU_ADSS" + "','" + userName + "','very_fun', 'beer-sheva','blackhole')");

            Assert.IsTrue(AreTablesEqual(businessTableExample, SelectFromTable(itemTableName, "*", "name='" + "BGU_ADSS" + "'")));

            // Category
            if (SelectFromTable("Categories", "*", "category='Sport'").Rows.Count == 0)
            {
                InsertToTableWithValues("Categories", "('Sport','none')");
            }
            // Business Categories
            DataTable businessCategoriesTableExample = SelectFromTable(itemCategoriesTableName, "*", "1=0");//NOTE: this is just in order to get the form of the table
            DataRow newBusinessCategories = businessCategoriesTableExample.NewRow();
            newBusinessCategories["businessName"] = "BGU_ADSS";
            newBusinessCategories["category"] = "Sport";

            businessCategoriesTableExample.Rows.Add(newBusinessCategories);
            InsertToTableWithValues(itemCategoriesTableName, "('" + "BGU_ADSS" + "','" + "Sport" + "')");

            Assert.IsTrue(AreTablesEqual(businessCategoriesTableExample, SelectFromTable(itemCategoriesTableName, "*", "businessName='" + "BGU_ADSS" + "'")));

            // check update
            UpdateTable(tableName, "password='*&^Hello',email='myLife@aaaa.com',username='Bleach'", "username='" + userName + "'");
            userName = "Bleach";
            DataTable dataTable2 = SelectFromTable(tableName, "*", "1=0");//NOTE: this is just in order to get the form of the table
            newBusinessManager = dataTable2.NewRow();
            newBusinessManager["username"] = "Bleach";
            newBusinessManager["password"] = "*&^Hello";
            newBusinessManager["email"] = "myLife@aaaa.com";
            newBusinessManager["phoneNumber"] = "0576519998";
            dataTable2.Rows.Add(newBusinessManager);

            Assert.IsTrue(AreTablesEqual(dataTable2, SelectFromTable(tableName, "*", "username='" + "Bleach" + "'")));

            businessTableExample = SelectFromTable(itemTableName, "*", "1=0");//NOTE: this is just in order to get the form of the table
            newBusiness = businessTableExample.NewRow();
            newBusiness["name"] = "BGU_ADSS";
            newBusiness["managerUsername"] = userName;
            newBusiness["description"] = "very_fun";
            newBusiness["city"] = "beer-sheva";
            newBusiness["address"] = "blackhole";
            businessTableExample.Rows.Add(newBusiness);

            Assert.IsTrue(AreTablesEqual(businessTableExample, SelectFromTable(itemTableName, "*", "name='" + "BGU_ADSS" + "'")));


            // check remove post condition
            DeleteFromTableWhereCondition(tableName, "username='" + secondaryUserName + "'");
            Assert.IsTrue(SelectFromTable("Login", "*", "username='"+secondaryUserName+"'").Rows.Count == 0);
            Assert.IsTrue(SelectFromTable(tableName, "*", "username='"+secondaryUserName+"'").Rows.Count == 0);
            
            DeleteFromTableWhereCondition(tableName, "username='" + userName + "'");
            Assert.IsTrue(SelectFromTable("Login", "*", "username='" + userName + "'").Rows.Count == 0);
            Assert.IsTrue(SelectFromTable(tableName, "*", "username='" + userName + "'").Rows.Count == 0);
            Assert.IsTrue(SelectFromTable(itemTableName, "*", "name='BGU_ADSS'").Rows.Count == 0);
            Assert.IsTrue(SelectFromTable(itemCategoriesTableName, "*", "businessName='BGU_ADSS'").Rows.Count == 0);

        }

        [TestMethod]
        public void TestCouponBasicCommand()
        {
            string couponName = "Bleach";
            string userName = "Lidor8";


            string tableName = "Coupons";
            string businessManagerTableName = "BusinessManager";
            string businessTableName = "Business";
            string businessCategoriesTableName = "BusinessCategories";
            string couponCategoriesTableName = "CouponCategories";
            
            DataTable originalData = SelectFromTable(tableName, "*", "");

            // Prepare Table to Test 
            DeleteFromTableWhereCondition(businessManagerTableName, "username='" + userName + "'");
            DeleteFromTableWhereCondition(businessTableName, "name='BGU_ADSS'");

            //BusinessManager
            InsertToTableWithValues(businessManagerTableName, "('" + userName + "', '1', 'a@aa','123')");
            //Business
            InsertToTableWithValues(businessTableName, "('" + "BGU_ADSS" + "','" + userName + "','very_fun', 'beer-sheva','blackhole')");
            // Category
            if (SelectFromTable("Categories", "*", "category='Sport'").Rows.Count == 0)
            {
                InsertToTableWithValues("Categories", "('Sport','none')");
            }
            // Business Categories
            InsertToTableWithValues(businessCategoriesTableName, "('" + "BGU_ADSS" + "','" + "Sport" + "')");

            //           HERE CREATE COUPON
            DataTable dataTable = SelectFromTable(tableName, "*", "1=0");//NOTE: this is just in order to get the form of the table
            DataRow newCoupon = dataTable.NewRow();
            newCoupon["couponName"] = couponName;
            newCoupon["businessName"] = "BGU_ADSS";
            newCoupon["description"] = "cool";
            newCoupon["originalPrice"] = "58";
            newCoupon["priceAfterDiscount"] = "22";
            newCoupon["rating"] = "4";
            newCoupon["expireDate"] = "2/2/2020";
            newCoupon["isApproved"] = "False";
            newCoupon["price"] = "100";
            newCoupon["brand"] = "nike";
            newCoupon["addedByUser"] = userName;
            dataTable.Rows.Add(newCoupon);
            

            //from here it's the test:
            InsertToTableWithValues(tableName, "('" + couponName + "', 'BGU_ADSS', 'cool','58','22','4','2/2/2020','0','100','nike','"+userName+"',NULL)");
            
            // check that the coupon was inserted like it was supposed to.
            Assert.IsTrue(AreTablesEqual(dataTable, SelectFromTable(tableName, "*", "couponName='" + couponName+"'")));

            // Coupon Categories
            InsertToTableWithValues(couponCategoriesTableName, "('" + couponName +"','BGU_ADSS','" + "Sport" + "')");

            Assert.IsTrue(SelectFromTable(couponCategoriesTableName, "*", "couponName='" + couponName + "'").Rows.Count == 1);

            // Try to add invalid categories
            if (SelectFromTable("Categories", "*", "category='FUN'").Rows.Count == 0)
            {
                InsertToTableWithValues("Categories", "('FUN','none')");
            }
            try
            {
                InsertToTableWithValues(couponCategoriesTableName, "('" + couponName + "',BGU_ADSS','" + "FUN" + "')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            DeleteFromTableWhereCondition(tableName, "couponName='" + couponName + "'");
            Assert.IsTrue(SelectFromTable(tableName, "*", "couponName='" + couponName + "'").Rows.Count == 0);
            Assert.IsTrue(SelectFromTable(couponCategoriesTableName, "*", "couponName='" + couponName + "'").Rows.Count == 0);
        }

        [TestMethod]
        public void TestPrachaseCouponBasicCommand()
        {
            bool insertedBusinessManager = false;
            bool insertedBusiness = false;
            bool insertedBuyer1 = false;
            bool insertedBuyer2 = false;
            bool insertCategory1 = false;
            bool insertCategory2 = false;
            bool insertBussinessCategory = false;


            string couponName = "Bleach";
            string userName = "Lidor8";
            string buyerName = "Tomer092";
            string secondBuyer = "Itay92";
            string tableName = "PruchasedCoupon";
            string buyerTableName = "Buyers";
            string couponTableName = "Coupons";
            string businessManagerTableName = "BusinessManager";
            string businessTableName = "Business";
            string businessCategoriesTableName = "BusinessCategories";
            string couponCategoriesTableName = "CouponCategories";

            //BusinessManager
            if (SelectFromTable(businessManagerTableName, "*", "username='" + userName + "'").Rows.Count == 0) {
                InsertToTableWithValues(businessManagerTableName, "('" + userName + "', '1', 'a@aa','123')");
                insertedBusinessManager = true;
            }
            //Buyer 1
            if (SelectFromTable(buyerTableName, "*", "username='" + buyerName + "'").Rows.Count == 0)
            {
                insertedBuyer1 = true;
                InsertToTableWithValues(buyerTableName, "('" + buyerName + "', '1', 'a@aa','123')");
            }
            //Buyer 2
            if (SelectFromTable(buyerTableName, "*", "username='" + secondBuyer + "'").Rows.Count == 0)
            {
                insertedBuyer2 = true;
                InsertToTableWithValues(buyerTableName, "('" + secondBuyer + "', '1', 'a@aa','123')");
            }
            //Business
            if (SelectFromTable(businessTableName, "*", "name='BGU_ADSS'").Rows.Count == 0)
            {
                insertedBusiness = true;
                InsertToTableWithValues(businessTableName, "('" + "BGU_ADSS" + "','" + userName + "','very_fun', 'beer-sheva','blackhole')");
            }
            // Category
            if (SelectFromTable("Categories", "*", "category='Sport'").Rows.Count == 0)
            {
                insertCategory1 = true;
                InsertToTableWithValues("Categories", "('Sport','none')");
            }
            // Business Categories
            if (SelectFromTable(businessCategoriesTableName, "*", "businessName='BGU_ADSS' AND category='Sport'").Rows.Count == 0)
            {
                insertBussinessCategory = true;
                InsertToTableWithValues(businessCategoriesTableName, "('" + "BGU_ADSS" + "','" + "Sport" + "')");
            }
            

            InsertToTableWithValues(couponTableName, "('" + couponName + "', 'BGU_ADSS', 'cool','58','22','4','2/2/2020','0','100','nike','" + userName + "',NULL)");
            
            //from here it's the test:
            DataTable dataTable = SelectFromTable(tableName, "*", "1=0");//NOTE: this is just in order to get the form of the table
            DataRow newCoupon = dataTable.NewRow();
            newCoupon["couponName"] = couponName;
            newCoupon["businessName"] = "BGU_ADSS";
            newCoupon["buyerUsername"] = buyerName;
            newCoupon["pruchaseDate"] = "2/2/2020";
            newCoupon["isUsed"] = "False";
            newCoupon["serial"] = "1aX$22s5";
            dataTable.Rows.Add(newCoupon);

            InsertToTableWithValues(tableName, "('" + couponName + "', 'BGU_ADSS', '" + buyerName + "','2/2/2020'"+
            ",'False','1aX$22s5'"+")");

            Assert.IsTrue(AreTablesEqual(dataTable, SelectFromTable(tableName, "*", "couponName='" + couponName + "'")));

            try
            {
                InsertToTableWithValues(tableName, "('" + couponName + "', 'BGU_ADSS', '" + secondBuyer + "','2/5/2020'" +
                    ",'False','1aX$22s5'" + ")");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }

            DeleteFromTableWhereCondition(couponTableName, "couponName='" + couponName + "'");
            Assert.IsTrue(SelectFromTable(couponTableName, "*", "couponName='" + couponName + "'").Rows.Count == 0);
            Assert.IsTrue(SelectFromTable(couponCategoriesTableName, "*", "couponName='" + couponName + "'").Rows.Count == 0);

            if (insertedBusinessManager)
            {
                DeleteFromTableWhereCondition(businessManagerTableName, "username='" + userName + "'");
            }
            if (insertedBusiness)
            {
                DeleteFromTableWhereCondition(businessTableName, "name='BGU_ADSS'");
            }
            if (insertedBuyer1)
            {
                DeleteFromTableWhereCondition(buyerTableName, "username='" + buyerName + "'");
            }
            if (insertedBuyer2)
            {
                DeleteFromTableWhereCondition(buyerTableName, "username='" + secondBuyer  + "'");
            }
            if (insertCategory1)
            {
                DeleteFromTableWhereCondition("Categories", "category='Sport'");
            }
            if (insertCategory2)
            {
                DeleteFromTableWhereCondition("Categories", "category='FUN'");
            }
            if (insertBussinessCategory)
            {
                DeleteFromTableWhereCondition(businessCategoriesTableName, "businessName='BGU_ADSS'");
            }
        }


        [TestMethod]
        public void TestLoginBasicCommand()
        {
            string username = "One piece";
            string buyerTableName = "Buyers";
            string systemManagerTableName = "SystemManager";
            string businessManagerTableName = "BusinessManager";

            bool buyerExist = false;
            DataRow buyer = null;
            bool systemManagerExist = false;
            DataRow systemManager = null;
            bool businessManagerExist = false;
            DataRow businessManager = null;

            //backup data
            if (SelectFromTable(buyerTableName, "*", "username='" +username+ "'").Rows.Count != 0)
            {
                buyerExist = true;
                buyer = SelectFromTable(buyerTableName, "*", "username='" + username + "'").Rows[1];
                DeleteFromTableWhereCondition(buyerTableName, "username='" + username + "'");
            }
            if (SelectFromTable(systemManagerTableName, "*", "username='" + username + "'").Rows.Count != 0)
            {
                systemManagerExist = true;
                systemManager = SelectFromTable(systemManagerTableName, "*", "username='" + username + "'").Rows[1];
                DeleteFromTableWhereCondition(systemManagerTableName, "username='" + username + "'");
            }
            if (SelectFromTable(businessManagerTableName, "*", "username='" + username + "'").Rows.Count != 0)
            {
                businessManagerExist = true;
                businessManager = SelectFromTable(businessManagerTableName, "*", "username='" + username + "'").Rows[1];
                DeleteFromTableWhereCondition(businessManagerTableName, "username='" + username + "'");
            }

            InsertToTableWithValues(buyerTableName, "('" + username + "','1','a','1')");
            try
            {
                InsertToTableWithValues(businessManagerTableName, "('" + username + "','1','a','1')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            try
            {
                InsertToTableWithValues(systemManagerTableName, "('" + username + "','1','a')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }

            DeleteFromTableWhereCondition(buyerTableName, "username='" + username + "'");


            InsertToTableWithValues(businessManagerTableName, "('" + username + "','1','a','1')");
            try
            {
                InsertToTableWithValues(buyerTableName, "('" + username + "','1','a','1')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            try
            {
                InsertToTableWithValues(systemManagerTableName, "('" + username + "','1','a')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }

            DeleteFromTableWhereCondition(businessManagerTableName, "username='" + username + "'");

            InsertToTableWithValues(systemManagerTableName, "('" + username + "','1','a')");
            try
            {
                InsertToTableWithValues(buyerTableName, "('" + username + "','1','a','1')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            try
            {
                InsertToTableWithValues(businessManagerTableName, "('" + username + "','1','a','1')");
                Assert.IsTrue(false);
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            DeleteFromTableWhereCondition(systemManagerTableName, "username='" + username + "'");
            
            //restore Data
            if (buyerExist)
            {
                InsertToTableWithValues(buyerTableName,"('"+buyer[0]+"','"+buyer[1]+"','"+buyer[2]+"','"+buyer[3]+"')");
            }
            if (systemManagerExist)
            {
                InsertToTableWithValues(systemManagerTableName, "('" + systemManager[0] + "','" + systemManager[1]
                    + "','" + systemManager[2] + "')");
            }
            if (businessManagerExist)
            {
                InsertToTableWithValues(businessManagerTableName, "('" + businessManager[0] + "','" + businessManager[1] + "','" + businessManager[2] + "','" + businessManager[3] + "')");
            }
        }

    }
}
