﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Entities;
using BusinessLayer;
using System.Net.Http;
using System.Data.Spatial;

namespace ServiceLayer
{

    public class UserbuyerController : ServiceBaseController
    {
        public List<Coupons> GetCouponWithinRange(float range, float lat, float longtidue)
        {
            try
            {
                DbGeography geolocation = DbGeography.FromText("POINT("+lat+" "+longtidue+")");
                List<Coupons> coupons = businessLayer.GetCouponWithinRange(range, geolocation);
                return coupons;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Coupons> GetCouponInCategory([FromUri] List<string> categories)
        {
            try
            {
                List<Coupons> coupons = businessLayer.GetCouponInCategory(categories);
                return coupons;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Buyers> GetFriendList(string token)
        {
            try
            {
                List<Buyers> friends = businessLayer.GetFriendList(token);
                return friends;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Buyers> GetFriendRequests(string token)
        {
            try
            {
                List<Buyers> request = businessLayer.GetFriendRequests(token);
                return request;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public HttpResponseMessage PutSendFreindsRequests(string token, [FromUri] List<string> toUsers)
        {
            try
            {
                businessLayer.SendFreindsRequests(token, toUsers);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutAcceptFreindsRequest(string token, List<string> senders)
        {
            try
            {
                businessLayer.AcceptFreindsRequest(token,senders);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutDeleteFriends(string token, List<string> friends)
        {
            try
            {
                businessLayer.DeleteFriends(token, friends);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutDenyFriendRequests(string token, List<string> senders)
        {
            try
            {
                businessLayer.DenyFriendRequests(token, senders);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutBuyCoupon(string token, [FromBody] Coupons coupon)
        {
            try
            {
                 businessLayer.BuyCoupon(token, coupon);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public List<PruchasedCoupon> GetPressingCoupons(string token)
        {
            try
            {
                List<PruchasedCoupon> request = businessLayer.GetPressingCoupons(token);
                return request;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<PruchasedCoupon> GetPruchasedHistory(string token)
        {
            try
            {
                List<PruchasedCoupon> request = businessLayer.GetPruchasedHistory(token);
                return request;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public HttpResponseMessage PutEdit(string token,[FromBody] Buyers newData)
        {
            try
            {
                businessLayer.EditBuyerData(token, newData);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public List<PruchasedCoupon> GetFriendPruchasedCoupon(string token, string friend)
        {
            try
            {
                return businessLayer.GetFriendPruchasedCoupon(token, friend);
            }
            catch (Exception)
            {
                return null;
            }
        }


        /*       TODO   search Coupon with option       */
        /*       TODO   notification on coupons         */
        

    }
}
