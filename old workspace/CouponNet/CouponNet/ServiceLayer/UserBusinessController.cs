﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Entities;
using BusinessLayer;
using System.Net.Http;
using System.Data.Spatial;

namespace ServiceLayer
{

    public class UserbusinessController : ServiceBaseController
    {
        public HttpResponseMessage PutAddCoupon(string token, Coupons coupon)
        {
            try
            {
                businessLayer.AddCoupon(token, coupon);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutSetCouponAsUsed(string token, string serial)
        {
            try
            {
                businessLayer.SetCouponAsUsed(token, serial);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }


        public HttpResponseMessage PutEditBusinessData(string token, [FromBody] Business business)
        {
            try
            {
                businessLayer.EditBusinessData(token, business);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutEditBusinessManagerData(string token, [FromBody] BusinessManager manager)
        {
            try
            {
                businessLayer.EditBusinessManagerData(token, manager);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public List<Deal> GetPruchasedCoupon(string token)
        {
            try
            {
                List<Deal> deals = businessLayer.GetPruchasedCoupon(token);
                return deals;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
