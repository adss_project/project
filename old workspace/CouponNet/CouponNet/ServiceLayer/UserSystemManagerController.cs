﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Entities;
using BusinessLayer;
using System.Net.Http;
using System.Data.Spatial;

namespace ServiceLayer
{

    public class UsersystemmanagerController : ServiceBaseController
    {


        public HttpResponseMessage PutAddCoupon(string token, string businessName, [FromBody] Coupons coupon)
        {
            try
            {
                businessLayer.AddCoupon(token, businessName, coupon);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }


        public HttpResponseMessage PutSetCouponAsApproved(string token, string businessName, string couponName)
        {
            try
            {
                businessLayer.SetCouponAsApproved(token, businessName, couponName);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public List<Coupons> GetUnapprovedCoupons(string token)
        {
            try
            {
                List<Coupons> unapprovedCoupons = businessLayer.GetUnapprovedCoupons(token);
                return unapprovedCoupons;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public HttpResponseMessage PutBusinessManagerRegistretion(string token, [FromBody] BusinessManager user)
        {
            try
            {
                businessLayer.RegisterBusinessManager(token, user);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutSystemManagerRegistretion(string token, [FromBody] SystemManager user)
        {
            try
            {
                businessLayer.RegisterSystemManager(token, user);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutAddCategory(string token, [FromBody] Categories category)
        {
            try
            {
                businessLayer.AddCategory(token, category);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }


        public HttpResponseMessage PutEditData(string token, [FromBody] SystemManager manager)
        {
            try
            {
                businessLayer.EditSystemManagerData(token, manager);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutEditCoupon(string token, string couponName, string businessName,[FromBody] Coupons newData)
        {
            try
            {
                businessLayer.EditCoupon(token, couponName,businessName, newData);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }
    }
}
