﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class PurchasedCoupon:Coupon
    {
        private DateTime _purchaseDate;
        private bool _isUsed;
        private Coupon _coupon;
        private string _buyerUserName;
        public PurchasedCoupon()
            : base()
        {

        }
        public PurchasedCoupon(DateTime purchaseDate, bool isUsed, Coupon coupon, string buyerUserName)
            : base(coupon)
        {
            this._purchaseDate = purchaseDate;
            this._isUsed = isUsed;
            this._coupon = coupon;
            this._buyerUserName = buyerUserName;
        }
        public DateTime purchaseDate
        {
            get { return _purchaseDate; }
            set { _purchaseDate = value; }
        }
        public bool isUsed
        {
            get { return _isUsed; }
            set { _isUsed = value; }
        }
        public Coupon coupon
        {
            get { return _coupon; }
            set { _coupon = value; }
        }
        public string buyerUserName
        {
            get { return _buyerUserName; }
            set { _buyerUserName = value; }
        }

    }
}
