namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Coupons
    {
        public Coupons()
        {
            PruchasedCoupon = new HashSet<PruchasedCoupon>();
            Categories = new HashSet<Categories>();
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string couponName { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string businessName { get; set; }

        public string description { get; set; }

        public double? originalPrice { get; set; }

        public double? priceAfterDiscount { get; set; }

        public double? rating { get; set; }

        public DateTime? expireDate { get; set; }

        public bool? isApproved { get; set; }

        [StringLength(50)]
        public string brand { get; set; }

        [Required]
        [StringLength(50)]
        public string addedByUser { get; set; }

        [StringLength(50)]
        public string approvedByUser { get; set; }

        public DateTime? addingDate { get; set; }

        public virtual Business Business { get; set; }

        public virtual Login Login { get; set; }

        public virtual SystemManager SystemManager { get; set; }

        public virtual ICollection<PruchasedCoupon> PruchasedCoupon { get; set; }

        public virtual ICollection<Categories> Categories { get; set; }
    }
}
