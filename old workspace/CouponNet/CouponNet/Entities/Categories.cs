namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Categories
    {
        public Categories()
        {
            Business = new HashSet<Business>();
            Buyers = new HashSet<Buyers>();
            Coupons = new HashSet<Coupons>();
        }

        [Key]
        [StringLength(50)]
        public string category { get; set; }

        [StringLength(200)]
        public string description { get; set; }

        public virtual ICollection<Business> Business { get; set; }

        public virtual ICollection<Buyers> Buyers { get; set; }

        public virtual ICollection<Coupons> Coupons { get; set; }
    }
}
