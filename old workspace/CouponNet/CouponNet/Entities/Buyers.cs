namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Buyers
    {
        public Buyers()
        {
            PruchasedCoupon = new HashSet<PruchasedCoupon>();
            Categories = new HashSet<Categories>();
            Buyers1 = new HashSet<Buyers>();
            Buyers2 = new HashSet<Buyers>();
            Buyers11 = new HashSet<Buyers>();
            Buyers3 = new HashSet<Buyers>();
        }

        [Key]
        [StringLength(50)]
        public string username { get; set; }

        [Required]
        [StringLength(50)]
        public string password { get; set; }

        [Required]
        [StringLength(50)]
        public string email { get; set; }

        [StringLength(50)]
        public string phoneNum { get; set; }

        public virtual ICollection<PruchasedCoupon> PruchasedCoupon { get; set; }

        public virtual ICollection<Categories> Categories { get; set; }

        public virtual ICollection<Buyers> Buyers1 { get; set; }

        public virtual ICollection<Buyers> Buyers2 { get; set; }

        public virtual ICollection<Buyers> Buyers11 { get; set; }

        public virtual ICollection<Buyers> Buyers3 { get; set; }
    }
}
