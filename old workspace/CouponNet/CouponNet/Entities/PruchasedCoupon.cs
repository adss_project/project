namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PruchasedCoupon")]
    public partial class PruchasedCoupon
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string couponName { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string businessName { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string buyerUsername { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime pruchaseDate { get; set; }

        public bool isUsed { get; set; }

        [Required]
        [StringLength(200)]
        public string serial { get; set; }

        public virtual Buyers Buyers { get; set; }

        public virtual Coupons Coupons { get; set; }
    }
}
