﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Deal
    {
        public string couponName { get; set; }

        public string businessName { get; set; }

        public string buyerUsername { get; set; }

        public DateTime pruchaseDate { get; set; }

        public bool isUsed { get; set; }
    }
}
