namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Login")]
    public partial class Login
    {
        public Login()
        {
            Coupons = new HashSet<Coupons>();
        }

        [Key]
        [StringLength(50)]
        public string username { get; set; }

        [Required]
        [StringLength(50)]
        public string password { get; set; }

        [Required]
        [StringLength(1)]
        public string type { get; set; }

        public bool isLogin { get; set; }

        [StringLength(50)]
        public string LoginToken { get; set; }

        public virtual ICollection<Coupons> Coupons { get; set; }
    }
}
