namespace Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=ModelSetting")
        {
        }

        public virtual DbSet<Business> Business { get; set; }
        public virtual DbSet<BusinessManager> BusinessManager { get; set; }
        public virtual DbSet<Buyers> Buyers { get; set; }
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<Coupons> Coupons { get; set; }
        public virtual DbSet<Login> Login { get; set; }
        public virtual DbSet<PruchasedCoupon> PruchasedCoupon { get; set; }
        public virtual DbSet<SystemManager> SystemManager { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Business>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<Business>()
                .Property(e => e.managerUsername)
                .IsUnicode(false);

            modelBuilder.Entity<Business>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Business>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<Business>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<Business>()
                .HasMany(e => e.Categories)
                .WithMany(e => e.Business)
                .Map(m => m.ToTable("BusinessCategories").MapLeftKey("businessName").MapRightKey("category"));

            modelBuilder.Entity<BusinessManager>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<BusinessManager>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<BusinessManager>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<BusinessManager>()
                .Property(e => e.phoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<BusinessManager>()
                .HasMany(e => e.Business)
                .WithOptional(e => e.BusinessManager)
                .HasForeignKey(e => e.managerUsername)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Buyers>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<Buyers>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<Buyers>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Buyers>()
                .Property(e => e.phoneNum)
                .IsUnicode(false);

            modelBuilder.Entity<Buyers>()
                .HasMany(e => e.PruchasedCoupon)
                .WithRequired(e => e.Buyers)
                .HasForeignKey(e => e.buyerUsername);

            modelBuilder.Entity<Buyers>()
                .HasMany(e => e.Categories)
                .WithMany(e => e.Buyers)
                .Map(m => m.ToTable("BuyerPreference").MapLeftKey("buyerUsername").MapRightKey("category"));

            modelBuilder.Entity<Buyers>()
                .HasMany(e => e.Buyers1)
                .WithMany(e => e.Buyers2)
                .Map(m => m.ToTable("BuyersFriend").MapLeftKey("buyerUsername").MapRightKey("friendUsername"));

            modelBuilder.Entity<Buyers>()
                .HasMany(e => e.Buyers11)
                .WithMany(e => e.Buyers3)
                .Map(m => m.ToTable("FriendRequest").MapLeftKey("buyerUsername").MapRightKey("senderUsername"));

            modelBuilder.Entity<Categories>()
                .Property(e => e.category)
                .IsUnicode(false);

            modelBuilder.Entity<Categories>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Categories>()
                .HasMany(e => e.Coupons)
                .WithMany(e => e.Categories)
                .Map(m => m.ToTable("CouponCategories").MapLeftKey("category").MapRightKey(new[] { "couponName", "businessName" }));

            modelBuilder.Entity<Coupons>()
                .Property(e => e.couponName)
                .IsUnicode(false);

            modelBuilder.Entity<Coupons>()
                .Property(e => e.businessName)
                .IsUnicode(false);

            modelBuilder.Entity<Coupons>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<Coupons>()
                .Property(e => e.brand)
                .IsUnicode(false);

            modelBuilder.Entity<Coupons>()
                .Property(e => e.addedByUser)
                .IsUnicode(false);

            modelBuilder.Entity<Coupons>()
                .Property(e => e.approvedByUser)
                .IsUnicode(false);

            modelBuilder.Entity<Coupons>()
                .HasMany(e => e.PruchasedCoupon)
                .WithRequired(e => e.Coupons)
                .HasForeignKey(e => new { e.couponName, e.businessName });

            modelBuilder.Entity<Login>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<Login>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<Login>()
                .Property(e => e.type)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Login>()
                .Property(e => e.LoginToken)
                .IsUnicode(false);

            modelBuilder.Entity<Login>()
                .HasMany(e => e.Coupons)
                .WithRequired(e => e.Login)
                .HasForeignKey(e => e.addedByUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PruchasedCoupon>()
                .Property(e => e.couponName)
                .IsUnicode(false);

            modelBuilder.Entity<PruchasedCoupon>()
                .Property(e => e.businessName)
                .IsUnicode(false);

            modelBuilder.Entity<PruchasedCoupon>()
                .Property(e => e.buyerUsername)
                .IsUnicode(false);

            modelBuilder.Entity<PruchasedCoupon>()
                .Property(e => e.serial)
                .IsUnicode(false);

            modelBuilder.Entity<SystemManager>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<SystemManager>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<SystemManager>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<SystemManager>()
                .Property(e => e.phoneNum)
                .IsUnicode(false);

            modelBuilder.Entity<SystemManager>()
                .HasMany(e => e.Coupons)
                .WithOptional(e => e.SystemManager)
                .HasForeignKey(e => e.approvedByUser);
        }
    }
}
