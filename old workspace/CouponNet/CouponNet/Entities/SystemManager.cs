﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class SystemManager : User
    {
        public SystemManager(string name, string password, string phoneNum, string email) : base(name, password, phoneNum, email) { }
    }
}
