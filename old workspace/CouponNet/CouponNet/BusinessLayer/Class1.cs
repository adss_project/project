﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Class1 : IBusinessLayer
    {
        /// <summary>
        /// login user
        /// </summary>
        /// <param name="buyerUsername">his username</param>
        /// <param name="buyerPassword">his password</param>
        /// <returns>Token that keep his login and will be used to find him, token must be unique and contain info on the usertype</returns>
        public string GetLogin(string Username, string Password)
        {
            return "AA";
        }

        /// <summary>
        /// register buyer to the system
        /// </summary>
        /// <param name="user"> the buyer to register</param>
        /// <returns> true if success</returns>
        public bool RegisterBuyer(Buyers user)
        {
            if (user == null)
                throw new Exception();
            return false;
        }

        /// <summary>
        /// register businessmanager, the businessmanager must contain business else exception, to the system
        /// </summary>
        /// <param name="token">can only be done by system manager, this is the token he got when he loged in</param>
        /// <param name="user">the user to add</param>
        public void RegisterBusinessManager(string token, BusinessManager user)
        {
            if (user == null)
                throw new Exception();
            if (token != "TOKEN")
                throw new Exception();

        }

        /// <summary>
        /// register system manager to the system
        /// </summary>
        /// <param name="token">can only be done by system manager, this is the token he got when he loged in</param>
        /// <param name="user">the user to add</param>
        public void RegisterSystemManager(string token, SystemManager user)
        {
            if (user == null)
                throw new Exception();
            if (token != "TOKEN")
                throw new Exception();
        }

        /// <summary>
        /// Logout user from the system
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>true if sucsess or false otherwise</returns>
        public bool Logout(string token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// return all the coupons in radius range from location
        /// </summary>
        /// <param name="range">the radius for the search</param>
        /// <param name="location">the center</param>
        /// <returns>List of those coupons</returns>
        public List<Coupons> GetCouponWithinRange(float range, System.Data.Spatial.DbGeography location)
        {
            return getCouponList();
        }


        private List<Coupons> getCouponList()
        {
            List<Coupons> c = new List<Coupons>();
            c.Add(new Coupons() { couponName = "A1", businessName = "B1" });
            c.Add(new Coupons() { couponName = "A2", businessName = "B2" });
            c.Add(new Coupons() { couponName = "A3", businessName = "B3" });
            c.Add(new Coupons() { couponName = "A4", businessName = "B4" });
            c.Add(new Coupons() { couponName = "A5", businessName = "B5" });
            c.Add(new Coupons() { couponName = "A6", businessName = "B6" });
            return c;
        }

        private List<PruchasedCoupon> getPruchasedCouponList(string token)
        {
            if (token == "TOKEN")
            {
                List<PruchasedCoupon> c = new List<PruchasedCoupon>();
                c.Add(new PruchasedCoupon() { couponName = "A1", businessName = "B1" });
                c.Add(new PruchasedCoupon() { couponName = "A2", businessName = "B2" });
                c.Add(new PruchasedCoupon() { couponName = "A3", businessName = "B3" });
                c.Add(new PruchasedCoupon() { couponName = "A4", businessName = "B4" });
                c.Add(new PruchasedCoupon() { couponName = "A5", businessName = "B5" });
                c.Add(new PruchasedCoupon() { couponName = "A6", businessName = "B6" });
                return c;
            }
            else throw new Exception();
        }

        private List<Deal> getDealList(string token)
        {
            if (token == "TOKEN")
            {
                List<Deal> c = new List<Deal>();
                c.Add(new Deal());
                c.Add(new Deal());
                c.Add(new Deal());
                c.Add(new Deal());
                c.Add(new Deal());
                c.Add(new Deal());
                return c;
            }
            else throw new Exception();
        }

        private List<Buyers> getBuyerList(string token)
        {
            if (token == "TOKEN")
            {
                List<Buyers> c = new List<Buyers>();
                c.Add(new Buyers() { username = "A1", password = "B1" });
                c.Add(new Buyers() { username = "A2", password = "B2" });
                c.Add(new Buyers() { username = "A3", password = "B3" });
                c.Add(new Buyers() { username = "A4", password = "B4" });
                c.Add(new Buyers() { username = "A5", password = "B5" });
                c.Add(new Buyers() { username = "A6", password = "B6" });
                return c;
            }
            else throw new Exception();
        }

        /// <summary>
        /// get all coupons that belongs to given categories.
        /// </summary>
        /// <param name="category">list of categories</param>
        /// <returns>get all coupons that belongs to given categories.</returns>
        public List<Coupons> GetCouponInCategory(List<string> category)
        {
            if (category == null)
                throw new Exception();
            return getCouponList();
        }

        /// <summary>
        /// get the friend list of user with token.
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>friend list of user with token</returns>
        public List<Buyers> GetFriendList(string token)
        {
            return getBuyerList(token);
        }

        /// <summary>
        ///  get all the friend requests of user with token.
        /// </summary>
        /// <param name="token">his token</param>
        /// <returns>all the friend requests of user with token</returns>
        public List<Buyers> GetFriendRequests(string token)
        {
            return getBuyerList(token);
        }

        /// <summary>
        /// Accept all friend request of user with token that their sender is in senders
        /// </summary>
        /// <param name="token"></param>
        /// <param name="senders"></param>
        /// <returns></returns>
        public bool AcceptFreindsRequest(string token, List<string> senders)
        {
            if (senders == null)
                throw new Exception();
            return false;
        }

        /// <summary>
        /// delete all of user with token, friend that in friends
        /// </summary>
        /// <param name="token"></param>
        /// <param name="friends">list of friend that we want to delete</param>
        /// <returns></returns>
        public bool DeleteFriends(string token, List<string> friends)
        {
            if (friends == null)
                throw new Exception();
            return true;
        }

        /// <summary>
        /// deny all friends requests of user with token, that their sender in senders
        /// </summary>
        /// <param name="token"></param>
        /// <param name="senders">list of all the sender we want to deny</param>
        public bool DenyFriendRequests(string token, List<string> senders)
        {
            if (senders == null)
                throw new Exception();
            return true;
        }

        /// <summary>
        /// user with token buy coupon
        /// </summary>
        /// <param name="token">the token of the buying user</param>
        /// <param name="coupon">the coupon he bought</param>
        /// <returns>if sucess return true else false</returns>
        public bool BuyCoupon(string token, Coupons coupon)
        {
            if (coupon == null)
                throw new Exception();
            return true;
        }

        /// <summary>
        /// Get all coupon that was bougth by user with token and have one week to be expires
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public List<PruchasedCoupon> GetPressingCoupons(string token)
        {
            return getPruchasedCouponList(token);
        }

        /// <summary>
        /// get all the coupons that user with token has bougth
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public List<PruchasedCoupon> GetPruchasedHistory(string token)
        {
            return getPruchasedCouponList(token);
        }

        /// <summary>
        /// Edit buyer data
        /// </summary>
        /// <param name="token">his token</param>
        /// <param name="newData">the new data that will override the old one</param>
        public void EditBuyerData(string token, Buyers newData)
        {
            if (newData == null)
                throw new Exception();
        }

        /// <summary>
        /// Get list of couon that friend has bougth
        /// </summary>
        /// <param name="token"></param>
        /// <param name="friend">the friend</param>
        /// <returns></returns>
        public List<PruchasedCoupon> GetFriendPruchasedCoupon(string token, string friend)
        {
            return getPruchasedCouponList(token);
        }

        /// <summary>
        /// send friend request from user with token to toUser
        /// </summary>
        /// <param name="token">token of sender user</param>
        /// <param name="toUsers">the reciver of the request</param>
        /// <returns></returns>
        public List<Buyers> SendFreindsRequests(string token, List<string> toUsers)
        {
            if (toUsers == null)
                throw new Exception();
            return getBuyerList(token);
        }

        /// <summary>
        /// Add coupon
        /// </summary>
        /// <param name="token">token of user that added the coupon</param>
        /// <param name="coupon">the coupon to be added</param>
        public void AddCoupon(string token, Coupons coupon)
        {
            if (coupon == null)
                throw new Exception();
        }

        /// <summary>
        /// set coupon as used
        /// </summary>
        /// <param name="token">need to be the business Manager whitch his business accept the coupon</param>
        /// <param name="serial">the serial of the deal</param>
        public void SetCouponAsUsed(string token, string serial)
        {

        }

        /// <summary>
        /// Edit business
        /// </summary>
        /// <param name="token">token of the business manager</param>
        /// <param name="business">the new data that will override the old</param>
        public void EditBusinessData(string token, Business business)
        {
            if (business == null)
                throw new Exception();
            Console.WriteLine(business);
        }

        /// <summary>
        /// Edit business Manager
        /// </summary>
        /// <param name="token">of the business manager</param>
        /// <param name="manager">the new data that will override the old one</param>
        public void EditBusinessManagerData(string token, BusinessManager manager)
        {
            if (manager == null)
                throw new Exception();
            Console.WriteLine(manager);
        }

        /// <summary>
        /// Edit coupon, WARNING token must belong to system manager
        /// </summary>
        /// <param name="token">of system manager</param>
        /// <param name="couponName">whitch coupon</param>
        /// <param name="businessName">The business of this coupon</param>
        /// <param name="newData">the new data that will override the old one</param>
        public void EditCoupon(string token, string couponName, string businessName, Coupons newData)
        {
            if (newData == null)
                throw new Exception();
            Console.WriteLine(newData);
        }

        /// <summary>
        /// Add coupon o business businessName.
        /// </summary>
        /// <param name="token">the businessmanager of this business</param>
        /// <param name="businessName">the business</param>
        /// <param name="coupon">the coupon to be added</param>
        public void AddCoupon(string token, string businessName, Coupons coupon)
        {
            if (coupon == null)
                throw new Exception();
            Console.WriteLine(coupon);
        }

        /// <summary>
        /// set coupon as Approved, WARNING token must be belong to system manger
        /// </summary>
        /// <param name="token">of system manager that approves it</param>
        /// <param name="businessName">the business of the coupon</param>
        /// <param name="couponName">  the name of the coupon</param>
        public void SetCouponAsApproved(string token, string businessName, string couponName)
        {
            Console.WriteLine(businessName + "," + couponName);
        }

        /// <summary>
        /// Get list of all the unapproved coupon, Token must belong to system manager
        /// </summary>
        /// <param name="token">of system manager</param>
        /// <returns>list of all the unapproved coupons</returns>
        public List<Coupons> GetUnapprovedCoupons(string token)
        {
            return getCouponList();
        }

        /// <summary>
        /// Add category to the system, must be done by system manager ==> token belong to system manager
        /// </summary>
        /// <param name="token">of system manger</param>
        /// <param name="category">the category to be added</param>
        public void AddCategory(string token, Categories category)
        {
            if (token != "TOKEN" || category == null)
                throw new Exception();
        }

        /// <summary>
        /// edit data of system manger
        /// </summary>
        /// <param name="token">the token of the system manager that we will edit</param>
        /// <param name="manager">the new data that will overrdie the old one</param>
        public void EditSystemManagerData(string token, SystemManager manager)
        {
            if (token != "TOKEN" || manager == null)
                throw new Exception();
        }

        /// <summary>
        /// Get all the pruchased coupon that was bougth in bussiness belong to user businessmanager with token.
        /// MUST NOT return the serial of those deals!!!
        /// </summary>
        /// <param name="token">of businessmanager that manage the business</param>
        /// <returns></returns>
        public List<Deal> GetPruchasedCoupon(string token)
        {
            return getDealList(token);
        }

    }
}