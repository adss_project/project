﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseExecutor dbe = new DatabaseExecutor();
            while (true)
            {
                string query = Console.ReadLine();
                dbe.ExecuteSqlQuery(query);
            }
        }
    }
}
