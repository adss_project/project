﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Entities;
using System.Data.Spatial;
namespace DataLAyer
{
    public class DataManager:IDataManager
    {
        private const string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\CouponDB.mdf;Integrated Security=True";
        private SqlConnection connection;
        public DataManager()
        {
            this.connection = new SqlConnection(connectionString);
        }
        /**
         * 
         * 
         * it should contain calls of queries from the DB and 
         * And returns collection of CONCRETE OBJECTS (from the Entities Layer)
         * This static class will be used in the business layer
         * 
         * VERY IMPORTENT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         * NOTE: all of the functions here should be based on stored procedured from the DB
         * That means that if you want to use a queary and there is no stored procedured for it in the DB, then you should add it to there....
         * SEE the DB file: in stored procedured folder, there are some completed queries there.....
         * Note: every function you write in here, should have a signature in its interface...
         * so add it in the interface as well
         * 
         * */
        private bool IsT(string token, string T)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Login Where username={0} AND isLogin={1} AND type={2}", token, true, T);
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter1 = new SqlDataAdapter(sqlCmd);
            if (dataAdapter1.Fill(table) <= 0)
                return false;
            return true;
        }
        private bool IsDataManager(string token)
        {
            return IsT(token, "S");
        }
        private bool IsBusinessManager(string token)
        {
            return IsT(token, "M");
        }
        private bool IsBuyer(string token)
        {
            return IsT(token, "B");
        }
        private DataTable CategoriesCollectionToDataTable(List<Category> collection)
        {
            DataTable table = new DataTable();
            foreach (Category category in collection)
            {
                table.Rows.Add(category.ToString());
            }
            return table;
        }
        public DataTable GetLogin(string userName, string password)
        {

            DataTable table = new DataTable();
            using (var cmd = new SqlCommand("SearchForLogin", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = userName;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = password;
                cmd.Parameters.Add("@LoginToken", SqlDbType.VarChar).Value = userName;
                dataAdapter.Fill(table);
            }
            return table;
        }
        bool RegisterBuyer(Buyer user)
        {
            using (var cmd = new SqlCommand("RegisterBuyer", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = user.userName;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = user.password;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = user.email;
                cmd.Parameters.Add("@phonenum", SqlDbType.VarChar).Value = user.phoneNum;
                cmd.Parameters.AddWithValue("@hobbies", CategoriesCollectionToDataTable(user.preferences));
                return (cmd.ExecuteNonQuery() > 0);
            }
        }
        bool RegisterBusinessManager(string token, BusinessManager user)
        {
            if (!IsDataManager(token))
                return false;
            Business business = user.business;
            using (var cmd = new SqlCommand("RegisterBusiness", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@businessManagerUsername", user.userName);
                cmd.Parameters.AddWithValue("@businessManagerPassword", user.password);
                cmd.Parameters.AddWithValue("@businessManagerEmail", user.email);
                cmd.Parameters.AddWithValue("@businessManagerPhone", user.phoneNum);
                cmd.Parameters.AddWithValue("@businessName", business.businessName);
                cmd.Parameters.AddWithValue("@businessDescription", business.description);
                cmd.Parameters.AddWithValue("@businessCity", business.address.city);
                cmd.Parameters.AddWithValue("@businessAddress", business.address.ToString());
                cmd.Parameters.AddWithValue("@businessLatitude", business.location.Latitude);
                cmd.Parameters.AddWithValue("@businessLongitude", business.location.Longitude);
                cmd.Parameters.AddWithValue("@location", business.location);
                cmd.Parameters.AddWithValue("@categories", CategoriesCollectionToDataTable(business.categories));

                return (cmd.ExecuteNonQuery() > 0);
            }
        }
        bool RegisterSystemManager(string token, SystemManager user)
        {
            if (!IsDataManager(token))
                return false;
            using (var cmd = new SqlCommand("RegisterSystemManager", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = user.userName;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = user.password;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = user.email;
                cmd.Parameters.Add("@phonenum", SqlDbType.VarChar).Value = user.phoneNum;
                return (cmd.ExecuteNonQuery() > 0);
            }
        }
        bool Logout(string token)
        {
            using (var cmd = new SqlCommand("Logout", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = token;
                return (cmd.ExecuteNonQuery() > 0);
            }
        }
        DataTable GetCouponWithinRange(float range, DbGeography location)
        {
            DataTable table = new DataTable();
            using (var cmd = new SqlCommand("GeograpySearchCoupon", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@location", location);
                cmd.Parameters.AddWithValue("@range", range);
                dataAdapter.Fill(table);
            }
            return table;
        }
        DataTable GetCouponInCategory(List<string> categories)
        {
            DataTable table = new DataTable();
            
            using (var cmd = new SqlCommand("CategorySearchCoupon", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string category in categories)
                {
                    DataTable tmp = new DataTable();
                    cmd.Parameters.AddWithValue("@category", category);
                    dataAdapter.Fill(tmp);
                    table.Merge(tmp);
                }
            }
            return table;
        }
        DataTable GetFriendList(string token)
        {
            DataTable table = new DataTable();
            using (var cmd = new SqlCommand("GetFriendList", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@username", token);
                dataAdapter.Fill(table);
            }
            return table;
        }
        DataTable GetFriendRequests(string token)
        {
            DataTable table = new DataTable();
            using (var cmd = new SqlCommand("GetFriendRequest", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@username", token);
                dataAdapter.Fill(table);
            }
            return table;
        }
        bool AcceptFreindsRequest(string token, List<string> senders)
        {
            bool check = true;
            using (var cmd = new SqlCommand("AcceptFreindRequest", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string sender in senders)
                {
                    cmd.Parameters.AddWithValue("@buyerUsername", token);
                    cmd.Parameters.AddWithValue("@senderUsername", sender);
                    check = check & (cmd.ExecuteNonQuery() > 0);
                }
            }
            return check;
        }
        bool DeleteFriends(string token, List<string> friends)
        {
            bool check = true;
            using (var cmd = new SqlCommand("DeleteFriend", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string sender in friends)
                {
                    cmd.Parameters.AddWithValue("@buyerUsername", token);
                    cmd.Parameters.AddWithValue("@friendUsername", sender);
                    check = check & (cmd.ExecuteNonQuery() > 0);
                }
            }
            return check;
        }
        bool DenyFriendRequests(string token, List<string> senders)
        {
            bool check = true;
            using (var cmd = new SqlCommand("DenyFreindRequest", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string sender in senders)
                {
                    cmd.Parameters.AddWithValue("@buyerUsername", token);
                    cmd.Parameters.AddWithValue("@senderUsername", sender);
                    check = check & (cmd.ExecuteNonQuery() > 0);
                }
            }
            return check;
        }
        bool BuyCoupon(string token, Coupon coupon)
        {
            bool check = true;
            using (var cmd = new SqlCommand("BuyCoupon", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@couponName", coupon.name);
                cmd.Parameters.AddWithValue("@businessName", coupon.businessName);
                cmd.Parameters.AddWithValue("@buyerUsername", token);
                cmd.Parameters.AddWithValue("@serial", coupon.serial);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        DataTable GetPressingCoupons(string token)
        {
            DataTable table = new DataTable();
            using (var cmd = new SqlCommand("GetPressingCoupon", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@username", token);
                dataAdapter.Fill(table);
            }
            return table;
        }
        DataTable GetPruchasedHistory(string token)
        {
            DataTable table = new DataTable();
            using (var cmd = new SqlCommand("GetPruchaseHistory", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@username", token);
                dataAdapter.Fill(table);
            }
            return table;
        }
        bool EditBuyerData(string token, Buyer newData)
        {
            bool check = true;
            using (var cmd = new SqlCommand("EditBuyer", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token",token);
                cmd.Parameters.AddWithValue("@username", newData.userName);
                cmd.Parameters.AddWithValue("@password", newData.password);
                cmd.Parameters.AddWithValue("@email", newData.email);
                cmd.Parameters.AddWithValue("@phonenum", newData.phoneNum);
                cmd.Parameters.AddWithValue("@hobbies", CategoriesCollectionToDataTable(newData.preferences));
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        DataTable GetFriendPruchasedCoupon(string token, string friend)
        {
            DataTable table = new DataTable();
            StringBuilder cmdStr = new StringBuilder();
            cmdStr.AppendFormat("SELECT * FROM Login Where username={0} AND isLogin={1} AND type={2}", token, true, "B");
            SqlCommand sqlCmd = new SqlCommand(cmdStr.ToString(), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlCmd);
            if (dataAdapter.Fill(table) <= 0)
                return null;
            return GetPruchasedHistory(friend);
        }
        bool SendFreindsRequests(string token, List<string> toUsers)
        {
            bool check = true;
            using (var cmd = new SqlCommand("AddToFrinedRequests", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (string toUser in toUsers)
                {
                    cmd.Parameters.AddWithValue("@buyerUsername", toUser);
                    cmd.Parameters.AddWithValue("@senderUsername", token);
                    check = check & (cmd.ExecuteNonQuery() > 0);
                }
            }
            return check;
        }
        bool AddCoupon(string token, Coupon coupon)
        {
            if((!IsBusinessManager(token))&&(!IsDataManager(token))){
                return false;
            }
            bool check = true;
            using (var cmd = new SqlCommand("AddNewCoupon", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@couponName", coupon.name);
                cmd.Parameters.AddWithValue("@businessName", coupon.businessName);
                cmd.Parameters.AddWithValue("@description", coupon.description);
                cmd.Parameters.AddWithValue("@originalPrice", coupon.originalPrice);
                cmd.Parameters.AddWithValue("@priceAfterDiscount", coupon.priceAfterDiscount);
                cmd.Parameters.AddWithValue("@rating", coupon.rating);
                cmd.Parameters.AddWithValue("@expireDate", coupon.expirationDate);
                cmd.Parameters.AddWithValue("@isApproved", coupon.isApproved);
                cmd.Parameters.AddWithValue("@brand", coupon.itemBrand);
                cmd.Parameters.AddWithValue("@addedByUser", coupon.adderUsername);
                cmd.Parameters.AddWithValue("@approvedByUser", coupon.approverUsername);
                cmd.Parameters.AddWithValue("@categories", CategoriesCollectionToDataTable(coupon.categories));
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        bool SetCouponAsUsed(string token, string serial)
        {
            if (!IsBusinessManager(token))
                return false;
            bool check = true;
            using (var cmd = new SqlCommand("SetCouponAsUsed2", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@serial", serial);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        bool EditBusinessData(string token,string oldBusinessName, Business business)
        {
            if (!IsBusinessManager(token))
                return false;
            bool check = true;
            using (var cmd = new SqlCommand("EditBusinessData", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", oldBusinessName);
                cmd.Parameters.AddWithValue("@name", business.businessName);
                cmd.Parameters.AddWithValue("@managerUsername", business.manager);
                cmd.Parameters.AddWithValue("@description", business.description);
                cmd.Parameters.AddWithValue("@city", business.address.city);
                cmd.Parameters.AddWithValue("@address", business.address.ToString());
                cmd.Parameters.AddWithValue("@location", business.location);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        bool EditBusinessManagerData(string token, string oldBusinessManagerUserName, BusinessManager manager)
        {
            if (!IsDataManager(token))
                return false;
            bool check = true;
            using (var cmd = new SqlCommand("EditBusinessManagerData", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", oldBusinessManagerUserName);
                cmd.Parameters.AddWithValue("@username", manager.userName);
                cmd.Parameters.AddWithValue("@password", manager.password);
                cmd.Parameters.AddWithValue("@email", manager.email);
                cmd.Parameters.AddWithValue("@phoneNumber", manager.phoneNum);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        bool EditSystemManagerData(string token, SystemManager manager)
        {
            if (!IsDataManager(token))
                return false;
            bool check = true;
            using (var cmd = new SqlCommand("EditBusinessManagerData", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@token", token);
                cmd.Parameters.AddWithValue("@username", manager.userName);
                cmd.Parameters.AddWithValue("@password", manager.password);
                cmd.Parameters.AddWithValue("@email", manager.email);
                cmd.Parameters.AddWithValue("@phoneNumber", manager.phoneNum);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }

        bool EditCoupon(string token, string couponName, string businessName, Coupon newData)
        {
            if (!IsDataManager(token))
                return false;

            bool check = true;
            using (var cmd = new SqlCommand("EditCouponData", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@oldCouponName", couponName);
                cmd.Parameters.AddWithValue("@businessName", businessName);
                cmd.Parameters.AddWithValue("@couponName", newData.name);
                cmd.Parameters.AddWithValue("@description", newData.description);
                cmd.Parameters.AddWithValue("@originalPrice", newData.originalPrice);
                cmd.Parameters.AddWithValue("@priceAfterDiscount", newData.priceAfterDiscount);
                cmd.Parameters.AddWithValue("@rating", newData.rating);
                cmd.Parameters.AddWithValue("@expireDate", newData.expirationDate);
                cmd.Parameters.AddWithValue("@isApproved", newData.isApproved);
                cmd.Parameters.AddWithValue("@brand", newData.itemBrand);
                cmd.Parameters.AddWithValue("@addedByUser", newData.adderUsername);
                cmd.Parameters.AddWithValue("@approvedByUser", newData.approverUsername);
                cmd.Parameters.AddWithValue("@addingDate", newData.addedDate);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        bool SetCouponAsApproved(string token, string businessName, string couponName)
        {
            if (!IsDataManager(token))
                return false;

            bool check = true;
            using (var cmd = new SqlCommand("ApproveCoupon", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@couponName", couponName);
                cmd.Parameters.AddWithValue("@businessName", businessName);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        DataTable GetUnapprovedCoupons(string token)
        {
            if (!IsDataManager(token))
                return null;
            DataTable table = new DataTable();
            using (var cmd = new SqlCommand("GetUnApprovedCoupons", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                dataAdapter.Fill(table);
            }
            return table;
        }
        bool AddCategory(string token, Category category)
        {
            if (!IsDataManager(token))
                return false;

            bool check = true;
            using (var cmd = new SqlCommand("AddCategory", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@category", category.name);
                cmd.Parameters.AddWithValue("@description", category.description);
                check = check & (cmd.ExecuteNonQuery() > 0);
            }
            return check;
        }
        DataTable GetPruchasedCoupon(string token)
        {
            if (!IsBusinessManager(token))
                return null;
            DataTable table = new DataTable();
            using (var cmd = new SqlCommand("GetPruchaseListForBusiness", connection))
            using (var dataAdapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@businessManagerUserName", token);
                dataAdapter.Fill(table);
            }
            return table;
        }
    }
}
