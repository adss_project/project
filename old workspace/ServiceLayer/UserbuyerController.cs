﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Spatial;

namespace ServiceLayer
{
    public class UserbuyerController : ServiceBaseController
    {
        public List<Coupon> GetCouponWithinRange(float range, float lat, float longtidue)
        {
            try
            {
                DbGeography geolocation = DbGeography.FromText("POINT(" + lat + " " + longtidue + ")");
                List<Coupon> Coupon = businessLayer.GetCouponWithinRange(range, geolocation);
                return Coupon;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Coupon> GetCouponInCategory([FromUri] List<string> categories)
        {
            try
            {
                List<Coupon> Coupon = businessLayer.GetCouponInCategory(categories);
                return Coupon;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Buyer> GetFriendList(string token)
        {
            try
            {
                List<Buyer> friends = businessLayer.GetFriendList(token);
                return friends;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Buyer> GetFriendRequests(string token)
        {
            try
            {
                List<Buyer> request = businessLayer.GetFriendRequests(token);
                return request;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public HttpResponseMessage PutSendFreindsRequests(string token, [FromUri] List<string> toUsers)
        {
            try
            {
                businessLayer.SendFreindsRequests(token, toUsers);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutAcceptFreindsRequest(string token, List<string> senders)
        {
            try
            {
                businessLayer.AcceptFreindsRequest(token, senders);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutDeleteFriends(string token, List<string> friends)
        {
            try
            {
                businessLayer.DeleteFriends(token, friends);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutDenyFriendRequests(string token, List<string> senders)
        {
            try
            {
                businessLayer.DenyFriendRequests(token, senders);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutBuyCoupon(string token, [FromBody] Coupon coupon)
        {
            try
            {
                businessLayer.BuyCoupon(token, coupon);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public List<PurchasedCoupon> GetPressingCoupon(string token)
        {
            try
            {
                List<PurchasedCoupon> request = businessLayer.GetPressingCoupons(token);
                return request;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<PurchasedCoupon> GetPruchasedHistory(string token)
        {
            try
            {
                List<PurchasedCoupon> request = businessLayer.GetPruchasedHistory(token);
                return request;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public HttpResponseMessage PutEdit(string token, [FromBody] Buyer newData)
        {
            try
            {
                businessLayer.EditBuyerData(token, newData);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public List<PurchasedCoupon> GetFriendPruchasedCoupon(string token, string friend)
        {
            try
            {
                return businessLayer.GetFriendPruchasedCoupon(token, friend);
            }
            catch (Exception)
            {
                return null;
            }
        }


        /*       TODO   search Coupon with option       */
        /*       TODO   notification on Coupon         */


    }
}
