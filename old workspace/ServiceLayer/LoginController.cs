﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServiceLayer
{
    public class LoginController : ServiceBaseController
    {
        public HttpResponseMessage GetLogin(string buyerUsername, string buyerPassword)
        {
            try
            {
                string token = businessLayer.GetLogin(buyerUsername, buyerPassword);
                HttpResponseMessage m = Request.CreateResponse(HttpStatusCode.OK);
                return m;
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutLogout([FromBody] string token)
        {
            try
            {
                businessLayer.Logout(token);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutBuyerRegistretion([FromBody] Buyer user)
        {
            try
            {
                businessLayer.RegisterBuyer(user);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }


        public List<Coupon> GetCouponWithinRange()//float range)//, [FromBody] DbGeography location)
        {
            try
            {
                //List<Coupons> coupons = businessLayer.GetCouponWithinRange(range, location);
                return null;// coupons;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
