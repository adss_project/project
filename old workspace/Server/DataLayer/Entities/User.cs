﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class User
    {
        private string _userName;
        private string _password;
        private string _phoneNum;
        private string _email;
        public User()
        {

        }
        public User(string userName, string password, string phoneNum, string email)
        {
            this._userName = userName;
            this._password = password;
            this._phoneNum = phoneNum;
            this._email = email;
        }
        public string userName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string password
        {
            get { return _password; }
            set { _password = value; }
        }
        public string phoneNum
        {
            get { return _phoneNum; }
            set { _phoneNum = value; }
        }
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }
    }
}
