﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace CouponNet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SqlConnection connection;
        TextBox _query;
        DataGrid _result;

        public MainWindow()
        {
            InitializeComponent();
            _query = (TextBox)this.FindName("Query");
            _result = (DataGrid)this.FindName("Result");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\Administrator\Documents\simester4\adss\CouponNet\CouponNet\CouponDB.mdf;Integrated Security=True";
            connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand cmd = new SqlCommand(_query.Text,connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            try
            {
                dataAdapter.Fill(dataTable);
            }
            catch (Exception exception) 
            {
                MessageBox.Show(exception.Message);
            }
            connection.Close();
            this._result.ItemsSource = dataTable.DefaultView;
            
        }
    }
}
