﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DataLayer
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseExecutor dbe = new DatabaseExecutor();

            Console.WriteLine("SQL Query Executor.\n");
            
            Console.WriteLine("For the Unit Tests close this windows and then press 'Ctrl+R,A' instead of running this program.\n");
            while (true)
            {
                Console.WriteLine("\n\nEnter SQL Command:\n");
                string query = Console.ReadLine();
                try
                {
                    DataTable dt = dbe.ExecuteSqlQuery(query);
                    foreach (DataRow row in dt.Rows)
                    {
                        Console.WriteLine();
                        for (int x = 0; x < dt.Columns.Count; x++)
                        {
                            Console.Write(row[x].ToString() + " ");
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.ToString());
                }
            }
        }
    }
}
