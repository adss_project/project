﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class BusinessManager : User
    {
        private Business _business;

        public BusinessManager()
        {

        }
        public BusinessManager(string name, string password, string phoneNumber, string email, Business business)
            : base(name, password, phoneNumber,email)
        {
            this._business = business;
            this._business.manager = this.userName;
        }
        public BusinessManager(string name, string password, string phoneNumber, string email)
            : base(name, password, phoneNumber,email)
        {
            this._business = null;
        }
        public Business business
        {
            get { return _business; }
            set { _business = value; }
        }
    }
}
