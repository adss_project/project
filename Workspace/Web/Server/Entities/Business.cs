﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Spatial;
namespace Entities
{
    public class Business
    {
        private string _businessName;
        private string _description;
        private string _manager;
        private List<Category> _categories;
        private Address _address;
        private double _latitude;
        private double _longitude;

        public Business()
        {

        }
        public Business(string businessName, string description, string manager, Address address, List<Category> categories, DbGeography location)
        {
            this._businessName = businessName;
            this._description = description;
            this._manager = manager;
            this._address = address;
            this._categories = categories;
        }
        public Business(string businessName, string description, Address address, List<Category> categories)
        {
            this._businessName = businessName;
            this._description = description;
            this._manager = null;
            this._address = address;
            this._categories = categories;
        }
        public string manager
        {
            get { return _manager; }
            set { _manager = value; }
        }
        public string businessName
        {
            get { return _businessName; }
            set { _businessName = value; }
        }
        public string description
        {
            get { return _description; }
            set { _description = value; }
        }
        public List<Category> categories
        {
            get { return _categories; }
            set { _categories = value; }
        }
        public Address address
        {
            get { return _address; }
            set { _address = value; }
        }

        public double latitude
        {
            get { return _latitude; }
            set { _latitude = latitude; }
        }
        public double longitude
        {
            get { return _longitude; }
            set { _longitude = longitude; }
        }
    }
}
