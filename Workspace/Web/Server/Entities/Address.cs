﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Address
    {
        public string city;
        public string street;
        public int number;
        string fullAdd;
        public Address(string city, string street, int number)
        {
            this.city = city;
            this.street = street;
            this.number = number;
            this.fullAdd = this.street.ToString() + ", " + this.number.ToString();
        }
        public override string ToString()
        {
            return fullAdd.ToString();
        }
    }
}
