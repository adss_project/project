﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Spatial;
namespace Entities
{
    public class Buyer:User
    {
        private string _firstName;
        private string _lastName;
        private DbGeography _location;
        private string _phoneNumber;
        private List<Category> _preferences;
        private List<Buyer> _friends;
        private List<PurchasedCoupon> _purchases;
        public Buyer():base()
        {

        }
        public Buyer(string username, string password, string firstName , string lastName, DbGeography location, string email, string phoneNumber)
            : base(username, password, phoneNumber,email)
        {
            this._location = location;
            this._preferences = new List<Category>();
            this._friends = new List<Buyer>();
            this._purchases = new List<PurchasedCoupon>();
        }

        public string firstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        
        public string lastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public DbGeography location
        {
            get { return _location; }
            set { _location = value; }
        }
        
        public List<Category> preferences
        {
            get { return _preferences; }
            set { _preferences = value; }
        }
        public List<Buyer> friends
        {
            get { return _friends; }
            set { _friends = value; }
        }
        public List<PurchasedCoupon> purchases
        {
            get { return _purchases; }
            set { _purchases = value; }
        }
        public void addFriend(Buyer buyer)
        {
            this._friends.Add(buyer);
        }
        public void removeFriend(Buyer buyer)
        {
            this._friends.Remove(buyer);
        }
        public void addpurchase(PurchasedCoupon purchase)
        {
            this._purchases.Add(purchase);
        }
    }
}
