﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using System.Net.Http;
using System.Data.SqlClient;
using System.Data;

namespace ClassLibrary2
{
    public class Class1
    {
        static void Main()
        {
            string baseAddress = "http://localhost:9020/";

            // Start OWIN host 
            WebApp.Start<Startup>(url: baseAddress);


            Console.WriteLine("SERVER");
            string query; 
            while ((query = Console.ReadLine()) != "exit")
            {
                SqlConnection connection  = new SqlConnection();
                string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\CouponDB.mdf;Integrated Security=True";
                try
                {
                    connection.ConnectionString = connectionString;
                    connection.Open();
                }
                catch (Exception e)
                {
                   Console.WriteLine(e.Message);
                }
                SqlCommand cmd = new SqlCommand(query, connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                try
                {
                    dataAdapter.Fill(dataTable);
                    if (dataTable != null)
                    {
                        foreach (DataRow row in dataTable.Rows)
                        {
                            Console.WriteLine("\n ROW:   ");
                            for (int x = 0; x < dataTable.Columns.Count; x++)
                            {
                                Console.Write(row[x].ToString() + " ");
                            }
                        }
                        Console.WriteLine("\n");
                    }
                    else
                    {
                        Console.WriteLine("EMPTY");
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}
