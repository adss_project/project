﻿using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServiceLayer
{
    public class LoginController : ServiceBaseController
    {
        public HttpResponseMessage GetLogin(string buyerUsername, string buyerPassword)
        {
            try
            {
                List<string> token = businessLayer.GetLogin(buyerUsername, buyerPassword);
                return Request.CreateResponse(HttpStatusCode.OK, token);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, String.Format("Error: {0}.", e.Message));
            }
        }

        public HttpResponseMessage PutLogout([FromBody] string token)
        {
            try
            {
                businessLayer.Logout(token);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("Error: {0}.", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
        }

        public HttpResponseMessage PutBuyerRegistretion([FromBody] Buyer user)
        {
            try
            {
                businessLayer.RegisterBuyer(user);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, String.Format("Error: {0}.", e.Message));
            }
        }

        public List<Category> GetCategories()
        {
            return businessLayer.getCategories();
        }

        public List<Business> GetBussiness(string bussinesName, string category)
        {
            return businessLayer.GetBussiness(bussinesName, category);
        }
        public List<Coupon> GetCoupon(string bussinesName, string category) //need to add reference to spacial but we dont know from where !!!
        {
            return businessLayer.GetCoupons(bussinesName, category);
        }

        public Buyer GetBuyer(string buyerName)
        {
            return businessLayer.GetBuyer(buyerName);
        }

        public BusinessManager GetBusinessManager(string businessName)
        {
            return businessLayer.GetBusinessManager(businessName);
        }
    }
}
