﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ServiceLayer
{
    public class UsersystemmanagerController : ServiceBaseController
    {


        public HttpResponseMessage PutAddCoupon(string token, string businessName, [FromBody] Coupon coupon)
        {
            try
            {
                businessLayer.AddCoupon(token, coupon);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }


        public HttpResponseMessage PutSetCouponAsApproved(string token, string businessName, string couponName)
        {
            try
            {
                businessLayer.SetCouponAsApproved(token, businessName, couponName);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public List<Coupon> GetUnapprovedCoupons(string token)
        {
            try
            {
                List<Coupon> unapprovedCoupons = businessLayer.GetUnapprovedCoupons(token);
                return unapprovedCoupons;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public HttpResponseMessage PutBusinessManagerRegistretion(string token, [FromBody] BusinessManager user)
        {
            try
            {
                businessLayer.RegisterBusinessManager(token, user);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutSystemManagerRegistretion(string token, [FromBody] SystemManager user)
        {
            try
            {
                businessLayer.RegisterSystemManager(token, user);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutAddCategory(string token, [FromBody] Category category)
        {
            try
            {
                businessLayer.AddCategory(token, category);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }


        public HttpResponseMessage PutEditData(string token, [FromBody] SystemManager manager)
        {
            try
            {
                businessLayer.EditSystemManagerData(token, manager);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage PutEditCoupon(string token, string couponName, string businessName, [FromBody] Coupon newData)
        {
            try
            {
                businessLayer.EditCoupon(token, couponName, businessName, newData);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }

        public HttpResponseMessage AddBusiness(string userName, [FromBody] Business businesstoAdd)
        {
            try
            {
               // businessLayer.AddBusiness(token, couponName, businessName, newData);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                var message = string.Format("An Error has Occurd: {0}", e);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, err);
            }
        }
    }
}
