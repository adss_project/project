﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Owin;
using System.Web.Http;

namespace ClassLibrary2
{
    public class Startup
    {


        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional });
            config.Routes.MapHttpRoute(
                name: "LoginApi",
                routeTemplate: "api/{controller}/{action}/{buyerUsername}/{buyerPassword}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "tokenApi",
                routeTemplate: "api/{controller}/{action}/{token}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly;

            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Never;
            appBuilder.UseWebApi(config);
        }
    }
}
