﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Category
    {
        private string _name;
        private string _description;
        public Category(string name, string description)
        {
            this._name = name;
            this._description = description;
        }

        public Category()
        {
            // TODO: Complete member initialization
        }
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string description
        {
            get { return _description; }
            set { _description = value; }
        }
        public override string ToString()
        {
            return this.name.ToString();
        }
    }
}
