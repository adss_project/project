﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net.GUI.Add_GUI
{
    public partial class AdminUser_AddBuisness : System.Web.UI.Page
    {
        List<Category> categoriesList;
 
        protected void Page_Load(object sender, EventArgs e)
        {
 
                try
                {

                    ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();
                    categoriesList = s.GetCategories();

               
                if (!Page.IsPostBack)
                {
                    CategoryList.Items.Add("Select Category");
                    foreach (Category category in (categoriesList))
                    {
                        CategoryList.Items.Add(category.name);
                    }
                } 
                }
                catch (Exception ex) {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
                }

        }

        protected void Add_Click(object sender, EventArgs e)
        {
            string  name = BuisnessNameText.Text,
                    description = BuisnessInformationTextBox.Text,
                    categoty = CategoryList.SelectedValue,
                    password = BMPassword.Text;


            
            try
            {
                Address newAddress = new Address(CityTextBox.Text, streetTextBox.Text, int.Parse(numTextBox.Text));

                Business business = new Business(name, description, newAddress, categoriesList);
                BusinessManager manager = new BusinessManager(BusinessMName.Text, BMPassword.Text, BMPassword.Text, BMEmail.Text, business);
                ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();
                s.PutBusinessManagerRegistretion((string)Session["userName"], manager);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }

        }

    }
}