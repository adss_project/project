﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net.GUI.Add_GUI
{
    public partial class AdminUser_AddAuthorisation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                List<Coupon> couponList;
                ServiceLayerClient.ServiceLayerClient serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                try
                {
                    couponList = serviceLayer.GetUnapprovedCoupon((String)Session["Admin"]);


                    foreach (Coupon coupon in couponList)
                    {
                        ListItem newLine = new ListItem();
                        newLine.Text = coupon.name;  //text name goes i.e. here tab1
                        RadioButtonList1.Items.Add(newLine);
                    }
                }
                catch(Exception ex){
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('"+ex.Message+"');</script>");
                }
            }
        }


        protected void aprrove_Click(object sender, EventArgs e)
        {
            ServiceLayerClient.ServiceLayerClient serviceLayer = new ServiceLayerClient.ServiceLayerClient();
            try
            {
                serviceLayer.PutSetCouponAsApproved((String)Session["Admin"], "", RadioButtonList1.SelectedValue);//should be changed to correct bussines name
                RadioButtonList1.Items.Remove(RadioButtonList1.SelectedItem);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void reject_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceLayerClient.ServiceLayerClient serviceLayer = new ServiceLayerClient.ServiceLayerClient();

                serviceLayer.deleteCoupon(RadioButtonList1.SelectedValue, (String)Session["Admin"]);
                RadioButtonList1.Items.Remove(RadioButtonList1.SelectedItem);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }
    }
}