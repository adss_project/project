﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BuisnessUser_AddCoupon.aspx.cs" Inherits="Coupon_Net.GUI.Options_GUI.Add_GUI.BuisnessUser_AddCoupon" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../style/toolbar_style.css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   <div id="nav">
        <ul>
            <li style="float:left;"><a href="../Welcome GUI/Logout.aspx">Log Out</a></li>
            <li><a href="#"> Coupons</a>
                <ul>
                    <li><a href="#" style="background:#ff6a00">Add New</a></li>
                    <li><a href="../Add GUI/BusinessUser_UseCoupon.aspx">Use</a></li>
                </ul>
            </li>
            <li><a href="#"> Search </a>
            <ul>
                <li><a href="../Search GUI/BuisnessUser_BuisnessSearch.aspx"> Buisness </a></li>
                <li><a href="../Search GUI/BuisnessUser_CouponSearch.aspx"> Coupon </a></li>
            </ul></li>
            <li><a href="#"> View  </a>
                 <ul>
                <li><a href="../View GUI/BuisnessUser_ProfileView.aspx"> Profile </a></li>
                <li><a href="../View GUI/BuisnessUser_CouponView.aspx">  Coupons </a></li>
            </ul></li>

        </ul>
    
    </div>
 <div id="div" style="padding: 50px; float:right; margin-top: 42px;">
             <asp:Image ID="Image1" runat="server" ImageUrl="~/GUI/View/title.png" style=" margin-top: 20px; margin-bottom: 70px"  />

    <p id="Title" style="font:italic; color:#FFFBFF; text-align:left; margin-left:100px; font-size:30px; text-decoration:underline;" > Add Coupon</p>
    <table id="table" class="auto-style3">
      
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label3" runat="server" Font-Size="Large" ForeColor="White" Text="Coupon Name:"></asp:Label>

            </td>
            <td class="auto-style8">
                <asp:TextBox ID="CouponName" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label10" runat="server" Font-Size="Large" ForeColor="White" Text="Coupon Description:"></asp:Label>


            </td>
            <td class="auto-style8">
                <asp:TextBox ID="CouponDescriptionTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label4" runat="server" Font-Size="Large" ForeColor="White" Text="Initial Price:"></asp:Label>


            </td>
            <td class="auto-style8">
                <asp:TextBox ID="InitPrizeTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" >0</asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
         <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label5" runat="server" Font-Size="Large" ForeColor="White" Text="New Prizc:"></asp:Label>


            </td>
            <td class="auto-style8">
                <asp:TextBox ID="NewPriceTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" >0</asp:TextBox>
            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
         <tr>
            <td class="auto-style7" style="border-color:#000000">
                                <asp:Label ID="Label7" runat="server" Font-Size="Large" ForeColor="White" Text="Coupon Category:"></asp:Label>


            </td>
            <td class="auto-style8">

                <asp:DropDownList ID="CategoryList" runat="server" Width="205px">
                </asp:DropDownList>

            </td>
            <td class="auto-style4">
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style7">
                                <asp:Label ID="Label9" runat="server" Font-Size="Large" ForeColor="White" Text="expiration date:"></asp:Label>

            </td>
            <td class="auto-style9">

                   <asp:TextBox ID="ExpirationTextBox" runat="server" TextMode="Phone" Width="200px" style="height: 25px" ></asp:TextBox>
            

            </td>
            <td>

            </td>
        </tr>

        
        <tr id="tr">
            <td class="auto-style7">&nbsp;</td>
            <td id="td" class="auto-style9">
                <asp:Button ID="AddButton" runat="server"  Text="Add"  BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                            BorderWidth="1px" Font-Names="Verdana" Font-Bold="true" ForeColor="#284775" Height="29px" Width="69px" OnClick="Add_Click" style="margin-left: 9px" />
                <br />
                
                &nbsp;&nbsp;
            </td>
            <td>
                <br />
            </td>
        </tr>
    </table>
  </div>





    </form>
</body>
</html>

