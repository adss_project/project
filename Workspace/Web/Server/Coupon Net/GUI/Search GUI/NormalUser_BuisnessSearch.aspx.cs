﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net.GUI.Search_GUI
{
    public partial class NormalBuisnessSearch : System.Web.UI.Page
    {
        List<Category> categoriesList;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();
                categoriesList = s.GetCategories();


                if (!Page.IsPostBack)
                {
                    CategoryList.Items.Add("Select Category");
                    foreach (Category category in (categoriesList))
                    {
                        CategoryList.Items.Add(category.name);
                    }
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceLayerClient.ServiceLayerClient s = new ServiceLayerClient.ServiceLayerClient();
                string redirect = "<script>window.open('SearchShow.aspx');</script>";

                string category = null;
                string name = null;
                if (CategoryList.SelectedValue != "Select Category")
                {
                    category = CategoryList.SelectedValue;
                }
                if(TextBox_Buisness.Text != ""){
                    name = TextBox_Buisness.Text;
                }
                Session["TableToShow"] = s.GetBusiness(name, category, null); // RadioButtonList1.SelectedValue;
                //Session["TableToShow"] = (List<Business>)Session["Business"];
                Session["typeOfSearch"] = "Business";
                Response.Write(redirect);
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }
    }
}