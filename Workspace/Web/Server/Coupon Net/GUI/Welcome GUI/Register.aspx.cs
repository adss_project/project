﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net.GUI.Welcome_GUI
{
    public partial class Register : System.Web.UI.Page
    {
        ServiceLayerClient.ServiceLayerClient serviceLayer;
        protected void Page_Load(object sender, EventArgs e)
        {
            serviceLayer = new ServiceLayerClient.ServiceLayerClient();
            if (!IsPostBack)
            {
                Preferance.DataSource = serviceLayer.GetCategories();
                Preferance.DataBind();
            }
        }

        protected void SubmitRegistration(object sender, EventArgs e)
        {
            try
            {
                Buyer buyer = new Buyer(TextBox_UN.Text, TextBox_Password.Text, TextBox_FN.Text, TextBox_LN.Text, null, TextBox_Email.Text, TextBox_phone.Text);
                List<Category> list = new List<Category>();
                for (int i = 0; i < Preferance.Items.Count; i++)
                {

                    if (Preferance.Items[i].Selected)
                    {
                        list.Add(new Category(Preferance.Items[i].Value,""));
                    }

                }
                buyer.preferences = list;
                bool result = serviceLayer.BuyerRegistretion(buyer);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + "Done!." + "');</script>");
            }
            catch (Exception exception)
            {
                string error = exception.Message;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + error + "');</script>");
            }
        }

        protected void TextBox_Email_TextChanged(object sender, EventArgs e)
        {

        }
    }
}