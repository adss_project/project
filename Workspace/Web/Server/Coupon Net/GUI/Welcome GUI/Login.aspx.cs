﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;
namespace Coupon_Net
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Response.Redirect("../Options GUI/NormalEntry.aspx");
        }

        protected void Login1_Authenticate1(object sender, AuthenticateEventArgs e)
        {
            try
            {
                ServiceLayerClient.ServiceLayerClient serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                String user = Login1.UserName;
                String pass = Login1.Password;
                string type = serviceLayer.Login(pass, user);
                Session["userName"] = user;
                if (type.CompareTo("Normal") == 0)
                {
                    Session["Normal"] = Login1.UserName;
                    Response.Write("Password is correct!");
                    Response.Redirect("../Options GUI/NormalEntry.aspx");
                }
                else
                {
                    if (type.CompareTo("Admin") == 0)
                    {
                        Session["Admin"] = Login1.UserName;
                        Response.Write("Password is correct!");
                        Response.Redirect("../Options GUI/AdminEntry.aspx");
                    }
                    else
                    {
                        Session["BuisnessName"] = Login1.UserName;
                        Response.Redirect("../Options GUI/BuisnessEntry.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }
    }
}