﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;

namespace Coupon_Net.GUI.View_GUI
{
    public partial class NormalUser_ViewProfile : System.Web.UI.Page
    {
        Buyer buyer;
        ServiceLayerClient.ServiceLayerClient serviceLayer;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                buyer = serviceLayer.getBuyer((string)Session["Normal"]);

                passTextBox.Text = buyer.password;
                TextBox_UN.Text = buyer.userName;
                TextBox_Email.Text = buyer.email;
                TextBox_FN.Text = buyer.firstName;
                TextBox_LN.Text = buyer.lastName;
                TextBox_phone.Text = buyer.phoneNum;

                passTextBox.Enabled = false;
                TextBox_UN.Enabled = false;
                TextBox_Email.Enabled = false;
                TextBox_FN.Enabled = false;
                TextBox_LN.Enabled = false;
                TextBox_phone.Enabled = false;


            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
            }
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            ServiceLayerClient.ServiceLayerClient serviceLayer = new ServiceLayerClient.ServiceLayerClient();

            if (EditButton.Text.CompareTo("Edit") == 0)
            {
                passTextBox.Enabled = true;
                passTextBox.Enabled = true;
                TextBox_Email.Enabled = true;
                TextBox_FN.Enabled = true;
                TextBox_LN.Enabled = true;
                TextBox_phone.Enabled = true;
                EditButton.Text = "Confirm";
            }
            else
            {
                try
                {
                    passTextBox.Enabled = false;
                    passTextBox.Enabled = false;
                    TextBox_Email.Enabled = false;
                    TextBox_FN.Enabled = false;
                    TextBox_LN.Enabled = false;
                    TextBox_phone.Enabled = false;
                    EditButton.Text = "Edit";


                    Buyer newBuyer = new Buyer(buyer.userName, passTextBox.Text, TextBox_FN.Text, TextBox_LN.Text, null, TextBox_Email.Text, TextBox_phone.Text);

                    serviceLayer.PutEdit((string)Session["userName"], newBuyer);
                }
                catch (Exception ex)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
                }
            }
        }
    }
}