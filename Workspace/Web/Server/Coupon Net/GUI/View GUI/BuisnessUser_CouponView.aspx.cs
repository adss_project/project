﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entities;
using ServiceLayerClient;

namespace Coupon_Net.GUI.View_GUI
{
    public partial class Coupons_of_Buisness : System.Web.UI.Page
    {
        private ServiceLayerClient.ServiceLayerClient serviceLayer;
        List<Coupon> couponList;
        Business business;
        string businessName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                businessName = (string)Session["BusinessName"];
                try
                {
                    serviceLayer = new ServiceLayerClient.ServiceLayerClient();
                    business = (serviceLayer.GetBusiness(businessName, null, null)).ToArray()[0];
     


             
                
                

                Session["MyCoupons"] = serviceLayer.GetCoupon(businessName, null, null);//should be bussines.catagory, // RadioButtonList1.SelectedValue;
               // Session["MyCoupons"] = (List<Coupon>)Session["Coupon"];

                couponList = (List<Coupon>)Session["MyCoupons"];


                foreach (Coupon coupon in couponList)
                {
                    ListItem newLine = new ListItem();
                    newLine.Text = coupon.name;  //text name goes i.e. here tab1
                    RadioButtonList1.Items.Add(newLine);
                }
                }
                catch (Exception ex)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('" + ex.Message + "');</script>");
                }
            }   
        }

        protected void CouponView_Click(object sender, EventArgs e)
        {

            Coupon wantedCoupon = null;
            foreach (Coupon coupon in (List<Coupon>)Session["MyCoupons"])
            {
                if (coupon.name.CompareTo(RadioButtonList1.SelectedValue) == 0)
                    wantedCoupon = coupon;
            }
            Session["couponFromSearch"] = wantedCoupon;
            string redirect = "<script>window.open('../View GUI/ViewSingleCoupon.aspx');</script>";
            Response.Write(redirect);
        }
    }
}