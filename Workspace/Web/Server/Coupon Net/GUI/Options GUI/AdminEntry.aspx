﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminEntry.aspx.cs" Inherits="Coupon_Net.GUI.Options_GUI.AdminEntry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../style/toolbar_style.css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   <div id="nav">
        <ul>
            <li style="float:left;"><a href="../Welcome GUI/Logout.aspx">Log Out</a></li>
            <li><a href="#"> Add</a>
                <ul>
                    <li><a href="../Add GUI/AdminUser_AddCoupon.aspx">Coupon</a></li>
                    <li><a href="../Add GUI/AdminUser_AddBuisness.aspx">Business</a></li>
                    <li><a href="../Add GUI/AdminUser_AddAuthorization.aspx">Autorization</a></li>
                </ul>
            </li>
            <li><a href="#"> Search </a>
            <ul>
                <li><a href="../Search GUI/AdminUser_BuisnessSearch.aspx"> Business </a></li>
                <li><a href="../Search GUI/AdminUser_CouponSearch.aspx"> Coupon </a></li>
            </ul></li>
            <li><a href="#"> View  </a>
                 <ul>
                <li><a href="#"> Profile </a></li>
                <li><a href="#">  Coupons </a></li>
            </ul></li>

        </ul>
    
    </div>
    </form>
</body>
</html>
