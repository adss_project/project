﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NormalEntry.aspx.cs" Inherits="Coupon_Net.GUI.Options_GUI.NormalEntry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="../style/toolbar_style.css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="nav">
        <ul>
            <li style="float:left;"><a href="../Welcome GUI/Logout.aspx">Log Out</a></li>
            <li><a href="#"> Order</a></li>
            <li><a href="#"> Search </a>
            <ul>
                <li><a href="../Search GUI/NormalUser_BuisnessSearch.aspx"> Buisness </a></li>
                <li><a href="../Search GUI/NormalUser_CouponSearch.aspx"> Coupon </a></li>
            </ul></li>
            <li><a href="#"> View  </a>
                 <ul>
                <li><a href="../View GUI/NormalUser_ProfileView.aspx"> Profile </a></li>
                <li><a href="../View GUI/NormalUser_CouponView.aspx"> My Coupon </a></li>
            </ul></li>

        </ul>
    
    </div>
    </form>
</body>
</html>
